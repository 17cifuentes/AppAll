<?php
use Illuminate\Http\Request;

Route::get('Inicio','EmpresaController@Inicio');{
};

Route::group(['prefix' => 'rol'],function(){
    Route::get('administrador', 'RolController@administrador');
    Route::get('almacenista','RolController@almacenista');
    Route::get('operario','RolController@operario');
    Route::get('vendedor','RolController@vendedor');
    Route::get('rhumanos','RolController@rhumanos');
});
Route::group(['prefix' => 'usuario'],function(){
    Route::get('registrar', 'UsuarioController@registrar');
    Route::get('editar','UsuarioController@editar');
    Route::get('listar','UsuarioController@listar');
    Route::post('postregistrar','UsuarioController@postregistrar');
    Route::post('update','UsuarioController@update');
	Route::post('updatepassword','UsuarioController@updatepassword');
	Route::post('updatepic','UsuarioController@updatepic');	
    Route::get('cambiarestado','UsuarioController@cambiarestado');    
});

Route::group(['prefix' => 'Providers'],function(){
    Route::get('index','ProvidersController@index');
    Route::get('cambiarestado','ProvidersController@cambiarestado');
    Route::get('registrar','ProvidersController@registrar');
    Route::get('eliminar','ProvidersController@eliminar');
    Route::post('postregistro','ProvidersController@postregistro');
    Route::get('editar','ProvidersController@editar');
	Route::post('update','ProvidersController@update');
});

Route::group(['prefix' => 'Cliente'],function(){
    Route::get('index','ClienteController@index');
    Route::get('cambiarestado','ClienteController@cambiarestado');
    Route::get('registrar','ClienteController@registrar');
    Route::get('eliminar','ClienteController@eliminar');
    Route::post('postregistro','ClienteController@postregistro');
	Route::post('postLogin','ClienteController@postLogin');
    Route::get('editar','ClienteController@editar');
	Route::post('update','ClienteController@update');
	Route::post('validar','ClienteController@validar');
	Route::post('recuperar','ClienteController@recuperar');

});

Route::group(['prefix' => 'MateriaPrima'],function(){
    Route::get('index','MPController@index');
    Route::get('listar','MPController@listar');	
    Route::get('ver','MPController@ver');		
    Route::get('registrar','MPController@registrar');
    Route::post('postregistro','MPController@postregistro');
    Route::get('cambiarestado','MPController@cambiarestado');	
	Route::get('editar','MPController@editar');	
	Route::post('update','MPController@update');
    Route::get('bajar','MPController@bajar');	
	Route::get('addproviders','MPController@addproviders');		
	Route::post('addprovider','MPController@addprovider');	
	Route::get('removemps','MPController@removemps');		
	Route::post('removemp','MPController@removemp');	
});

Route::group(['prefix' => 'ProductoTerminado'],function(){
    Route::get('index','PTController@index');
    Route::get('listar','PTController@listar');	
	Route::get('listarMov','PTController@listarMov');
	Route::get('crear','PTController@crear');	
	Route::post('producir','PTController@producir');		
    Route::get('verREQ','PTController@verREQ');		
    Route::get('registrar','PTController@registrar');
    Route::post('postregistro','PTController@postregistro');
    Route::post('posteditar','PTController@posteditar');	
    Route::get('cambiarestado','PTController@cambiarestado');	
	Route::get('editar','PTController@editar');
	Route::get('addreqview','PTController@addreqview');
	Route::get('addMP','PTController@addMP');
	Route::get('addmpview','PTController@addmpview');	
	Route::post('addreq','PTController@addreq');	
	Route::get('eliminardetalle','PTController@eliminardetalle');	
	Route::post('editardetalle','PTController@editardetalle');		
});

Route::group(['prefix' => 'Finanza'],function(){
    Route::get('index','FinanzasController@index');
    Route::get('listar','FinanzasController@listar');	
	Route::get('reservado','FinanzasController@reservado');
    Route::get('registrar','FinanzasController@registrar');
    Route::post('postregistro','FinanzasController@postregistro');
    Route::get('editar','FinanzasController@editar');
	Route::post('update','FinanzasController@update');
	Route::POST('updateCuota','FinanzasController@updateCuota');	
    Route::get('reservado','FinanzasController@reservado');	
	Route::post('pdf','FinanzasController@pdf');	
	Route::post('pdf_todos','FinanzasController@pdf_todos');	
});

Route::group(['prefix' => 'Factura'],function(){
    Route::get('Factura/prodatos','FacturaController@prodatos');
	Route::get('index','FacturaController@index');
    Route::get('cambiarestado','FacturaController@cambiarestado');
    Route::get('registrar','FacturaController@registrar');
    Route::get('eliminar','FacturaController@eliminar');
    Route::post('postregistro','FacturaController@postregistro');
	Route::post('postLogin','FacturaController@postLogin');
    Route::get('editar','FacturaController@editar');
	Route::post('update','FacturaController@update');
	Route::get('agregar','FacturaController@agregar');	
	Route::get('eliminardetalle','FacturaController@eliminardetalle');
	Route::get('agregardeeditar','FacturaController@agregardeeditar');	
	Route::get('eliminardetalledeeditar','FacturaController@eliminardetalledeeditar');	
	Route::post('cuota','FacturaController@cuota');
	Route::get('pdf','FacturaController@pdf');
	Route::get('facturero','FacturaController@facturero');
    Route::get('pedidos','FacturaController@pedidos');
    Route::post('postpedido','FacturaController@postpedido');
	Route::get('pedidosLista','FacturaController@pedidosLista');
	Route::get('pdfpedido','FacturaController@pdfpedido');	
    Route::get('cambiarestadopedido','FacturaController@cambiarestadopedido');	
});

Route::group(['prefix' => 'Empresa'],function(){
    Route::get('editar','EmpresaController@editar');	
	Route::post('update','EmpresaController@update');
	Route::post('inicio','EmpresaController@Inicio');
});
/****************************************************************/

/****************************************************************/
Route::get('/', function () {
    return view('auth.loginyes');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home', 'EmpresaController@Inicio')->name('Inicio');
