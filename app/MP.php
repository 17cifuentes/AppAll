<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MP extends Model
{

   protected $table = 'materia_prima';

   protected $fillable = [
        'cod', 'detalle', 'estado'
    ];
	
}
