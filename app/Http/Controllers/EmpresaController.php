<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Empresa;
use Session;
use DB;
Use Auth;
use App\INVENTARIOMP;
use App\PT;
use App\Cliente;
use App\INVENTARIOART;
use App\Factura;

class EmpresaController extends Controller
{
	//Editar datos de la empresa
    public function editar()
    {
    	extract($_GET);
    	$info = Empresa::all();
    	return view("Empresas.editar",compact("info"));
    }
	//Actualizar datos de la edición
    public function update(){
		$srcpic="";
        extract($_POST);
		if (isset($_FILES['PIC'])){
            $file = $_FILES["PIC"];
            if($file["name"] == null){
               $src = $PIC_ruta;
            }else{
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/empresa/";
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
		}
		$srcpic=$src;
        }
		
        DB::table('empresa')->where("id",1)->update(['nombre'=>$Nombre,'tel'=>$Teléfono,'nit'=>$Nit,'ciudad'=>$Ciudad,'paiis'=>$País,'correo'=>$Correo,'address'=>$Address,'cod_postal'=>$CPostal,'pic'=>$srcpic,'salario'=>$salario,'auxilio'=>$auxilio,'pensioon'=>$pensioon,'salud'=>$salud,'cesantiia'=>$cesantiia,'intereses'=>$intereses,'prima'=>$prima,'vacaciones'=>$vacaciones,'lema1'=>$Lema,'lema2'=>$Responsabilidad,'factura'=>$Factura]);
        return redirect("Inicio");		
    }

	//Editar datos de la empresa
    public function Inicio()
    {
	$info = Empresa::all();
	session(["empresa" => $info[0]->nombre ]);
	
	try{
	if (Auth::user()->user_estado != "Inactivo")
	{
	$Fecha= "0";
	$Head= "Totales";
	extract($_POST);
	//dd($_POST);
	
	$invMP = INVENTARIOMP::all();
	$CostoMP= 0;
	//$MPFecha= DB::table('clientes')->where("id",$id)->delete();

	$invPT = INVENTARIOART::join("artiiculos","artiiculos.id","=","inventario_artiiculos.id_artiiculos")
	->get();
	$CostoPT= 0;
	if($Fecha=="0"){
	$factura = Factura::all();
	$facturaT = Factura::all();
	}
	else{
	$factura = DB::table('reporte')->where("fecha",$Fecha)->get();
	$facturaT = DB::table('facturas')->where("fecha",$Fecha)->get();
	//dd($factura);
	}	
	$totalFactura= 0;
	$cuotaFactura= 0;	
	$cuotapagadasFactura= 0;

	foreach (($invMP) as $Elemento)
	{$CostoMP+= $Elemento->disponible * $Elemento->totalcosto;}
	$CostoMP= number_format($CostoMP,0);
	
	//dd($invPT);
	foreach (($invPT) as $Elemento)
	{$CostoPT+= $Elemento->disponible * $Elemento->precio_unitario;}
	$CostoPT= number_format($CostoPT,0);	
	
	if($Fecha=="0"){
	foreach (($factura) as $Elemento)
	{
	if($Elemento->tipo_pago == "Efectivo")
	{
		if($Elemento->estado_factura == "Pagado" || $Elemento->estado_factura == "Entregado")
		$totalFactura+= $Elemento->total_venta;
	}
	elseif($Elemento->estado_factura == "Entregado")
	{
	$cuotaFactura+= $Elemento->total_venta;
	$cuotapagadasFactura+= $Elemento->total_cuota;
	}
	
	}
	$Head= "Totales";
	}else{
	foreach (($factura) as $Elemento)
	{
	if($Elemento->tipo == "Factura-Pago")
	{$totalFactura+= $Elemento->valor;}
	elseif($Elemento->tipo == "Factura-Crédito")
	{$cuotapagadasFactura+= $Elemento->valor;}
	elseif($Elemento->tipo == "Factura-Cuota")
	{$cuotaFactura+= $Elemento->valor;}	
	}
	$Head= $Fecha;
	}
	
	$Pagadas= 0;
	$Entregadas= 0;
	$Pendientes= 0;
	foreach (($facturaT) as $Elemento)
	{
	//dd($Elemento);
	
	if($Elemento->estado_factura == "Pagado")
	{$Pagadas++;}
	elseif($Elemento->estado_factura == "Entregado")
	{$Entregadas++;}
	elseif($Elemento->estado_factura == "Pendiente")
	{$Pendientes++;}	
	
	}
	
	$array = array();
	for($i=1;$i<=31;$i++){$array[] = 0;}//$i*10000
	
	for($i=0;$i<count($array);$i++)
	{
	$Día= (str_pad($i+1, 2, "00", STR_PAD_LEFT )."/".str_pad(date('n'), 2, "00", STR_PAD_LEFT )."/".date('Y'));
	$Lista= DB::table('reporte')->where("fecha",$Día)->get();
	for($j=0;$j<count($Lista);$j++){
		if($Lista[$j]->tipo == "Factura-Pago" || $Lista[$j]->tipo == "Factura-Cuota"){
		$array[$i] += $Lista[$j]->valor;}
		}
		
	}
	//dd($array);
	//session(["nombre" => "Jeff"]);
    return view("Inicio.index",compact("array","Head","CostoMP","CostoPT","totalFactura","cuotaFactura","cuotapagadasFactura","Pagadas","Entregadas","Pendientes"));
	}
	else{
	Auth::logout();
    Session::flash("incorrecto","El Usuario se encuentra Inactivo, Contáctate con el administrador");	
	return view('auth.loginyes');
	}
	}catch(\Exception $e){
	$Clientes= Cliente::all();
	//dd($Clientes);
	Auth::logout();
    Session::flash("incorrecto","Error en autenticación, por favor intentar de nuevo");	
	return view('auth.loginyes');	
	} 		
	
    }
	
}
