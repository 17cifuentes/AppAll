<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\PT;
use App\MP;
use App\DETALLEART;
use App\DETALLEMP;
Use App\REQUISITOART;
use App\INVENTARIOART;
use App\INVENTARIOMP;
use App\Provider;
use App\User;
use Session;
use DB;
class PTController extends Controller
{	
	//Registrar Producto terminado
    public function registrar()
    {return view("ProductoTerminado.add");}
   //Registro de datos
	public function postregistro()
    {
	
		$insumos= [];
		$nombre= [];
		//$LInsumos= "";
        extract($_POST);
		//dd($_POST);
		
		try{
        $artículos = new PT;
        $artículos->art_nombre=$Código;
        $artículos->detalle=$Detalle;
        $artículos->estado="Activo";
		$artículos->precio_unitario=0;
		$artículos->impuesto=0;
		$artículos->medida=$Medida;
		$artículos->save();
		
        $artículoINVENTARIO = new INVENTARIOART;
        $artículoINVENTARIO->id_artiiculos= PT::max("id");
		$artículoINVENTARIO->disponible= 0;
		$artículoINVENTARIO->save();
		
		for ($i= 0; $i < count($insumos); $i++)
		{
		//dd($insumos[$i]);
        $detalle_mp = new REQUISITOART;
		$detalle_mp->id_mp=$MPnombre[$i];
        $detalle_mp->id_artiiculos=$artículos->id;
        $detalle_mp->cantidad=$insumos[$i];
		$detalle_mp->save();		
		}
		
		for ($i= 0; $i < count($nombre); $i++)
		{
		try{
        $artículosMP = new MP;
        $artículosMP->cod=$nombre[$i];
        $artículosMP->detalle=$detalle[$i];
		$artículosMP->medida=$unidad[$i];
        $artículosMP->estado="Activo";
		$artículosMP->save();
		
		//$LInsumos= $LInsumos . $nombre[$i] . " ";
		
        $artículoINVENTARIO = new INVENTARIOMP;
        $artículoINVENTARIO->id_mp= MP::max("id");
		$artículoINVENTARIO->disponible= 0;
		$artículoINVENTARIO->totalcosto= 0;
		$artículoINVENTARIO->save();
		
        $detalle_mp = new REQUISITOART;
		$detalle_mp->id_mp= MP::max("id");
        $detalle_mp->id_artiiculos=$artículos->id;
        $detalle_mp->cantidad=$insumosreq[$i];
		$detalle_mp->save();			
		
		}catch(\Exception $e){
		/*Session::flash("incorrecto", " 
		Revisa los datos que almacenarás en la base de datos, 
		¿el insumo que almacenarás ya existe?. ".$nombre[$i]." 
		¿Haz llenado todos los campos?
		");
		return redirect("MateriaPrima/index");*/
		} 		
		}		
		
		Session::flash("correcto",$artículos->art_nombre." Se Registró");
		return redirect("ProductoTerminado/index");
		}catch(\Exception $e){
		Session::flash("incorrecto", " 
		Revisa los datos que almacenarás en la base de datos, 
		¿el artículo que almacenarás ya existe?. ".$Código." 
		¿Haz llenado todos los campos?
		");
		return redirect("ProductoTerminado/index");
		}
    }
	//Editar datos del artículo
    public function editar()
    {
    	extract($_GET);
    	$artiiculo = PT::where("id","=",$id_productoterminado)->get();
		$det_prov = REQUISITOART::where("id_artiiculos","=",$id_productoterminado)->get();		
		$MP = MP::all();
		$id_pt= $id_productoterminado;		
    	return view("ProductoTerminado.editar",compact("det_prov", "artiiculo","MP","id_pt"));
    }
   //Actualizar datos artículo
	public function posteditar()
    {
        extract($_POST);
		$artiiculo = PT::find($Id);
        DB::table('artiiculos')->where('id', $Id)->update(['art_nombre'=>$Código,'detalle'=>$Detalle,'medida'=>$Medida,'precio_unitario'=>$Precio,'impuesto'=>$Impuesto]);
        return redirect("ProductoTerminado/index")->with("correcto",$artiiculo->art_nombre." Se Actualizó");
    }  	
	//Añadir Materia Prima para cada requerimiento
    public function addmpview()
    {
    	extract($_GET);
		//SELECT * FROM requisito_art INNER JOIN materia_prima ON requisito_art.id_mp = materia_prima.id INNER JOIN inventario_mp ON requisito_art.id_mp = inventario_mp.id_mp  WHERE `id_artiiculos` = 1
		
		$requisitos = REQUISITOART::join("materia_prima","requisito_art.id_mp","=","materia_prima.id")
		->join("artiiculos","requisito_art.id_artiiculos","=","artiiculos.id")
		->join("inventario_mp","requisito_art.id_mp","=","inventario_mp.id_mp")
		->where("id_artiiculos","=",$id_productoterminado)
		->get();
		
		if (count($requisitos) > 0 )
		{return view("ProductoTerminado.addMP",compact("requisitos"));}
		else
		{return redirect("ProductoTerminado/listar")->with("incorrecto"," Aún no hay requisitos para éste artículo");}
    }
	//Agregar datos de materias primas
	public function addMP()
    {
        extract($_GET);
		$artiiculo = PT::where("id","=",$Id)
		->get();
		//dd($artiiculo);
		if ($Cantidad != 0)
		{
		//dd($_GET);
        $detalle_pt = new DETALLEART;
        $detalle_pt->id_artiiculos=$Id;
        $detalle_pt->cantidad=$Cantidad;
		$detalle_pt->save();
			
		$idMPS= (explode(",",$idMPS));
		$UsadoMPS= (explode(",",$UsadoMPS));
		//dd($UsadoMPS);
		for ($i = 0; $i < count($idMPS); ++$i)
		{
        $detalle_mp = new DETALLEMP;
		$detalle_mp->id_mp=$idMPS[$i];
        $detalle_mp->cantidad=round($UsadoMPS[$i],10,PHP_ROUND_HALF_UP);
		$detalle_mp->detalle="Usado para crear ".$artiiculo[0]->art_nombre;
		$detalle_mp->tipo=0;
		$detalle_mp->save();
		}
		
		Session::flash("correcto"," Se actualizó");
		return redirect("ProductoTerminado/crear");  
		}
		else
		{
		Session::flash("incorrecto"," La cantidad a agregar fue Cero");
		return redirect("ProductoTerminado/crear");  			
		}
		
    }	
	//añadir requerimiento
    public function addreqview()
    {
    	extract($_GET);
		$artiiculo = PT::where("id","=",$id_productoterminado)->get();
		$Lista = MP::all();
		$ListaDetalle = DETALLEMP::all();
        return view("ProductoTerminado.addreq",compact("Lista","artiiculo","ListaDetalle"));		
    }		
	//Agregar datos de requisito
	public function addreq()
    {
        extract($_POST);	
        $detalle_mp = new REQUISITOART;
		$detalle_mp->id_mp=$MateriaPrima;
        $detalle_mp->id_artiiculos=$Id;
        $detalle_mp->cantidad=$Cantidad;
		$detalle_mp->save();
		
		$det_prov = REQUISITOART::where("id_artiiculos","=",$Id)->get();
		$artiiculo = PT::where("id","=",$Id)->get();
		$MP = MP::all();
		$id_pt= $Id;
        return view("ProductoTerminado.editar",compact("det_prov", "artiiculo","MP","id_pt"));
    }
	//Listar los artículos
    public function index()
    {
		//session(["nombre" => "noJeff"]);	
		//dd(session(->write["nombre"]));
		//session(['nombre' => 'noJeff']);
		$ver= "Activo";
		extract($_GET);
		$ver= $ver;		
    	$Lista = PT::all();
		$Inventario = INVENTARIOART::all();
		$MateriaPrima = MP::all();
    	return view("ProductoTerminado.indexPT",compact("Lista", "Inventario", "MateriaPrima","ver"));
    }	
	//Listar para ver inventario
    public function listar()
    {
    	$Lista = PT::all();
		$Inventario = INVENTARIOART::all();
		$MateriaPrima = MP::all();
    	return view("ProductoTerminado.listar",compact("Lista", "Inventario", "MateriaPrima"));
    }
	//Listar movimientos de creaciones de artículos
    public function listarMov()
    {
    	$Lista = PT::join("produccioon","produccioon.id_artiiculos","=","artiiculos.id")
		->join("personal","personal.id_personal","=","produccioon.id_personal")
		->get();
		//dd($Lista);
    	return view("ProductoTerminado.listarMov",compact("Lista"));
    }	
	//Listar para crear producto terminado
    public function crear()
    {
    	$Lista = PT::all();
		$Inventario = INVENTARIOART::all();
		$MateriaPrima = MP::all();
		$usuarios = User::join("personal","personal.id_personal","=","users.id_personal")
		->get();
		$disponibles = array();
		//dd(($Lista[0]['attributes']['id']));
		for ($i= 0; $i < count($Lista); $i++)
		{
			$requisitos = REQUISITOART::join("materia_prima","requisito_art.id_mp","=","materia_prima.id")
			->join("artiiculos","requisito_art.id_artiiculos","=","artiiculos.id")
			->join("inventario_mp","requisito_art.id_mp","=","inventario_mp.id_mp")
			->where("id_artiiculos","=",$Lista[$i]['attributes']['id'])
			->get();
			//dd(count($requisitos));
			$posible= true;
			$Cantidad= 0;
			$texto= "";
			if(count($requisitos)== 0){$posible= false;$Cantidad= 1;}
			while($posible)
			{
				$Cantidad++;			
				for ($j= 0; $j < count($requisitos); $j++)
				{
					$Disponible= ($requisitos[$j]['attributes']['disponible']);
					$Solicita= ($requisitos[$j]['attributes']['cantidad']);
					$Nombre= ($requisitos[$j]['attributes']['cod']);
					$texto= $texto." ".$Nombre." ".$Solicita*$Cantidad."/".$Disponible."-".$Cantidad."-";
					if($Solicita*$Cantidad > $Disponible) $posible= false;
				}
			}
			//dd($texto);
			//dd($Cantidad-1);
			$disponibles[] = $Cantidad-1;
		}
    	return view("ProductoTerminado.crear",compact("Lista", "Inventario", "MateriaPrima","disponibles","usuarios"));
    }
	//Listar para registrar producción terminado
    public function producir()
    {
		extract($_POST);
		if($Cantidad > 0)
		{
    	$Lista = PT::all();
		$Inventario = INVENTARIOART::all();
		$MateriaPrima = MP::all();
		$usuarios = User::join("personal","personal.id_personal","=","users.id_personal")
		->get();
		$disponibles = array();
		//dd(($Lista[0]['attributes']['id']));

			$requisitos = REQUISITOART::join("artiiculos","requisito_art.id_artiiculos","=","artiiculos.id")
			->join("materia_prima","requisito_art.id_mp","=","materia_prima.id")
			->join("inventario_mp","requisito_art.id_mp","=","inventario_mp.id_mp")
			->where("id_artiiculos","=",$artiiculo)
			->get();
			//dd($requisitos);
			$posible= true;
			if(count($requisitos)== 0){$posible= false;}
			$texto= "";
				for ($j= 0; $j < count($requisitos); $j++)
				{
					$Disponible= ($requisitos[$j]['attributes']['disponible']);
					$Solicita= ($requisitos[$j]['attributes']['cantidad']);
					$Nombre= ($requisitos[$j]['attributes']['cod']);
					$texto= $texto." ".$Nombre." ".$Solicita*$Cantidad."/".$Disponible."-".$Cantidad."-";
					if($Solicita*$Cantidad > $Disponible){ $posible= false;}
				}
			//dd($texto);
			
		if($posible == true)
    	{
		
				for ($j= 0; $j < count($requisitos); $j++)
				{
					$Disponible= ($requisitos[$j]['attributes']['disponible']);
					$Solicita= ($requisitos[$j]['attributes']['cantidad']);
					$Nombre= ($requisitos[$j]['attributes']['cod']);
					$detalle_mp = new DETALLEMP;
					$detalle_mp->id_mp= ($requisitos[$j]['attributes']['id']);
					$detalle_mp->cantidad= round($Solicita*$Cantidad,10,PHP_ROUND_HALF_UP);
					$art_nombre = PT::where("id","=",$artiiculo)
					->get();
					$detalle_mp->detalle="Usado para crear ".$art_nombre[0]->art_nombre;
					$detalle_mp->tipo=0;
					$detalle_mp->save();			
				}
	
		$detalle_pt = new DETALLEART;
		$detalle_pt->id_artiiculos=$artiiculo;
		$detalle_pt->cantidad=$Cantidad;
		$detalle_pt->save();

		DB::table('produccioon')->insert(['id_personal'=>$usuario,'id_artiiculos'=>$artiiculo,'detalle'=>$Detalle,'cantidad'=>$Cantidad,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
		
		Session::flash("correcto"," Se fabricaron ".$Cantidad." ".$art_nombre[0]->art_nombre);
		}
		else
    	{
		Session::flash("incorrecto"," No hay suficiente cantidad de insumos para fabricarlo, revisa por favor");
		}		
		
    	return redirect("ProductoTerminado/crear");
		}else
    	{
		Session::flash("incorrecto"," No se producen cero unidades");
		return redirect("ProductoTerminado/crear");		
		}		
		
    	
			
		}	
	//Ver requerimientos del artículo
    public function verREQ()
    {
    	extract($_GET);
		$det_prov = REQUISITOART::where("id_artiiculos","=",$id_productoterminado)->get();
		$artiiculo = PT::where("id","=",$id_productoterminado)->get();
		$MP = MP::all();
		$id_pt= $id_productoterminado;
        return view("ProductoTerminado.editar",compact("det_prov", "artiiculo","MP","id_pt"));
    }	
	//Ver Materias Primas Usadas
    public function verMP()
    {
    	extract($_GET);
		$det_prov = DETALLEART::where("id_mp","=",$id_productoterminado)->get();
		$artiiculo = PT::where("id","=",$id_productoterminado)->get();
		$proveedores = Provider::all();
        return view("ProductoTerminado.viewprovider",compact("det_prov", "artiiculo","proveedores"));
    }		
	//Cambiar estado producto terminado
    public function cambiarestado()
    {
    	extract($_GET);
    	$artículo = PT::find($id_productoterminado);
    	if($artículo->estado == "Activo")
    	{
    		$artículo->estado = "Inactivo";	
    	}else
    	{
    		$artículo->estado = "Activo";
    	}
    	Session::flash("correcto",$artículo->art_nombre." se estableció como ".$artículo->estado);
    	$artículo->save();
    	return redirect("ProductoTerminado/index");
    }	
	//eliminar datos de requisitos de artículos
	public function eliminardetalle()
    {
        extract($_GET);
		//dd($_GET);
		DB::table('requisito_art')->where("id_requisitoart",$Id)->delete();
		return redirect("ProductoTerminado/verREQ?id_productoterminado=".$IdPT);
	}

	public function editardetalle()
    {
        extract($_POST);
		//dd($_POST);
		try{
		DB::table('requisito_art')->where("id_requisitoart",$Id)->update(['cantidad'=>$Cantidad]);
		return redirect("ProductoTerminado/verREQ?id_productoterminado=".$IdPT);
		}catch(\Exception $e){
		Session::flash("incorrecto", " 
		Revisa los datos que actualizarás en la base de datos, 
		¿el valor es correcto con punto o coma?. ".$Cantidad." 
		");
		return redirect("ProductoTerminado/index");
		} 		
	}	
}
