<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\MP;
use App\DETALLEMP;
use App\INVENTARIOMP;
use App\Provider;
use Session;
use DB;
class MPController extends Controller
{	
	//Registrar Materia Prima
    public function registrar()
    {return view("MateriaPrima.add");}
   //Registro de datos
	public function postregistro()
    {
	try{
        extract($_POST);
        $artículos = new MP;
        $artículos->cod=$Código;
        $artículos->detalle=$Detalle;
		$artículos->medida=$Medida;
        $artículos->estado="Activo";
		$artículos->save();
		
        $artículoINVENTARIO = new INVENTARIOMP;
        $artículoINVENTARIO->id_mp= MP::max("id");
		$artículoINVENTARIO->disponible= 0;
		$artículoINVENTARIO->totalcosto= 0;
		$artículoINVENTARIO->save();
		
		Session::flash("correcto",$artículos->detalle." Se Registró");
		return redirect("MateriaPrima/index"); 
	}catch(\Exception $e){
	Session::flash("incorrecto", " 
	Revisa los datos que almacenarás en la base de datos, 
	¿el insumo que almacenarás ya existe?. ".$Código." 
	¿Haz llenado todos los campos?
	");
	return redirect("MateriaPrima/index");
	} 		
    }
	//Editar datos del materia prima
    public function editar()
    {
    	extract($_GET);
    	$usuarios = MP::where("id","=",$id)->get();
    	return view("MateriaPrima.editar",compact("usuarios"));
    }	
	//Actualizar usuario
    public function update(){
        extract($_POST);
        DB::table('materia_prima')->where('id', $Id)->update(['cod'=>$Código,'detalle'=>$Detalle,'medida'=>$Medida]);
        return redirect("MateriaPrima/index")->with("correcto",$Código." Se Actualizó");
    }	
	//Añadir materia prima
    public function addproviders()
    {
    	extract($_GET);
		$artiiculo = MP::where("id","=",$id_materiaprima)->get();
		$Lista = Provider::all();
        return view("MateriaPrima.addprovider",compact("Lista","artiiculo"));		
    }	
	//Agregar datos de materia prima
	public function addprovider()
    {
        extract($_POST);	
        $detalle_mp = new DETALLEMP;
		$detalle_mp->id_mp=$Id;
        $detalle_mp->detalle=$Anotación." ".$Proveedor;
        $detalle_mp->cantidad=$Cantidad;
		$detalle_mp->costo=$Costo/$Cantidad;
		$detalle_mp->tipo=1;
		$detalle_mp->save();
		
		Session::flash("correcto",$Detalle." Se actualizó");
		return redirect("MateriaPrima/listar");  
    }
	//remover materia prioma vista 
    public function removemps()
    {
    	extract($_GET);
		$artiiculo = MP::where("id","=",$id_materiaprima)->get();
		$Lista = Provider::all();
        return view("MateriaPrima.removemps",compact("Lista","artiiculo"));		
    }	
	//Agregar datos de materia prima removida
	public function removemp()
    {
        extract($_POST);	
        $detalle_mp = new DETALLEMP;
		$detalle_mp->id_mp=$Id;
		$detalle_mp->detalle=$Anotación;
        $detalle_mp->cantidad=$Cantidad;
		$detalle_mp->costo=0;
		$detalle_mp->tipo=0;
		$detalle_mp->save();
				
		Session::flash("correcto",$Detalle." Se actualizó");
		return redirect("MateriaPrima/listar");  
    }
	//Agregar datos de materia prima removida desde producto terminado
	public function removempPT()
    {
        extract($_GET);	
        $detalle_mp = new DETALLEMP;
		$detalle_mp->id_mp=$Id;
        $detalle_mp->cantidad=$Cantidad;
		$detalle_mp->costo=0;
		$detalle_mp->tipo=0;
		$detalle_mp->save();
 
    }	
	//Listar las materias primas
    public function index()
    {
		$ver= "Activo";
		extract($_GET);
		$ver= $ver;		
    	$Lista = MP::all();
		$Inventario = INVENTARIOMP::all();
    	return view("MateriaPrima.indexMP",compact("Lista", "Inventario","ver"));
    }	
	//Listar para ver inventario
    public function listar()
    {
    	$Lista = MP::all();
		$Proveedores = Provider::all();
		$Inventario = INVENTARIOMP::all();
    	return view("MateriaPrima.listar",compact("Lista", "Inventario", "Proveedores"));
    }
	//Ver la materia prima
    public function ver()
    {
    	extract($_GET);
		$det_prov = DETALLEMP::where("id_mp","=",$id_materiaprima)->OrderBy("updated_at","desc")->get();
		$artiiculo = MP::where("id","=",$id_materiaprima)->get();
		$proveedores = Provider::all();
		$Inventario = INVENTARIOMP::where("id_inventario_mp","=",$id_materiaprima)->get();
        return view("MateriaPrima.viewprovider",compact("det_prov", "artiiculo","proveedores","Inventario"));
    }	
    //Eliminar mp
    public function bajar(){
        //extract($_GET);
        //DB::table('detalle_mp')->where("id_detallemp",$id)->delete();
		//return redirect("MateriaPrima/listar"); 

	}		
	//Cambiar estado materia prima
    public function cambiarestado()
    {
    	extract($_GET);
    	$artículo = MP::find($id_materiaprima);
    	if($artículo->estado == "Activo")
    	{
    		$artículo->estado = "Inactivo";	
    	}else
    	{
    		$artículo->estado = "Activo";
    	}
    	Session::flash("correcto",$artículo->detalle." se estableció como ".$artículo->estado);
    	$artículo->save();
    	return redirect("MateriaPrima/index");
    }
	
	
	

}
