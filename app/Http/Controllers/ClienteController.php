<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Cliente;
use App\User;
use Session;
use DB;
use Mail;
class ClienteController extends Controller
{
   //Añadir datos del registro del cliente
	public function postregistro()
    {
	try{
        extract($_POST);
        $usuario = new Cliente;
        $usuario->nombre_clientes=$Nombre;
        $usuario->tel=$Teléfono;
        $usuario->correo_cliente=$Correo;
		$usuario->address=$Address;
        $usuario->estado="Activo";
		$usuario->vendedor=$PersonalID;
		$usuario->valida=str_random(20);
		$usuario->password=bcrypt("123456");
		$usuario->save();
		
		$id_cliente = Cliente::max("id");
    	$user = new User;
    	$user->name = $Nombre;
    	$user->id_personal = 2;
    	$user->email = $Correo;
    	$user->rol = "Cliente";
		$user->cuota = $id_cliente;
		$user->remember_token= "";		
    	$user->user_estado = "Activo";
		$user->pic = "images/defaultCustomer.png";
    	$user->password =bcrypt("123456");
		
        if($user->save() == true){
			Session::flash("correcto",$usuario->nombre_clientes." Está ahora registrado");
        }else{
			Session::flash("incorrecto","¡Upss! Algo pasó en el registro, inténtalo nuevamente");
        };
		return redirect("Cliente/index");
	}catch(\Exception $e){
	Session::flash("incorrecto", " 
	Revisa los datos que almacenarás en la base de datos, 
	¿el correo que almacenarás ya existe?. ".$Correo." 
	¿Haz llenado todos los campos?
	");
	return redirect("Cliente/index");
	}		
    }
   //Añadir datos del registro del cliente
	public function postLogin()
    {
        extract($_POST);
        //$usuario = new Cliente;
        //$usuario->nombre_clientes=$Nombre;
        //$usuario->tel=$Teléfono;
        //$usuario->correo_cliente=$Correo;
		//$usuario->address=$Address;
        //$usuario->estado="Activo";
		//$usuario->valida=str_random(20);
		//$usuario->password=bcrypt("123456");
		//$usuario->save();
		$valida=  str_random(20);
		$CorreoR= "info@info.com";
		$NombreR= "info";
		//dd($Correo);
		Mail::send("Clientes.valida",["valida"=>$valida,"Nombre"=>$Nombre], function($message) use ($CorreoR,$Correo,$NombreR)
	       {

	           $message->from($CorreoR,$NombreR)->subject("Código de Verificación");
                   //$message->cc(["asanchez@tecnoquimicas.com","departamentodeseguridad@tecnoquimicas.com","seguridad@tecnoquimicas.com"]);
	           $message->to($Correo); 
                   $message->subject("Código de Verificación");
	      });
		
		return view("Clientes.postLogin",compact("valida","Nombre","Teléfono","Correo","Address"));
    }
	public function validar()
    {
	try{
		extract($_POST);
		if($valida == $código)
		{
			//dd($_POST);
			$usuario = new Cliente;
			$usuario->nombre_clientes=$nombre;
			$usuario->tel=$Teléfono;
			$usuario->correo_cliente=$Correo;
			$usuario->address=$Address;
			$usuario->estado="Activo";
			$usuario->valida=str_random(20);
			$usuario->password=bcrypt("123456");
			$usuario->save();
			
			$id_cliente = Cliente::max("id");
			$user = new User;
			$user->name = $nombre;
			$user->id_personal = 2;
			$user->email = $Correo;
			$user->rol = "Cliente";
			$user->cuota = $id_cliente;
			$user->remember_token= "";		
			$user->user_estado = "Activo";
			$user->pic = "images/defaultCustomer.png";
			$user->password =bcrypt("123456");	
			$user->save();
			
		Session::flash("correcto","Ingresa tu correo ".$Correo." y como contraseña 123456");	
		return view('auth.loginyes');				
		}
		else
		{
		Session::flash("incorrecto","Error de códigos, intenta registrarte nuevamente");	
		return view('auth.loginyes');			
		}
	}catch(\Exception $e){
	Session::flash("incorrecto", " 
	Tu correo ".$Correo." ya se encuentra registrado, 
	¿Olvidaste tu contraseña?
	");
	return view('auth.loginyes');
	}				
		//return view("Clientes.postLogin",compact("valida","Nombre","Teléfono","Correo","Address"));
    }
	//Recuperar contraseña
	public function recuperar()
    {
		extract($_POST);
		//dd($_POST);
		$valida=  str_random(20);
		$PasswordN =bcrypt($valida);
		if($Correo1 == $Correo2)
		{

        DB::table('users')->where('email', $Correo1)->update(['password'=>$PasswordN]);		
		$CorreoR= "info@info.com";
		$NombreR= "info";
		//dd($Correo);
		Mail::send("Clientes.forget",["valida"=>$valida], function($message) use ($CorreoR,$Correo1,$NombreR)
	       {

	           $message->from($CorreoR,$NombreR)->subject("Código de Verificación");
                   //$message->cc(["asanchez@tecnoquimicas.com","departamentodeseguridad@tecnoquimicas.com","seguridad@tecnoquimicas.com"]);
	           $message->to($Correo1); 
                   $message->subject("Código de Verificación");
	      });	
	
		Session::flash("correcto","Ingresa tu correo ".$Correo1." y como contraseña la que te hemos enviado al correo");	
		return view('auth.loginyes');

		}else{
		Session::flash("incorrecto", " 
		Tu correo ".$Correo1." empraje con la repetición, 
		¿Son correctos los ingresados?
		");		
		}
    }
	//Mostrar todos los cliente
    public function index()
    {
		$ver= "Activo";
		extract($_GET);
		$ver= $ver;		
    	$usuarios = User::join("personal","personal.id_personal","=","users.id_personal")
		->join("clientes","users.id","=","clientes.vendedor")
		->get();
		$vendedores = User::join("personal","users.id_personal","=","personal.id_personal")
		->get();		
		//dd($vendedores);
    	return view("Clientes.indexUsuario",compact("vendedores","usuarios","ver"));
    }	
	//Cambiar estado del cliente
    public function cambiarestado()
    {
    	extract($_GET);
    	$user = Cliente::find($id);
    	if($user->estado == "Activo")
    	{
    		$user->estado = "Inactivo";	
    	}else
    	{
    		$user->estado = "Activo";
    	}
    	Session::flash("correcto",$user->nombre_clientes." Ahora es ".$user->estado);
    	$user->save();
    	return redirect("Cliente/index");
    }
    //Eliminar cliente
    public function eliminar(){
        extract($_GET);
        DB::table('clientes')->where("id",$id)->delete();
        return redirect("Cliente/index")->with("correcto","Eliminado Correctamente");
    }	
	//Editar datos del cliente
    public function editar()
    {
    	extract($_GET);
    	$usuarios = Cliente::where("id","=",$id)->get();
		$vendedores = User::join("personal","users.id_personal","=","personal.id_personal")
		->get();		
    	return view("Clientes.editar",compact("usuarios","vendedores"));
    }
	//Actualizar datos del cliente
    public function update(){
        extract($_POST);
		$usuarios = Cliente::find($Id);
        DB::table('clientes')->where('id', $Id)->update(['nombre_clientes'=>$Nombre,'correo_cliente'=>$Correo,'tel'=>$Teléfono,'address'=>$Address,'vendedor'=>$PersonalID]);
        DB::table('users')->where('cuota', $Id)->update(['name'=>$Nombre,'email'=>$Correo]);		
        return redirect("Cliente/index")->with("correcto",$usuarios->nombre_clientes." Se Actualizó");
    }
	
}
