<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Finanza;
use App\Personal;
use App\User;
use App\Empresa;
use Session;
use DB;
use PDF;
use NumeroALetras;
class FinanzasController extends Controller
{
   //Añadir datos del registro del gasto
	public function postregistro()
    {
        extract($_POST);
        $usuario = new Finanza;
        $usuario->id_personal=$PersonalID;
        $usuario->concepto=$Concepto;
		$usuario->valor=$Valor;
		$Id = Finanza::max("id_gastos");

		if (isset($_FILES['Comprobante'])){
            $file = $_FILES["Comprobante"];
            if($file["name"] == null){
                $carpeta = "images/comprobantes/";
                $src = "images/default.jpg";
            }else{
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/comprobantes/".$Id;
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
				}
		$usuario->comprobante=$src;
        }		

        $usuario->save();
			Session::flash("correcto",$usuario->concepto." Se registró");
		return redirect("Finanza/index");
    }
	//Mostrar todos los gastos
    public function index()
    {
		$ver= "Activo";
		extract($_GET);
		$ver= $ver;	
    	$usuarios = Finanza::join("users","users.id","=","gastos.id_personal")
		->join("personal","personal.id_personal","=","users.id_personal")
		->get();
		$vendedores = User::join("personal","users.id_personal","=","personal.id_personal")
		->get();
		//dd($usuarios);
    	return view("Finanzas.indexUsuario",compact("vendedores","usuarios","ver"));
    }
	//Mostrar todos los vendedores
    public function listar()
    {
    	$usuarios = User::join("personal","users.id_personal","=","personal.id_personal")
		->join("plantilla","plantilla.id_personal","=","personal.id_personal")
		->get();
		//dd($usuarios);
		$personal = Personal::all();
		$info = Empresa::all();
    	return view("Finanzas.listar",compact("usuarios","personal","info"));
    }
	//Mostrar quincenal
    public function reservado()
    {
    	$usuarios = User::join("personal","users.id_personal","=","personal.id_personal")
		->join("plantilla","plantilla.id_personal","=","personal.id_personal")
		->get();
		//dd($usuarios);
		$personal = Personal::all();
		$info = Empresa::all();	
    	return view("Finanzas.reservado",compact("usuarios","personal","info"));
    }	
	//Editar datos del gasto
    public function editar()
    {
    	extract($_GET);
    	$usuarios = Finanza::where("id_gastos","=",$id)->get();
		$personal = Personal::all();
    	return view("Finanzas.editar",compact("usuarios","personal"));
    }
	//Actualizar datos del gasto
    public function update(){
		
        extract($_POST);		
		if (isset($_FILES['Comprobante'])){
            $file = $_FILES["Comprobante"];
            if($file["name"] == null){
                $src = $Comprobante_ruta;
            }else{
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/comprobantes/".$Id;
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
        }
		$srccer=$src;
		}
        DB::table('gastos')->where('id_gastos', $Id)->update(['concepto'=>$Concepto,'comprobante'=>$srccer,'valor'=>$Valor]);
        return redirect("Finanza/index")->with("correcto"," Se Actualizó");
    }
	//Actualizar datos de pagos
    public function updateCuota(){
		
        extract($_POST);		
		
        DB::table('plantilla')->where('id_plantilla', $Id)->update(['comisiones'=>$Comisioon,'otros'=>$Otros,'arl_categoria'=>$ARL,'contrato'=>$contrato]);
        return redirect("Finanza/listar")->with("correcto"," Se Actualizó");
    }
	
	public function pdf()
          {	
		  try{
               extract($_POST);
			   //dd($_POST);
			   $info = Empresa::all();
			   //$días= date($FechaFinal)-date($FechaInicio);
			   $date1=date_create($FechaInicio);//date_create("2018-01-21");
			   //dd($date1);
				$date2=date_create($FechaFinal);//date_create("2018-02-02");
				$días=date_diff($date1,$date2);
				$días= $días->format("%a");
			   //dd($días->format("%a"));
			   //if($días  < 0 ) $días= 0;
	   
               $letras = "DIEZ";//NumeroALetras::convertir($facturas[0]->total_venta);
               return PDF::loadView('Finanzas.pdfhtml',compact("días","letras","info","FechaInicio","Nombre","Sueldo","FechaFinal","Cargo","Contrato","Pensión","Salud","Auxilio","Comisiones","Otros_Pagos"))->stream('Desprendible.pdf'); 
				}catch(\Exception $e){
				Session::flash("incorrecto", " 
				¿Accediste correctamente al proceso?
				");
				return redirect("Finanza/listar");
				}
          }	

	public function pdf_todos()
          {	

               extract($_POST);
			   //dd($_POST);
			   $info = Empresa::all();
			   //$días= date($FechaFinal)-date($FechaInicio);
			   $date1=date_create($FechaInicio);//date_create("2018-01-21");
			   //dd($date1);
				$date2=date_create($FechaFinal);//date_create("2018-02-02");
				$días=date_diff($date1,$date2);
				$días= $días->format("%a");

               $letras = "DIEZ";//NumeroALetras::convertir($facturas[0]->total_venta);
			   
			   $arrayNombre = array();
			   $arraySueldo = array();
			   $arrayCargo = array();
			   $arrayContrato = array();
			   $arrayPensión = array();
			   $arraySalud = array();
			   $arrayAuxilio = array();
			   $arrayComisiones = array();
			   $arrayOtros_Pagos = array();
			   
				$usuarios = User::join("personal","users.id_personal","=","personal.id_personal")
				->join("plantilla","plantilla.id_personal","=","personal.id_personal")
				->get();
				
				foreach($usuarios as $elemento)
				{
					if($elemento->user_estado == "Activo" && $elemento->name != "admin" && $elemento->rol != "Cliente" )
					{
				   $arrayNombre[] = $elemento->nombre;
				   $arraySueldo[] = $elemento->valor_a_pagar;
				   $arrayCargo[] = $elemento->cargo;
				   if($elemento->contrato == 0) $arrayContrato[] = "Prestación de Servicios";
				   else $arrayContrato[] = "Labor Contratada";
				   $arrayPensión[] = round(($elemento->valor_a_pagar+$elemento->comisiones+$elemento->otros)*(0.16-$info[0]->pensioon/100));
				   $arraySalud[] = round(($elemento->valor_a_pagar+$elemento->comisiones+$elemento->otros)*(0.125-$info[0]->salud/100));
				   if($elemento->contrato == 0) $arrayAuxilio[] = 0;
				   else $arrayAuxilio[] = $info[0]->auxilio;
				   $arrayComisiones[] = $elemento->comisiones;
				   $arrayOtros_Pagos[] = $elemento->otros;
				   }
				}
				//dd($arrayNombre);
               //return PDF::loadView('Finanzas.desprendibles',compact("días","letras","info","FechaInicio","arrayNombre","arraySueldo","FechaFinal","arrayCargo","arrayContrato","arrayPensión","arraySalud","arrayAuxilio","arrayComisiones","arrayOtros_Pagos"))->stream('Desprendible.pdf'); 
			   $pdf = \App::make('dompdf.wrapper');
			   //PDF::loadView('Finanzas.desprendibles',compact("días","letras","info","FechaInicio","arrayNombre","arraySueldo","FechaFinal","arrayCargo","arrayContrato","arrayPensión","arraySalud","arrayAuxilio","arrayComisiones","arrayOtros_Pagos"))->save('/path//my_stored_file.pdf');
        $pdf->loadView('Finanzas.desprendibles',compact("días","letras","info","FechaInicio","arrayNombre","arraySueldo","FechaFinal","arrayCargo","arrayContrato","arrayPensión","arraySalud","arrayAuxilio","arrayComisiones","arrayOtros_Pagos"));
        //$pdf->save(asset('/images/pdf.pdf'));
		return $pdf->stream();

          }	
	
}
