<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Personal;
use Session;
use DB;
class UsuarioController extends Controller
{
	//Mostrar todos los usuarios
    public function listar()
    {
		$ver= "Activo";
		extract($_GET);
		$ver= $ver;
    	$users = User::all();
    	return view("Usuarios.listarusuario",compact("users","ver"));
    }
	//Editar usuario
    public function editar()
    {
    	extract($_GET);
    	$user = User::where("id","=",$id)->join("personal","personal.id_personal","=","users.id_personal")->get();
    	return view("Usuarios.editar",compact("user","personal"));
    }
	//Actualizar usuario
    public function update(){
        extract($_POST);
		$usuarios = User::find($Id);
        DB::table('users')->where('id', $Id)->update(['name'=>$name,'rol'=>$rol,'email'=>$email,'cuota'=>$Cuota]);
        DB::table('personal')->where('id_personal', $Id)->update(['nombre'=>$name,'tel'=>$Teléfono,'address'=>$address,'correo'=>$email,'cargo'=>$rol,'valor_a_pagar'=>$Salario]);		
        return redirect("usuario/listar")->with("correcto",$usuarios->name." Se Actualizó");
    }
	//Actualizar contraseña usuario
    public function updatepassword(){
        extract($_POST);
		$PasswordN =bcrypt($password);
		$usuarios = User::find($Id);
        DB::table('users')->where('id', $Id)->update(['password'=>$PasswordN]);
        return redirect("home")->with("correcto",$usuarios->name." Se Actualizó");
    }
	//Actualizar imagen de usuario
    public function updatepic(){
        extract($_POST);
		//dd($_POST);
		if (isset($_FILES['Pic'])){
            $file = $_FILES["Pic"];
            if($file["name"] == null){
                //$carpeta = "images/providers/";
               $src = $Pic_ruta;
            }else{
                $nom = $Id;//$file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/personal/".$Id;
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
				}
		$srcpic=$src;
        }
		
		$usuarios = User::find($Id);
        DB::table('users')->where('id', $Id)->update(['pic'=>$srcpic]);
        return redirect("home")->with("correcto",$usuarios->name." Se Actualizó");
    }		
	//Guardar datos de registro nuevo
    public function postregistrar()
    {
try{
    	//Se traen los datos del post del formulario
    	extract($_POST);
    	//Registro Personal
    	$personal = new Personal;
    	$personal->nombre = $name;
		$personal->tel = $Teléfono;
    	$personal->correo = $email;
    	$personal->address = $address;
    	$personal->cargo = $rol;
    	$personal->valor_a_pagar = $Salario;   
    	$personal->save();
    	//Registro Usuario
		$id_personal = Personal::max("id_personal");
    	$user = new User;
    	$user->name = $name;
    	$user->id_personal = $id_personal;
    	$user->email = $email;
    	$user->rol = $rol;
		$user->cuota = $Cuota;
		$user->remember_token= $_token;		
		$user->pic = "images/defaultPersonal.png";
    	$user->user_estado = $user_estado;
    	$user->password =bcrypt($password);
    	$user->save();
		
		DB::table('plantilla')->insert(['id_personal'=>$id_personal,'contrato'=>1,'arl_categoria'=>1,'auxilio'=>1,'comisiones'=>0,'otros'=>0,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
		
		Session::flash("correcto",$user->name." Está ahora registrado");
    	
    	return redirect("usuario/listar");
	
}catch(\Exception $e){
Session::flash("incorrecto", " 
Revisa los datos que almacenarás en la base de datos, 
¿el correo que almacenarás ya existe?. ".$email." 
¿Haz llenado todos los campos?
");
return redirect("usuario/listar");
}

}
	

	//Cambiar estado del usuario
    public function cambiarestado()
    {
    	extract($_GET);
    	$user = User::find($id);	
		if($user->id == 1)
		{    	Session::flash("incorrecto",$user->name." No se le debe desactivar");		}
		else
		{
    	if($user->user_estado == "Activo")
    	{
    		$user->user_estado = "Inactivo";	
    	}else
    	{
    		$user->user_estado = "Activo";
    	}
    	Session::flash("correcto",$user->name." ahora es ".$user->user_estado);
    	$user->save();
		}
    	return redirect("usuario/listar");
		
    }
	

}
