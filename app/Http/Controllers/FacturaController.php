<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Factura;
use App\Pedido;
use App\User;
use App\Cliente;
use App\PT;
use App\Empresa;
use Session;
use PDF;
use NumeroALetras;
use DB;
use Auth;
class FacturaController extends Controller
{
	function prodatos(){
		extract($_GET);
		$artículos = PT::where("id","=",$id)->get();
		$arr = array('impuesto' => $artículos[0]->impuesto, 'precio' => $artículos[0]->precio_unitario);
		return json_encode($arr);
	}
	//Registrar factura
    public function registrar()
    {
	$vendedores = User::join("personal","personal.id_personal","=","users.id_personal")
	//->join("users","users.id_personal","=","personal.id_personal")
	->get();
	//dd($vendedores);
	$clientes = Cliente::all();
	$artículos = PT::all();
	//dd($artículos);
	$info = Empresa::all();
	$Id = $info[0]->factura + Factura::max("id_facturas")+1;
	$Id = str_pad( $Id, 4, "0", STR_PAD_LEFT );
	$detalle= DB::table('detalle_factura')
	->join("artiiculos","detalle_factura.id_producto","=","artiiculos.id")
	->where('num_factura',$Id)
	->select('detalle_factura.*','artiiculos.*')
	->get();
	//dd($detalle);
	//$tasks = \App\Task::all();
	
	return view("Facturas.crear_factura",compact("vendedores","clientes","Id","artículos","detalle","tasks"));
	}
	//pedidos a la empresa
    public function pedidos()
    {
    	extract($_GET);
		$artículos = PT::all();
    	$info = Empresa::all();
		$cliente = User::
		join("clientes","users.cuota","=","clientes.id")
		->where("users.id",Auth::user()->id)
		->get();
		//if ($cliente[0])
		//dd(count($cliente) > 0);
		if (count($cliente) > 0)$Cliente= $cliente[0]->id;
		else $Cliente= "2";
		//dd($cliente);
    	return view("Empresas.pedidos",compact("info","artículos","Id","Cliente"));
    }
   //Añadir datos del registro del factura
	public function postpedido()
    {
        extract($_POST);
		if($Cliente != 0){		
		//dd($farticulo[0]);
		$Id = Pedido::max("id_pedidos")+1;
		$Num = str_pad( $Id, 4, "0", STR_PAD_LEFT );		
		$idARTS= (explode(",",$farticulo[0]));
		$cantidadARTS= (explode(",",$fcantidad[0]));
		$impuestoARTS= (explode(",",$fimpuesto[0]));
		$totalARTS= (explode(",",$fbruto[0]));
		for ($i = 0; $i < count($idARTS); ++$i)
		{
		$detalle= DB::table('detalle_pedidos')->where('num_pedido',$Num)->get();
		//dd($detalle);
		$selogra= 0;
		foreach ($detalle as $objeto)
		{
			if ($objeto->id_producto == $idARTS[$i])
				{
				$oldcantidad= DB::table('detalle_pedidos')->where('num_pedido',$Num)->where('id_producto',$idARTS[$i])->get();
				//dd($oldcantidad[0]->cantidad);
				DB::table('detalle_pedidos')->where('num_pedido',$Num)->where('id_producto',$idARTS[$i])->update(['cantidad'=>$cantidadARTS[$i] + $oldcantidad[0]->cantidad]);
				$selogra= 1;
				}
		}
		if ($selogra==0)
			{
			DB::table('detalle_pedidos')->insert(['num_pedido'=>$Num,'id_producto'=>$idARTS[$i],'cantidad'=>$cantidadARTS[$i],'impuesto'=>$impuestoARTS[$i],'precio_venta'=>$totalARTS[$i]]);
			}
		}
		
        $factura = new Pedido;
        $factura->num_pedido=$Id;
        $factura->id_cliente=$Cliente;
		//dd($Cliente);
		$factura->tipo_pago=$pago;
		if ($pago == "Efectivo"){$factura->total_venta=$fgrantotal;}
		elseif ($pago != "Efectivo"){$factura->total_venta=$fgrantotal;}
		
		$factura->estado_factura= "Pendiente";
		$Fecha= date('d/m/Y');
		$factura->fecha=$Fecha;

        if($factura->save() == true){
			Session::flash("correcto"," Se ha registrado el pedido");
        }else{
			Session::flash("incorrecto","¡Upss! Algo pasó en el registro, inténtalo nuevamente");
        };
		}
		return redirect("Factura/pedidos");
    }	
   //Añadir datos del registro del factura
	public function postregistro()
    {
        extract($_POST);
		
		//dd($_POST);
		//dd($farticulo[0]);
		$idARTS= (explode(",",$farticulo[0]));
		$cantidadARTS= (explode(",",$fcantidad[0]));
		$impuestoARTS= (explode(",",$fimpuesto[0]));
		$totalARTS= (explode(",",$fbruto[0]));
		for ($i = 0; $i < count($idARTS); ++$i)
		{		
		$detalle= DB::table('detalle_factura')->where('num_factura',$Num)->get();
		//dd($detalle);
		$selogra= 0;
		foreach ($detalle as $objeto)
		{
			if ($objeto->id_producto == $idARTS[$i])
				{
				$oldcantidad= DB::table('detalle_factura')->where('num_factura',$Num)->where('id_producto',$idARTS[$i])->get();
				//dd($oldcantidad[0]->cantidad);
				DB::table('detalle_factura')->where('num_factura',$Num)->where('id_producto',$idARTS[$i])->update(['cantidad'=>$cantidadARTS[$i] + $oldcantidad[0]->cantidad]);
				$selogra= 1;
				}
		}
		if ($selogra==0)
			{
			DB::table('detalle_factura')->insert(['num_factura'=>$Num,'id_producto'=>$idARTS[$i],'cantidad'=>$cantidadARTS[$i],'impuesto'=>$impuestoARTS[$i],'precio_venta'=>$totalARTS[$i]]);
			}
		}
		
        $factura = new Factura;
        $factura->num_factura=$Id;
        $factura->id_clientes=$Clientes;
        $factura->id_vendedor=$Vendedores;
		$factura->tipo_pago=$pago;
		$factura->ciudad=$Ciudad;
		if ($pago == "Efectivo"){$factura->total_venta=$fgrantotal;}
		elseif ($pago != "Efectivo"){$factura->total_venta=$fgrantotal;}
		$factura->total_cuota=0;
		$factura->ciudad=$Ciudad;
		
		$factura->estado_factura= "Pendiente";
		$factura->fecha=$Fecha;

        if($factura->save() == true){
			Session::flash("correcto",$factura->nombre." Está ahora registrado");
        }else{
			Session::flash("incorrecto","¡Upss! Algo pasó en el registro, inténtalo nuevamente");
        };
		
		return redirect("Factura/index");
    }
	//Agregar datos de artículos a factura
	public function agregar()
    {
        extract($_GET);
		//dd($_GET);
		$detalle= DB::table('detalle_factura')->where('num_factura',$num)->get();
		//dd($detalle);
		$selogra= 0;
		foreach ($detalle as $objeto)
		{
			if ($objeto->id_producto == $Id)
				{
				$oldcantidad= DB::table('detalle_factura')->where('num_factura',$num)->where('id_producto',$Id)->get();
				//dd($oldcantidad[0]->cantidad);
				DB::table('detalle_factura')->where('num_factura',$num)->where('id_producto',$Id)->update(['cantidad'=>$Cantidad + $oldcantidad[0]->cantidad]);
				$selogra= 1;
				}
		}
		if ($selogra==0)
			{
			DB::table('detalle_factura')->insert(['num_factura'=>$num,'id_producto'=>$Id,'cantidad'=>$Cantidad,'precio_venta'=>$Precio]);
			}
		return redirect("Factura/registrar");
	}
	//eliminar datos de artículos a factura
	public function eliminardetalle()
    {
        extract($_GET);
		//dd($_GET);
		DB::table('detalle_factura')->where("id_detalle",$Id)->delete();
		return redirect("Factura/registrar");
	}
	//Agregar datos de artículos a factura de editar
	public function agregardeeditar()
    {
        extract($_GET);
		//dd($_GET);
		$detalle= DB::table('detalle_factura')->where('num_factura',$num)->get();
		//dd($detalle);
		$selogra= 0;
		foreach ($detalle as $objeto)
		{
			if ($objeto->id_producto == $Id)
				{
				$oldcantidad= DB::table('detalle_factura')->where('num_factura',$num)->where('id_producto',$Id)->get();
				//dd($oldcantidad[0]->cantidad);
				DB::table('detalle_factura')->where('num_factura',$num)->where('id_producto',$Id)->update(['cantidad'=>$Cantidad + $oldcantidad[0]->cantidad]);
				$selogra= 1;
				}
		}
		if ($selogra==0)
			{
			DB::table('detalle_factura')->insert(['num_factura'=>$num,'id_producto'=>$Id,'cantidad'=>$Cantidad,'precio_venta'=>$Precio]);
			}
		$valor= DB::table('facturas')->where('id_facturas', $num)->get();
		//dd($valor);		
		DB::table('facturas')->where('id_facturas', $num)->update(['total_venta'=>$valor[0]->total_venta+$Precio*$Cantidad]);
			
		return redirect("Factura/editar?id=".$num);
	}
	//eliminar datos de artículos a factura de editar
	public function eliminardetalledeeditar()
    {
        extract($_GET);
		//dd($_GET);
		DB::table('detalle_factura')->where("id_detalle",$Id)->delete();
		$valor= DB::table('facturas')->where('id_facturas', $num)->get();
		//dd($valor);		
		DB::table('facturas')->where('id_facturas', $num)->update(['total_venta'=>$valor[0]->total_venta-$precio]);
		return redirect("Factura/editar?id=".$num);
	}	
   //generar pago de cuota
	public function cuota()
    {
        extract($_POST);
		//dd($_POST);
		if($Cantidad <= $Adeudado)
			{
			$cuota= DB::table('facturas')->where('id_facturas', $Id)->get();
			DB::table('facturas')->where('id_facturas', $Id)->update(['total_venta'=>$Adeudado-$Cantidad,'total_cuota'=>$cuota[0]->total_cuota+$Cantidad]);
			
			DB::table('reporte')->insert([
			'fecha'=>date('d/m/Y'),'tipo'=>"Factura-Cuota",'valor'=>$Cantidad,'detalle'=>$Id,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')
			]);
			
			return redirect("Factura/index")->with("correcto","Cuota´para el crédito ligada");
			}
		else
			{
			return redirect("Factura/index")->with("incorrecto","La cuota no debe de ser mayor a lo adeudado");
			}
			
    }	
	//Mostrar todos las factura
    public function index()
    {
    	$clientes = Factura::join("clientes","clientes.id","=","facturas.id_clientes")
    	->join("users","users.id","=","facturas.id_vendedor")
		->join("personal","personal.id_personal","=","users.id_personal")
		->get();
		//dd($clientes);
    	return view("Facturas.indexUsuario",compact("clientes", "vendedores"));
    }	
	//Mostrar todos los pedidos
    public function pedidosLista()
    {
    	$clientes = Pedido::join("clientes","clientes.id","=","pedidos.id_cliente")
		->get();
		$vendedores = User::join("personal","personal.id_personal","=","users.id_personal")
		->get();
		//dd($clientes);
    	return view("Facturas.pedidosLista",compact("clientes", "vendedores"));
    }		
	//Cambiar estado del factura
    public function cambiarestado()
    {
    	extract($_GET);
    	//$user = Factura::find($id);
		$user= DB::table('facturas')->where("id_facturas",$id)->get();
		//dd($user[0]);
		$esposible= 0;
		$secambia= 0;
		if($user[0]->tipo_pago == "Efectivo")
			{
			if($user[0]->estado_factura == "Pendiente")
			{
			$user[0]->estado_factura = "Pagado";$secambia= 1;
			
			DB::table('reporte')->insert([
			'fecha'=>date('d/m/Y'),'tipo'=>"Factura-Pago",'valor'=>$user[0]->total_venta,'detalle'=>$user[0]->num_factura,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')
			]);
			
			}
			elseif($user[0]->estado_factura == "Pagado")
			{$user[0]->estado_factura = "Entregado";$secambia= 2;}			
			}
		if($user[0]->tipo_pago != "Efectivo")
			{
			if($user[0]->estado_factura == "Pendiente")
			{
			$user[0]->estado_factura = "Entregado";$secambia= 2;
			
			DB::table('reporte')->insert([
			'fecha'=>date('d/m/Y'),'tipo'=>"Factura-Crédito",'valor'=>$user[0]->total_venta,'detalle'=>$user[0]->num_factura,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')
			]);
			
			}
			}
		
		if ($secambia == 2)
			{
				$info = Empresa::all();
				$detalle= DB::table('detalle_factura')->where("num_factura",$info[0]->factura+ $id)
				->join("artiiculos","detalle_factura.id_producto","=","artiiculos.id")
				->join("inventario_artiiculos","detalle_factura.id_producto","=","inventario_artiiculos.id_artiiculos")
				->get();
				//dd($detalle);
				//verificar si se pueden descargar los artículos del inventario
				$validar= 0;
				$advertencias= "";				
				
				for($x=0; $x < count($detalle); $x++ )
				{
					if($detalle[$x]->cantidad > $detalle[$x]->disponible){
						$advertencias= $advertencias." De: ".$detalle[$x]->art_nombre." / se Piden ".$detalle[$x]->cantidad." Existen ".$detalle[$x]->disponible;
					}else{
						$validar++;
					}
				}
				
			//dd(count($detalle));
			if ($validar == count($detalle))
				{
					$secambia = 1;
					for($x=0; $x < count($detalle); $x++ )
					{
						DB::table('inventario_artiiculos')->where('id_artiiculos', $detalle[$x]->id_artiiculos)->update(['disponible'=>$detalle[$x]->disponible - $detalle[$x]->cantidad]);
					}
				}
			}
			
		if ($secambia == 1)
			{
			Session::flash("correcto",$user[0]->num_factura." Ahora es ".$user[0]->estado_factura);
			DB::table('facturas')->where('id_facturas', $id)->update(['estado_factura'=>$user[0]->estado_factura]);
			}
		else 
			{
			Session::flash("incorrecto","No hay suficiente cantidad ".$advertencias);
			}			
		
    	return redirect("Factura/index");
    }
	//Cambiar estado del factura
    public function cambiarestadopedido()
    {
    	extract($_GET);
    	//$user = Factura::find($id);
		$user= DB::table('pedidos')->where("id_pedidos",$id)->get();
		//dd($user[0]);
		$esposible= 0;
		$secambia= 0;
			if($user[0]->estado_factura == "Pendiente")
			{
			$user[0]->estado_factura = "Revisado";$secambia= 1;
			}
			elseif($user[0]->estado_factura == "Revisado")
			{$user[0]->estado_factura = "Aprobado";$secambia= 2;}			

		
		if ($secambia == 2)
			{
               $facturas = DB::table('pedidos')
			   ->join("clientes","clientes.id","=","pedidos.id_cliente")
               ->where('num_pedido','=',$id)
               ->get();
               $cont_factura = DB::table('detalle_pedidos')
			   ->join("artiiculos","artiiculos.id","=","detalle_pedidos.id_producto")
               ->where('num_pedido','=',$id)
               ->get();
			   //dd($cont_factura);
			$info = Empresa::all();
			$Id = $info[0]->factura + Factura::max("id_facturas")+1;
			$Id = str_pad( $Id, 4, "0", STR_PAD_LEFT );
			$factura = new Factura;
			$factura->num_factura=$Id;
			$factura->id_clientes=$user[0]->id_cliente;
			$factura->id_vendedor="6";//$Vendedores;
			$factura->tipo_pago=$user[0]->tipo_pago;
			$factura->ciudad="";//$Ciudad;			
			$factura->total_venta=$user[0]->total_venta;
			$factura->total_cuota=0;
			$factura->estado_factura= "Pendiente";
			$Fecha= date('d/m/Y');
			$factura->fecha=$Fecha;
			$factura->save();
			
			foreach ($cont_factura as $objeto)
			{
			DB::table('detalle_factura')->insert(['num_factura'=>$Id,'id_producto'=>$objeto->id_producto,'cantidad'=>$objeto->cantidad,'impuesto'=>$objeto->impuesto,'precio_venta'=>$objeto->precio_venta]);
			}
			DB::table('pedidos')->where('id_pedidos', $id)->update(['estado_factura'=>$user[0]->estado_factura]);
			Session::flash("correcto",$factura->nombre." Está ahora registrada como factura");
			return redirect("Factura/index");
			}
			
		if ($secambia == 1)
			{
			Session::flash("correcto",$user[0]->num_pedido." Ahora es ".$user[0]->estado_factura);
			DB::table('pedidos')->where('id_pedidos', $id)->update(['estado_factura'=>$user[0]->estado_factura]);
			return redirect("Factura/pedidosLista");
			}
		
    }
    //Eliminar factura
    public function eliminar(){
        extract($_GET);
        DB::table('clientes')->where("id",$id)->delete();
        return redirect("Factura/index")->with("correcto","Eliminado Correctamente");
    }	
	//Editar datos del factura
    public function editar()
    {
    	extract($_GET);
		$info = Empresa::all();
    	$Factura = Factura::where("id_facturas","=",$id)
		->join("users","users.id","=","facturas.id_vendedor")
		->join("personal","users.id_personal","=","personal.id_personal")		
		->join("clientes","clientes.id","=","facturas.id_clientes")		
		->get();
		$artículos = PT::all();
		$detalle= DB::table('detalle_factura')
		->join("artiiculos","detalle_factura.id_producto","=","artiiculos.id")
		->where('num_factura',$info[0]->factura + $id)
		->select('detalle_factura.*','artiiculos.*')
		->get();		
		//dd($Factura);
    	return view("Facturas.editar",compact("Factura","artículos","detalle"));
    }
	//Actualizar datos del factura
    public function update(){
        extract($_POST);
		$facturas = Factura::find($Id);
        DB::table('clientes')->where('id', $Id)->update(['nombre'=>$Nombre,'correo'=>$Correo,'tel'=>$Teléfono,'address'=>$Address]);
        return redirect("Factura/index")->with("correcto",$facturas->nombre." Se Actualizó");
    }
	
	public function showDate(Request $request)
    {
       dd($request->date);
    }	
	
	public function pdf()
          {
               extract($_GET);
			   //dd($_GET);
               $facturas = DB::table('facturas')
			   ->join("personal","personal.id_personal","=","facturas.id_vendedor")
			   ->join("clientes","clientes.id","=","facturas.id_clientes")
               ->where('num_factura','=',$num_factura)
               ->get();
               $cont_factura = DB::table('detalle_factura')
			   ->join("artiiculos","artiiculos.id","=","detalle_factura.id_producto")
               ->where('num_factura','=',$num_factura)
               ->get();
			   //dd($cont_factura);
			   $info = Empresa::all();
			   //dd($facturas);
			   //dd($info[0]->pic);
               $letras = NumeroALetras::convertir($facturas[0]->total_venta);
               //$letras = "00";
			   if($ID==1)
			   {
               return PDF::loadView('Facturas.pdfhtml',compact("facturas","letras","info","cont_factura"))->stream('Factura'.str_pad( $num_factura, 4, "0", STR_PAD_LEFT ).'.pdf'); 
			   }
			   elseif($ID==0)
			   {
               return PDF::loadView('Facturas.pdfhtml',compact("facturas","letras","info","cont_factura"))->download('Factura'.str_pad( $num_factura, 4, "0", STR_PAD_LEFT ).'.pdf');
			   }			   
               //return PDF::loadView('Facturas.pdfhtml',compact("facturas","letras","info"))->stream('Factura'.$num_factura.'.pdf');			   
			   //download
          }
		  
	public function pdfpedido()
          {
               extract($_GET);
			   //dd($_GET);
               $facturas = DB::table('pedidos')
			   ->join("clientes","clientes.id","=","pedidos.id_cliente")
               ->where('num_pedido','=',$num_pedido)
               ->get();
               $cont_factura = DB::table('detalle_pedidos')
			   ->join("artiiculos","artiiculos.id","=","detalle_pedidos.id_producto")
               ->where('num_pedido','=',$num_pedido)
               ->get();
			   //dd($cont_factura);
			   $info = Empresa::all();
			   //dd($facturas);
			   //dd($info[0]->pic);
               $letras = NumeroALetras::convertir($facturas[0]->total_venta);
               //$letras = "00";
               return PDF::loadView('Facturas.pdfhtmlpedidos',compact("facturas","letras","info","cont_factura"))->stream('Factura'.str_pad( $num_pedido, 4, "0", STR_PAD_LEFT ).'.pdf'); 
          }
		  
	public function facturero()
          {

		$Lista= Factura::all();
		$info = Empresa::all();
		$arrayD = array();
		foreach($Lista as $Elemento)
		{
			//dd($Elemento->num_factura);
               $facturas = DB::table('facturas')
			   ->join("personal","personal.id_personal","=","facturas.id_vendedor")
			   ->join("clientes","clientes.id","=","facturas.id_clientes")
               ->where('num_factura','=',$Elemento->num_factura)
               ->get();
               $cont_factura = DB::table('detalle_factura')
			   ->join("artiiculos","artiiculos.id","=","detalle_factura.id_producto")
               ->where('num_factura','=',$Elemento->num_factura)
               ->get();
               $letras = NumeroALetras::convertir($facturas[0]->total_venta);
			   $arrayD[] = [$facturas,$cont_factura,$letras];
		}
		//dd($arrayD);
               return PDF::loadView('Facturas.facturero',compact("arrayD","info"))->stream('Facturero.pdf'); 		   
        }
	
}
