<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Provider;
use Session;
use DB;
class ProvidersController extends Controller
{
	//Registrar proveedor
    public function registrar()
    {return view("Providers.crear_usuario");}
   //Añadir datos del registro del proveedor
	public function postregistro()
    {
	try{
        extract($_POST);
        $usuario = new Provider;
        $usuario->empresa=$Empresa;
        $usuario->tel=$Teléfono;
        $usuario->correo=$correo;
		$usuario->address=$Adress;
        $usuario->nit=$Nit;
		$Id = Provider::max("id");
		
		if (isset($_FILES['CámaraC'])){
            $file = $_FILES["CámaraC"];
            if($file["name"] == null){
                $carpeta = "images/providers/";
                $src = "images/default.jpg";
            }else{
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/providers/".$Id;
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
				}
		$usuario->camara=$src;
        }
		
		if (isset($_FILES['Certificado'])){
            $file = $_FILES["Certificado"];
            if($file["name"] == null){
                $carpeta = "images/providers/";
                $src = "images/default.jpg";
            }else{
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/providers/".$Id;
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
        }				
        $usuario->certificado=$src;	
		}
		
		if (isset($_FILES['Rut'])){
            $file = $_FILES["Rut"];
            if($file["name"] == null){
                $carpeta = "images/providers/";
                $src = "images/default.jpg";
            }else{
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/providers/".$Id;
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
        }
        $usuario->rut=$src;
		}
		
				
        $usuario->estado="Activo";

        if($usuario->save() == true){
			Session::flash("correcto",$usuario->empresa." Está ahora registrado");
        }else{
			Session::flash("incorrecto","¡Upss! Algo pasó en el registro, inténtalo nuevamente");
        };
		return redirect("Providers/index");
	}catch(\Exception $e){
	Session::flash("incorrecto", " 
	Revisa los datos que almacenarás en la base de datos, 
	¿el correo que almacenarás ya existe?. ".$correo." 
	¿Haz llenado todos los campos?
	");
	return redirect("Providers/index");
	}
    }
	//Mostrar todos los proveedores
    public function index()
    {
		$ver= "Activo";
		extract($_GET);
		$ver= $ver;	
    	$usuarios = Provider::all();
    	return view("Providers.indexUsuario",compact("usuarios","ver"));
    }	
	//Cambiar estado del proveedor
    public function cambiarestado()
    {
    	extract($_GET);
    	$user = Provider::find($id);
    	if($user->estado == "Activo")
    	{
    		$user->estado = "Inactivo";	
    	}else
    	{
    		$user->estado = "Activo";
    	}
    	Session::flash("correcto",$user->empresa." Ahora es ".$user->estado);
    	$user->save();
    	return redirect("Providers/index");
    }
    //Eliminar Proveedor
    public function eliminar(){
        extract($_GET);
        DB::table('proveedores')->where("id",$id)->delete();
        return redirect("Providers/index")->with("correcto","Eliminado Correctamente");
    }	
	//Editar datos del proveedor
    public function editar()
    {
    	extract($_GET);
    	$usuarios = Provider::where("id","=",$id)->get();
    	return view("Providers.editar",compact("usuarios"));
    }
	//Actualizar datos del proveedor
    public function update(){
	//dd($_FILES['CámaraC']);
	//echo '<script language="javascript">alert("ref");</script>';
        extract($_POST);
	//dd($CámaraC_ruta);		
		if (isset($_FILES['CámaraC'])){
            $file = $_FILES["CámaraC"];
            if($file["name"] == null){
                //$carpeta = "images/providers/";
               $src = $CámaraC_ruta;
            }else{
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/providers/".$Id;
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
				}
		$srccam=$src;
        }
		
		if (isset($_FILES['Certificado'])){
            $file = $_FILES["Certificado"];
            if($file["name"] == null){
               // $carpeta = "images/providers/";
                $src = $Certificado_ruta;
            }else{
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/providers/".$Id;
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
        }
		$srccer=$src;
		}
		
		if (isset($_FILES['Rut'])){
            $file = $_FILES["Rut"];
            if($file["name"] == null){
               // $carpeta = "images/providers/";
                $src = $Rut_ruta;
            }else{
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/providers/".$Id;
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
        }
		$srcrut=$src;
		}		
		
		$usuarios = Provider::find($Id);
        DB::table('proveedores')->where('id', $Id)->update(['empresa'=>$Empresa,'correo'=>$Correo,'tel'=>$Teléfono,'address'=>$Address,'nit'=>$Nit,'camara'=>$srccam,'certificado'=>$srccer,'rut'=>$srcrut]);
        return redirect("Providers/index")->with("correcto",$usuarios->empresa." Se Actualizó");
    }
	
    public function cargar(){
	extract($_GET);
	dd("Certificado");
		if (isset($_FILES['Certificado'])){
            $file = $_FILES["Certificado"];
            if($file["name"] == null){
               // $carpeta = "images/providers/";
               // $src = $carpeta . "default.jpg";
            }else{
                $nom = $file["name"];
                $dir_temp = $file["tmp_name"];
                $carpeta = "images/providers/".$Id;
                $src = $carpeta . $nom;
                move_uploaded_file($dir_temp, $src);
        }
		$srccer=$src;
		}	
	
	DB::table('proveedores')->where('id', $id)->update(['certificado'=>$srccer]);
	
	echo $src;
    }	
	
}
