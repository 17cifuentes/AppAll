<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Redirect;
use Session;
class rol{
protected $auth;
    public function __construct(Guard $auth){
        
        $this->auth=$auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd($this->auth->user()->rol );
        if($this->auth->user()->user_estado == "Activo")
        {
        if($this->auth->user()->rol == "Administrador")
        {
		//session(['nombre' => 'JeffA']);
            return redirect()->to("rol/administrador");

        }else if($this->auth->user()->rol == "Almacenista")
        {
			session(['nombre' => 'JeffB']);
            return redirect()->to("rol/almacenista");
        }
        else if($this->auth->user()->rol == "Operario")
        {
            return redirect()->to("rol/operario");
        }else if($this->auth->user()->rol == "Vendedor")
        {
            return redirect()->to("rol/vendedor");
        }else if($this->auth->user()->rol == "R.Humanos")
        {
            return redirect()->to("rol/rhumanos");
        }else
        {
               return $next($request);
        }
        
        }else{
               return $next($request);
        }



               
    }
}
