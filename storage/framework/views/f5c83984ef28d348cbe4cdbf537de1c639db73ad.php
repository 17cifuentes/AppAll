<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
 
$total_1N= 0; 
$total_2N=0; 
$total_3N= 0;

?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[16] == '1'): ?>  
    <div class="col-md-12">
      <div class="card">
       <div class="card-header" style="overflow-x:auto;">
		<table class="table" width="100%" cellspacing="0" >
		<thead>
			<tr>
            <th ><p style="font-size:50px">Gestión de Nómina</th>
            <th align="center" valign="middle">
			<a href="<?php echo e(url('Finanza/reservado')); ?>" class="btn btn-success" title="Ver nómina quincenal" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Quincenal</a>
			</th>			
			
				</tr>
			</thead>	
			</table>	
				</div>
		<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
		<thead>
            <tr>
              <th>NOMBRE</th>
			  <th>Cargos</th>
              <th align="center">Salario</th>
			  <th align="center">Contrato</th>
			  <th align="center"><?php echo e($info[0]->pensioon); ?>% Pensión $</th> <!-- SEGURIDAD SOCIAL         -->
			  <th align="center"><?php echo e($info[0]->salud); ?>% Salud $</th> <!-- SEGURIDAD SOCIAL         -->
			  <th align="center">4% Caja de Compensación $</th> <!-- COMPENSACIÓN FAMILIAR         -->			  
			  <th align="center">ARL $</th> <!-- 0.52% 1.04% 2.44% 4.35% 6.96%         -->
			  <th align="center"><?php echo e($info[0]->cesantiia); ?>% Cesantía $</th> <!-- PROVISIÓN MENSUAL PRESTACIONES SOCIALES         -->
			  <th align="center"><?php echo e($info[0]->intereses); ?>% Intereses Cesantía $</th> <!-- PROVISIÓN MENSUAL PRESTACIONES SOCIALES         -->
			  <th align="center"><?php echo e($info[0]->prima); ?>% Prima $</th> <!-- PROVISIÓN MENSUAL PRESTACIONES SOCIALES         -->
			  <th align="center"><?php echo e($info[0]->vacaciones); ?>% Vacaciones $</th> <!-- PROVISIÓN MENSUAL PRESTACIONES SOCIALES         -->
			  <th align="center">Auxilio de Transporte $</th>
			  <th align="center">Comisiones $</th>
			  <th align="center">Otros Pagos $</th>
			  <th align="center">Total Seguridad Social Mensual</th>			  
			  <th align="center">RESERVA PROVISIÓN MENSUAL PRESTACIONES SOCIALES</th>
			  <th align="center">Costo Total sin Provisión</th>
<?php if($permisos[17] == '1'): ?> 			  
			  <th>Editar</th>
<?php endif; ?>			  
            </tr>
          </thead>
          <tbody>
          <?php $__currentLoopData = $usuarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php if($usuario->user_estado == "Activo" && $usuario->nombre != "admin" && $usuario->rol != "Cliente"): ?>

            <tr>
              <td>
			  <a href="mailto:
		<?php echo e(($usuario->email)); ?>

		?subject=
		Tirilla%20Nómina
		&body=
		<?php echo e(ucwords($usuario->nombre)); ?>, la siguiente es la tirrilla de nómina
		">
			  <?php echo e(ucwords($usuario->nombre)); ?>

				</a> <?php $total_1= 0; $total_2=0; $total_3= 0;?>
			  </td>
			  <td><?php echo e(ucwords($usuario->rol)); ?></td>
              <td align="right"><span class="label label-primary">$ <?php echo e(number_format($usuario->valor_a_pagar,0)); ?></span> <?php $total_3+= $usuario->valor_a_pagar;$total_3N+=$total_3;?> </td>
			  <td>
			  <?php if($usuario->contrato == 0): ?>
			  <span class="label label-success">Servicios</span>
			  <?php else: ?>
			  <span class="label label-primary">Laboral</span>
			  <?php endif; ?>			  
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*$info[0]->pensioon/100,0)); ?> <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*$info[0]->pensioon/100;?> 
			  <?php else: ?>
			  0 <?php $total_1+= 0;?>
			  <?php endif; ?>
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*$info[0]->salud/100,0)); ?> <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*$info[0]->salud/100;?> 
			  <?php else: ?>
			  0 <?php $total_1+= 0;?>
			  <?php endif; ?>
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.04,0)); ?> <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.04;?> 
			  <?php else: ?>
			  0 <?php $total_1+= 0;?>
			  <?php endif; ?>
			  </td>			  
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
				  <?php if($usuario->arl_categoria == 1): ?>
				  <?php echo e(number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.00522,0)); ?> <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.00522;?>
				  <?php elseif($usuario->arl_categoria == 2): ?>
				  <?php echo e(number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0104,0)); ?> <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0104;?>
				  <?php elseif($usuario->arl_categoria == 3): ?>
				  <?php echo e(number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0244,0)); ?> <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0244;?>
				  <?php elseif($usuario->arl_categoria == 4): ?>
				  <?php echo e(number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0435,0)); ?> <?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0435;?>
				  <?php elseif($usuario->arl_categoria == 5): ?>
				  <?php echo e(number_format(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0696,0)); ?>	<?php $total_1+= ($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*0.0696;?>
				  <?php endif; ?>
				  <span class="label label-info">Categoría <?php echo e($usuario->arl_categoria); ?></span>
			  <?php else: ?>
			  0 <?php $total_1+= 0;?>
			  <?php endif; ?>				  
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format(round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros+$info[0]->auxilio)*$info[0]->cesantiia/100,0),0)); ?> <?php $total_2+= round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros+$info[0]->auxilio)*$info[0]->cesantiia/100,0);?> 
			  <?php else: ?>
			  0 <?php $total_2+= 0;?>
			  <?php endif; ?>			  
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format(round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros+$info[0]->auxilio)*$info[0]->intereses/100,0),0)); ?> <?php $total_2+= round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros+$info[0]->auxilio)*$info[0]->intereses/100,0);?> 
			  <?php else: ?>
			  0 <?php $total_2+= 0;?>
			  <?php endif; ?>
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format(round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros+$info[0]->auxilio)*$info[0]->prima/100,0),0)); ?> <?php $total_2+= round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros+$info[0]->auxilio)*$info[0]->prima/100,0);?> 
			  <?php else: ?>
			  0 <?php $total_2+= 0;?>
			  <?php endif; ?>
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format($usuario->valor_a_pagar*$info[0]->vacaciones/100,0)); ?> <?php $total_2+= $usuario->valor_a_pagar*$info[0]->vacaciones/100;?> 
			  <?php else: ?>
			  0 <?php $total_2+= 0;?>
			  <?php endif; ?>
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
				  <?php if($usuario->valor_a_pagar <= $usuario->valor_a_pagar*2): ?>
				  <?php echo e(number_format($info[0]->auxilio,0)); ?> <?php $total_3+= $info[0]->auxilio;?>
				  <?php else: ?>
				  0
				  <?php endif; ?>
			  <?php else: ?>
			  0 <?php $total_2+= 0;?>
			  <?php endif; ?>				  
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format($usuario->comisiones,0)); ?> <?php $total_3+= $usuario->comisiones?> 
			  <?php else: ?>
			  0 <?php $total_3+= 0;?>
			  <?php endif; ?>			  
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format($usuario->otros,0)); ?> <?php $total_3+= $usuario->otros?> 
			  <?php else: ?>
			  0 <?php $total_3+= 0;?>
			  <?php endif; ?>
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <span class="label label-primary"><?php echo "$ ".number_format($total_1,0);$total_1N+=$total_1;?></span><?php $total_3+= $total_1;?> 
			  <?php else: ?>
			  0 <?php $total_3+= 0;$total_1N+= 0;?>
			  <?php endif; ?>			  
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <span class="label label-warning"><?php echo "$ ".number_format($total_2,0);$total_2N+=$total_2;?></span>
			  <?php else: ?>
			  0 <?php $total_2N+= 0;?>
			  <?php endif; ?>			  
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <span class="label label-info"><?php echo "$ ".number_format($total_3,0);?></span>
			  <?php else: ?>
			  0 
			  <?php endif; ?>			  
			  </td>
<?php if($permisos[17] == '1'): ?> 			  
			  <td align="center" valign="middle">
				<a href="#" data-toggle="modal" data-target="#editar<?php echo e($usuario->id_plantilla); ?>" class="btn-sm btn-success" title="Editar el registro de los otros gastos <?php echo e(ucwords($usuario->nombre)); ?>"> Editar</a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="editar<?php echo e($usuario->id_plantilla); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						  <form action="<?php echo e(url('Finanza/updateCuota')); ?>" method="POST">
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">     	 
							  <div class="col-md-6">
						  <div class="card">
							<div class="card-header">
							  <h2>Detalles de Otros Pagos para <?php echo e(ucwords($usuario->nombre)); ?></h2>
							</div>
							<div class="card-body">
							  <div class="row">
								<div class="col-md-6">
								<label>Contrato*</label>
								  <select required="" class="select" name="contrato">
									<option value="1"> Laboral</option>
									<option value="0"> Servicios</option>
								  </select>	
								  <br>
								<label>Comisiones $</label>
								<input type="number" id="venci" class="form-control date" name="Comisioon" value="<?php echo e($usuario->comisiones); ?>">
								<input type="hidden" id="venci" class="form-control date" name="Id" value="<?php echo e($usuario->id_plantilla); ?>">								
								<br>
								<label>Otros pagos, extras, compensatorios $</label>
								<input type="number"  id="venci" class="form-control date" name="Otros" value="<?php echo e($usuario->otros); ?>">
								<label>Categoría ARL *</label>
								  <select required="" class="select" name="ARL">
									<option value="1"> Categoría 1 </option>
									<option value="2"> Categoría 2 </option>
									<option value="3"> Categoría 3 </option>
									<option value="4"> Categoría 4 </option>
									<option value="5"> Categoría 5 </option>
								  </select>
								
								</div>
							  <center><input type="submit" class="btn btn-success" value="Actualizar Pagos"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
								
							  </div>
							</div>
						  </div>
							  </div>


						  </form>
							</div>
						<!-- Modal -->				  
			  </td>	
<?php endif; ?>			  
            </tr>		
		
		<?php endif; ?>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
		</div> 
		
			
		
      </div>
    </div>
	
    <div class="col-md-12">
      <div class="card">
		
       <div class="card-header" style="overflow-x:auto;">
		<table class="table" width="100%" cellspacing="0" >
		<thead>
			<tr>
            <th ><p style="font-size:50px"> Reporte de Nómina <?php echo $meses[date('n')-1] ?>:  </th>
				</tr>
			</thead>	
			</table>	
				</div>		
		
        <div class="card-body">
          <div class="row">
            <div class="col-md-6" align="right">
              <label>Total Salarios Mensuales </label>
              <input type="text" style="text-align:right;" readonly class="form-control" name="SalarioMensual" value="$ <?php echo e(number_format($total_3N,0)); ?>" maxlength="250">
              <label>Total Seguridad Social Mensual</label>
              <input type="text" style="text-align:right;" name="Seguridad" readonly class="form-control" value="$ <?php echo e(number_format($total_1N,0)); ?>" maxlength="250">
            </div>
            <div class="col-md-6" align="right">
              <label>Total reserva de Provisiones Sociales Mensuales</label>
              <input type="text" style="text-align:right;" name="Salario" readonly class="form-control" value="$ <?php echo e(number_format($total_2N,0)); ?>" >
              <label>Total Costo de Nómina</label>
              <input type="text" style="text-align:right;" name="GNómina" readonly class="form-control" value="$ <?php echo e(number_format($total_3N + $total_1N,0)); ?>" >			  
            </div>
          </div>
        </div>
      </div>
    </div>			
	
  </div>
<?php endif; ?>  
<?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
