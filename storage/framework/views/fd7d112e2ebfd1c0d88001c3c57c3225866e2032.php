<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[6] == '1'): ?>
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
		<table class="table" width="100%" cellspacing="0" >
		<thead>
			<tr>
				<th ><p style="font-size:50px">Inventario de Insumos</th>
			</tr>
		</thead>	
		</table>	
			</div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>NOMBRE</th>
              <th>DETALLE</th>		  
              <th>DISPONIBLE</th>
<?php if($permisos[7] == '1'): ?>			  
			  <th>Registrar Bajada</th>
			<th>Agregar Insumos por proveedor</th>			  			  
<?php endif; ?>			
			  <th>Ver Movimientos</th>
            </tr>
          </thead>
          <tbody>
		  <?php for($i = 0; $i < count($Lista); $i++): ?>
		  <?php if($Lista[$i]->estado == "Activo"): ?>
            <tr>
              <td><?php echo e(ucwords($Lista[$i]->cod)); ?></td>
              <td><?php echo e(ucwords($Lista[$i]->detalle)); ?></td>
              <td><?php echo e($Inventario[$i]->disponible); ?></td>
<?php if($permisos[7] == '1'): ?>				  
			<td align="center" valign="middle">	
			<!-- Modal -->			
			<a type="button" class="btn-sm btn-danger btn-sm" data-toggle="modal" data-target="#UsarMP<?php echo e($Lista[$i]->id); ?>" title="Registrar consumo externo de materia prima <?php echo e(ucwords($Lista[$i]->cod)); ?>">Usar</a>
			<!-- ModalBODY -->
				<div class="modal fade" id="UsarMP<?php echo e($Lista[$i]->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Usar materia prima de <?php echo e($Lista[$i]->cod); ?></h4>
					  </div>
				  <form action="<?php echo e(url('MateriaPrima/removemp')); ?>" method="POST">		  
				  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"> 
					<div class="card-body">
					  <div class="row">
						<div class="col-md-6">
						<label>Disponible </label>
						<input readonly type="text" id="venci" class="form-control date" name="Disponible" value="<?php echo e($Inventario[$i]->disponible); ?>" >
						<input type="hidden" class="form-control date"  value="<?php echo e($Lista[$i]->id); ?>" name="Id">           
						<label>Anotación </label>
						<input type="text" id="venci" class="form-control date" name="Anotación" value="Desperdicio" >						
						</div>
						<input type="hidden" readonly id="venci" class="form-control date" name="Detalle" value="<?php echo e($Lista[$i]->detalle); ?>">			
						<div class="col-md-6">
						<label>Cantidad a usar</label>	
						<input type="number" step="any" placeholder="0.00" required="" id="Cantidad" class="form-control date" name="Cantidad" onChange="compararDisponible(<?php echo e($Inventario[$i]->disponible); ?>, this.form.Cantidad);">
						</div>
					  </div>
					</div>			
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary">Usar</button> 
					  </div>
			  </form>		  
					</div>
				  </div>
				</div>			
			<!-- Modal -->			
			</td>	
			            <td align="center" valign="middle">		  
            <!-- <a href="<?php echo e(url('MateriaPrima/addproviders?id_materiaprima='.$Lista[$i]->id)); ?>" class="btn btn-primary">Agregar</a> -->
			<!-- Modal -->			
			<a type="button" class="btn-sm btn-primary btn-sm" data-toggle="modal" data-target="#addProvider<?php echo e($Lista[$i]->id); ?>" title="Agregar cantidad que ingresa de materia prima <?php echo e(ucwords($Lista[$i]->cod)); ?>">Agregar</a>
			<!-- ModalBODY -->
				<div class="modal fade" id="addProvider<?php echo e($Lista[$i]->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Añadir ingreso para <?php echo e($Lista[$i]->cod); ?></h4>
					  </div>
				  <form action="<?php echo e(url('MateriaPrima/addprovider')); ?>" method="POST">		  
				  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"> 
					<div class="card-body">
					  <div class="row">
						<div class="col-md-6">
						<label>Anotación </label>
						<input type="text" id="venci" class="form-control date" name="Anotación" value="Ingreso por " >
						
							<label>Proveedor *</label>
						  <select required="" class="form-control" name="Proveedor">
							<?php $__currentLoopData = $Proveedores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($usuario->estado == "Activo"): ?>
							<option value="<?php echo e($usuario->empresa); ?>"><?php echo e($usuario->empresa); ?></option>
							<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>				
						  </select>	
						  <input type="hidden" class="form-control date"  value="<?php echo e($Lista[$i]->id); ?>" name="Id">           
						 
						</div>
						<input type="hidden" readonly id="venci" class="form-control date" name="Detalle" value="<?php echo e($Lista[$i]->detalle); ?>">			
						<div class="col-md-6">
						<label>Cantidad a Agregar</label>	
						<input required="" type="number"  step="any" placeholder="0.00" id="venci" class="form-control date" name="Cantidad">
						<label>El Costo total $</label>	
						<input required="" type="number"  placeholder="0" id="venci" class="form-control date" name="Costo">
						
						</div>
					  </div>
					</div>			
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary">Agregar</button>
					  </div>
			  </form>		  
					</div>
				  </div>
				</div>			
			<!-- Modal -->			
			</td>
<?php endif; ?>			
            <td align="center" valign="middle">		  
            <a href="<?php echo e(url('MateriaPrima/ver?id_materiaprima='.$Lista[$i]->id)); ?>" class="btn-sm btn-info" title="Ver el registro de las entradas y bajadas de las materias primas <?php echo e(ucwords($Lista[$i]->cod)); ?>">Ver</a>						
			</td>
			
            </tr>
		<?php endif; ?>
          <?php endfor; ?>
          </tbody>
        </table>      
      </div>
    </div>
  </div>
  
<!-- Modal -->

<!-- Modal -->
<?php endif; ?>
<?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script>
function compararDisponible(disponible,cantidad) 
{
	if (cantidad.value > disponible)
	{
	alert("La cantidad es mayor a lo disponible");
	cantidad.value = "0";
	}
}


</script>