<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[24] == '1'): ?>
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Pedidos</th>
            <th align="center" valign="middle">
			</th>
            <th align="center" valign="middle">
			</th>			
        </tr>
    </thead>	
	</table>	
        </div>
		<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
			  <th># Número</th>			
              <th>Fecha</th>
              <th>Cliente</th>
              <th>Tipo de Pago</th>
              <th>Total Efectuado</th>
			  <th>Estado</th>		
<?php if($permisos[25] == '1'): ?>				  
              <th>Cambiar Estado</th>
<?php endif; ?>			   
            </tr>
          </thead>
          <tbody>
          <?php $__currentLoopData = $clientes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		  <?php if($usuario->id_cliente == Auth::user()->cuota ||  Auth::user()->rol == "Administrador"): ?>
            <tr>
			  <td class='text-center'>
			  <a href="pdfpedido?num_pedido=<?php echo e($usuario->num_pedido); ?>&ID=1" target="_blank" class="btn-sm btn-info">Ver#<?php echo e(str_pad( $usuario->num_pedido, 4, "0", STR_PAD_LEFT )); ?></a> 
			  </td>			
              <td><?php echo e($usuario->fecha); ?></td>
              <td>
			  <a title="Correo: <?php echo e(($usuario->correo_cliente)); ?>" data-toggle="tooltip" data-placement="left" href="mailto:
		<?php echo e(($usuario->correo_cliente)); ?>

		?subject=
		Factura%20de%20Compra
		&body=
		<?php echo e(ucwords($usuario->nombre_clientes)); ?>, la siguiente es la factura de su compra
		">			  
			  <?php echo e(ucwords($usuario->nombre_clientes)); ?>

			  </a>
			  </td>
              <td>
			  <?php if( $usuario->tipo_pago === "Efectivo"): ?>
				  <span class="label label-success">
				  <?php echo e($usuario->tipo_pago); ?>

				  </span>
			  <?php elseif( $usuario->tipo_pago != "Efectivo"): ?>
				  <span class="label label-warning">
				  <?php echo e($usuario->tipo_pago); ?>

				  </span>			  
			  <?php endif; ?>
			  </td>
			  <td align= "right">
			  <?php if( $usuario->tipo_pago === "Efectivo"): ?>
				  <span class="label label-success">
				  $ <?php echo e(number_format($usuario->total_venta,0)); ?>

				  </span>
			  <?php elseif( $usuario->tipo_pago != "Efectivo"): ?>
					<?php if( $usuario->total_venta == 0): ?>
					  <span class="label label-success">
					  $ <?php echo e(number_format($usuario->total_venta,0)); ?>

					  </span>
					<?php else: ?>
					  <span class="label label-warning">
					  $ <?php echo e(number_format($usuario->total_venta,0)); ?>

					  </span>					  
					<?php endif; ?>
			  <?php endif; ?>			  
			  
			  </td>
              <td align="center" valign="middle">
			  <?php if( $usuario->estado_factura === "Pendiente"): ?>
				  <!--<span class="label label-danger"><?php echo e($usuario->estado_factura); ?></span>-->
				<span class="badge badge-warning badge-icon"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Pendiente</span></span>				  
			  <?php elseif( $usuario->estado_factura === "Revisado"): ?>
				  <!--<span class="label label-success"><?php echo e($usuario->estado_factura); ?></span>-->
				  <span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span>Revisado</span></span>
			  <?php elseif( $usuario->estado_factura === "Aprobado"): ?>
				  <!--<span class="label label-info"><?php echo e($usuario->estado_factura); ?></span>-->
				  <span class="badge badge-primary badge-icon"><i class="fa fa-truck" aria-hidden="true"></i><span>Aprobado</span></span>
			  <?php endif; ?>			  
			  </td>			
<?php if($permisos[25] == '1'): ?>				  
              <td align="center" valign="middle">
					<?php if($usuario->estado_factura === "Pendiente"): ?>
						<a href="<?php echo e(url('Factura/cambiarestadopedido?id='.$usuario->id_pedidos)); ?>" class="btn-sm btn-success" title="El pedido #<?php echo e($usuario->num_factura); ?> ha sido revisada" data-toggle="tooltip" data-placement="left">Revisado</a>
					<?php elseif($usuario->estado_factura === "Revisado"): ?>
						<a href="<?php echo e(url('Factura/cambiarestadopedido?id='.$usuario->id_pedidos)); ?>" class="btn-sm btn-info" title="El pedido #<?php echo e($usuario->num_factura); ?> fue aprobada" data-toggle="tooltip" data-placement="left">Aprobado</a>			  
					<?php endif; ?>
			  </td>
<?php endif; ?>			    			  
            </tr>
			<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
		</div> 
      </div>
    </div>
	
  </div>
<?php endif; ?>  
<?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
