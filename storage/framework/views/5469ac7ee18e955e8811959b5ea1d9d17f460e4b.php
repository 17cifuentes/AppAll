<?php if(count($errors) > 0): ?>
	<div class="alert alert-danger alert-dismissable">
  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  		<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<strong>¡Atención!</strong>
		<li><?php echo e($error); ?></li>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</div>
<?php endif; ?>
<?php if(session('correcto')): ?>
	<div class="alert alert-success alert-dismissable">
  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  		<strong>¡Éxito!</strong> <?php echo e(session('correcto')); ?>

	</div>
<?php endif; ?>
<?php if(session('incorrecto')): ?>
	<div class="alert alert-danger alert-dismissable">
  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  		<strong>¡Falló!</strong> <?php echo e(session('incorrecto')); ?>

	</div>
<?php endif; ?>