<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[0] == '1'): ?>
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Gestión de Insumos</th>
            <th align="center" valign="middle">
			<a href="#" data-toggle="modal" data-target="#ingresar" class="btn btn-info" title="Agregar Insumo" data-toggle="tooltip" data-placement="left"><span class="fa fa-plus"></span> <span class="fa fa-exchange"></span> Registrar Insumo</a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="ingresar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						  <form action="<?php echo e(url('MateriaPrima/postregistro')); ?>" method="POST">
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">     	 
							  <div class="col-md-12">
							  <div class="card">
								<div class="card-header">
								  <h2>REGISTRAR INSUMO</h2>
								</div>
								<div class="card-header">
								  <h4>Datos del Insumo: </h4>
								</div>
								<div class="card-body">
								  <div class="row">
									<div class="col-md-12">
									<label>Nombre </label>
									<input type="text" required="" id="venci" class="form-control date" name="Código" maxlength="250">
									<label>Detalle </label>
									<input type="text"  id="venci" class="form-control date" name="Detalle" maxlength="250">
									<label>Unidad de Medida </label>
									<input type="text"  id="venci" class="form-control date" name="Medida" maxlength="250">					
									</div>
								  </div>
								</div>
							  </div>
							  </div>
							<div class="col-md-12">
							  <center><input type="submit" class="btn btn-success" value="Registrar Insumo"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  

						<!-- Modal -->			
			</th>
            <th align="center" valign="middle">
			<?php if($ver == "Inactivo"): ?>
			<a href="<?php echo e(url('MateriaPrima/index?ver=Activo')); ?>" class="btn btn-success" title="Ver insumos activos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Activos</a>
			<?php else: ?>
			<a href="<?php echo e(url('MateriaPrima/index?ver=Inactivo')); ?>" class="btn btn-danger" title="Ver insumos inactivos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Inactivos</a>			
			<?php endif; ?>
			</th>
        </tr>
    </thead>	
	</table>	
        </div>
		<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>		
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>NOMBRE</th>
              <th>DETALLE</th>
              <th>UNIDAD DE MEDIDA</th>			  
              <th>ESTADO</th>			  
              <th>Activar / Desactivar</th>
              <th>Editar</th>
            </tr>
          </thead>
          <tbody>
		  <?php for($i = 0; $i < count($Lista); $i++): ?>
		  <?php if($Lista[$i]->estado === $ver): ?>
            <tr>
              <td><?php echo e(ucwords($Lista[$i]->cod)); ?></td>
              <td><?php echo e(ucwords($Lista[$i]->detalle)); ?></td>
			  <td><?php echo e(ucwords($Lista[$i]->medida)); ?></td>
              <td align="center" valign="middle">
			  <?php if( $Lista[$i]->estado === "Activo"): ?>
			  <span class="label label-success">
			  <?php echo e($Lista[$i]->estado); ?>

			  </span>
			  <?php elseif( $Lista[$i]->estado === "Inactivo"): ?>
			  <span class="label label-danger">
			  <?php echo e($Lista[$i]->estado); ?>

			  </span>
			  <?php endif; ?>
			  </td>              
            <td align="center" valign="middle">
			  <?php if($Lista[$i]->estado === "Activo"): ?>			  
            <a href="<?php echo e(url('MateriaPrima/cambiarestado?id_materiaprima='.$Lista[$i]->id)); ?>" class="btn-sm btn-danger" title="Desactivar la materia prima <?php echo e(ucwords($Lista[$i]->cod)); ?>" data-toggle="tooltip" data-placement="left">Desactivar</a>			
				<?php else: ?>	
            <a href="<?php echo e(url('MateriaPrima/cambiarestado?id_materiaprima='.$Lista[$i]->id)); ?>" class="btn-sm btn-info" title="Activar la materia prima <?php echo e(ucwords($Lista[$i]->cod)); ?>" data-toggle="tooltip" data-placement="left">Activar</a>							
			  <?php endif; ?>				
			</td>
			  <td>
			  <a href="<?php echo e(url('MateriaPrima/editar?id='.$Lista[$i]->id)); ?>" class="btn-sm btn-warning" title="Editar la materia prima <?php echo e(ucwords($Lista[$i]->cod)); ?>" data-toggle="tooltip" data-placement="left">Editar</a>
			  </td>			
            </tr>
			<?php endif; ?>
          <?php endfor; ?>
          </tbody>
        </table>     
      </div>
    </div>
  </div>
<?php endif; ?>  
<?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
