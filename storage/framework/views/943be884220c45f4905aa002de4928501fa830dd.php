<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Movimientos de producción</th>
        </tr>
    </thead>	
	</table>	
        </div>
		<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>			
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>Responsable</th>
              <th>Artículo producido</th>
			  <th>Cantidad</th>
			  <th>Detalle</th>
            </tr>
          </thead>
          <tbody>
		  <?php $__currentLoopData = $Lista; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $elemento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td><?php echo e(ucwords($elemento->nombre)); ?></td>
              <td><?php echo e($elemento->art_nombre); ?></td>
			  <td align="right"><?php echo e($elemento->cantidad); ?></td>
              <td align="right"><?php echo e($elemento->detalle); ?></td>
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>      
      </div>
		<center><a href="crear" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>	  
	  
    </div>
  </div>
<?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
