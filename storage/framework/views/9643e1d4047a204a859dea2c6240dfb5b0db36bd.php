<?php 
	if(Auth::user()->rol == "Administrador"){
	session(["permisos" => "11111111111111111111111111"]);
	}elseif(Auth::user()->rol == "Operario"){
	session(["permisos" => "00001010101100110000000000"]);	
	}elseif(Auth::user()->rol == "Almacenista"){
	session(["permisos" => "00001111110000110000000000"]);		
	}elseif(Auth::user()->rol == "Vendedor"){
	session(["permisos" => "00000000000010110011110000"]);			
	}elseif(Auth::user()->rol == "R.Humanos"){
	session(["permisos" => "11110000000011111100000000"]);				
	}elseif(Auth::user()->rol == "Cliente"){
	session(["permisos" => "00000000000000000000000010"]);				
	}
	
?>

<aside class="app-sidebar" id="sidebar">
  <div class="sidebar-menu">
    <ul class="sidebar-nav">
      <li class="active">
        <a href="<?php echo e(url('Inicio')); ?>">
          <div class="icon">
            <i class="fa fa-home" aria-hidden="true"></i>
          </div>
          <div class="title">Pantalla de Inicio <?php $permisos = Session::get('permisos') ?> </div>
        </a>
      </li>
<?php if($permisos[24] == '1'): ?> 	  
      <li>
        <a href="<?php echo e(url('Factura/pedidos')); ?>">
          <div class="icon">
            <i class="fa fa-shopping-basket"></i>
          </div>
          <div class="title">Pedidos</div>
        </a>
      </li>
<?php endif; ?>	  
<?php if($permisos[0] == '1'): ?>
      <li>
        <a href="<?php echo e(url('usuario/listar')); ?>" >
          <div class="icon">
            <i class="fa fa-users"></i>
          </div>
          <div class="title" >Gestión de Usuarios</div>
        </a>
      </li>
<?php endif; ?>
<?php if($permisos[2] == '1'): ?>
      <li>
        <a href="<?php echo e(url('Providers/index')); ?>">
          <div class="icon">
            <i class="fa fa-cart-arrow-down"></i>
          </div>
          <div class="title">Gestión de Proveedores</div>
        </a>
      </li>
<?php endif; ?>
<?php if($permisos[4] == '1'): ?>
      <li>
        <a href="<?php echo e(url('MateriaPrima/index')); ?>">
          <div class="icon">
            <i class="fa fa-stack-overflow"></i>
          </div>
          <div class="title">Gestión de Insumos</div>
        </a>
      </li>
<?php endif; ?>
<?php if($permisos[6] == '1'): ?>
      <li>
        <a href="<?php echo e(url('MateriaPrima/listar')); ?>">
          <div class="icon">
            <i class="fa fa-stack-overflow"></i>
          </div>
          <div class="title">Inventariado de Insumos</div>
        </a>
      </li>	
<?php endif; ?>	
<?php if($permisos[8] == '1'): ?>  
      <li>
        <a href="<?php echo e(url('ProductoTerminado/index')); ?>">
          <div class="icon">
            <i class="fa fa-tasks"></i>
          </div>
          <div class="title">Gestión de Productos</div>
        </a>
      </li>
<?php endif; ?>	 
<?php if($permisos[10] == '1'): ?>   
      <li>
        <a href="<?php echo e(url('ProductoTerminado/crear')); ?>">
          <div class="icon">
            <i class="fa fa-tasks"></i>
          </div>
          <div class="title">Reportes de Productos</div>
        </a> 
      </li>	  
<?php endif; ?>	
<?php if($permisos[12] == '1'): ?>  
	<li>
        <a href="<?php echo e(url('Cliente/index')); ?>">
          <div class="icon">
            <i class="fa fa-child"></i>
          </div>
          <div class="title">Gestión de Clientes</div>
        </a>
      </li>
<?php endif; ?>	
<?php if($permisos[14] == '1'): ?>    
      <li>
        <a href="<?php echo e(url('Finanza/index')); ?>">
          <div class="icon">
            <i class="fa fa-balance-scale"></i>
          </div>
          <div class="title">Gestión de Gastos</div>
        </a>
		</li>
<?php endif; ?>		
<?php if($permisos[16] == '1'): ?>  
      <li>
        <a href="<?php echo e(url('Finanza/listar')); ?>">
          <div class="icon">
            <i class="fa fa-balance-scale"></i>
          </div>
          <div class="title">Gestión de Nómina</div>
        </a>
		</li>
<?php endif; ?>	
<?php if($permisos[18] == '1'): ?> 	
      <li>
        <a href="<?php echo e(url('Factura/index')); ?>">
          <div class="icon">
            <i class="fa fa-check-square-o"></i>
          </div>
          <div class="title">Facturación</div>
        </a>	  
      </li>
<?php endif; ?>	
<?php if($permisos[23] == '1'): ?> 	  
      <li>
        <a href="<?php echo e(url('Empresa/editar')); ?>">
          <div class="icon">
            <i class="fa fa-cogs"></i>
          </div>
          <div class="title">Datos mi Empresa</div>
        </a>
      </li>
<?php endif; ?>
	  
	  
    </ul>
  </div>

  

</aside>

<div class="app-container">

  <nav class="navbar navbar-default" id="navbar">
  <div class="container-fluid">
  
    <div class="navbar-collapse collapse in">

      <ul class="nav navbar-nav navbar-mobile">
        <li>
          <button type="button" class="sidebar-toggle">
            <i class="fa fa-bars"></i>
          </button>
        </li>
        <li class="logo">
          <a class="navbar-brand" href="#"><span class="highlight">PEGA</span>Valle</a>
        </li>
        <li>
          <button type="button" class="navbar-toggle">
            <img class="profile-img" src="<?php echo e(asset('dash/assets/images/profile.png')); ?>">
          </button>
        </li>
      </ul>	

	  

      <ul class="nav navbar-nav navbar-right">
<?php if($permisos[24] == '1'): ?> 		  
        <li class="dropdown notification">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <div class="icon"><i class="fa fa-shopping-basket" aria-hidden="true"></i></div>
            <div class="title">Pedidos</div>
            <div class="count">0</div>
          </a>
          <div class="dropdown-menu">
            <ul>
              <li class="dropdown-header">Pedidos</li>
              <li class="dropdown-empty">No Hay Pedidos en el momento</li>
              <li class="dropdown-footer">
                <a href="<?php echo e(url('Factura/pedidosLista')); ?>">Ver Todo <i class="fa fa-angle-right" aria-hidden="true"></i></a>
              </li>
            </ul>
          </div>
        </li>
<?php endif; ?>		
        <li class="dropdown profile">
          <a href="/html/pages/profile.html" class="dropdown-toggle"  data-toggle="dropdown">
            <img class="profile-img" src="<?php echo e(asset( Auth::user()->pic)); ?>">
            <div class="title">Perfil</div>
          </a>
          <div class="dropdown-menu">
            <div class="profile-info">
              <h4 class="username"><?php echo e(Auth::user()->name); ?></h4>
            </div>
            <ul class="action">
              <li>
                <a data-toggle="modal" data-target="#cpassword">
                  Cambiar Contraseña
                </a>
              </li>
              <li>
                <a data-toggle="modal" data-target="#cfoto">
                  Cambiar Fotografía
                </a>
              </li>			  
              <li>
                <a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Finalizar Sesión</a>
                  <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                    <?php echo e(csrf_field()); ?>

                  </form>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>
				

