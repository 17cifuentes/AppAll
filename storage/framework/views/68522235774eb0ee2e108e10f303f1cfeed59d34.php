<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Acceso</title>
  	<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'>
	<link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
  <div class="login-wrap">
  	
	<div class="login-html">

		<div class="title" style="margin-top: -4em;margin-bottom: 6em"><center><h1 style="color:white">Ingresar a PEGA VALLE</h1></center></div>

		<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Ingresar</label>
		<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Registrarse</label>

		<div class="login-form">
			<form role="form" method="POST" action="<?php echo e(url('login')); ?>">
			<?php echo e(csrf_field()); ?>

			<div class="sign-in-htm">
				<div class="group">
					<label for="user" class="label">Correo</label>
					<input id="email" name="email" type="text" class="input">
				</div>
				<div class="group">
					<label for="pass" class="label">Contraseña</label>
					<input id="pass" type="password" name="password" class="input" data-type="password">
				</div>
				
				<div class="group">
					<input type="submit" class="button" value="Ingresar">
				</div>
				<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>			
				<div class="hr"></div>
				<div class="foot-lnk">
					<a href="#" data-toggle="modal" data-target="#desprendibles"  class="btn btn-danger" title="Gestionar Recuperación" data-backdrop="false">¿Olvidó contraseña?</a>
						<!-- ModalBODY 
							<div class="modal fade" id="desprendibles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: initial;">
							  <div class="modal-dialog modal-sm" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<h4 class="modal-title" id="myModalLabel">Recuperar Contraseña</h4>
								  </div>
							  <form target="_blank" action="<?php echo e(url('Cliente/recuperar')); ?>" method="POST">		  
								<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"> 
								<div class="card-body">
								  <div class="row">
									<div class="col-md-12">
									<label>Ingresar Correo </label>
									<input type="email" required id="Correo1" class="form-control" name="Correo1" value="">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
									<label>Re-escribir Correo</label>	
									<input type="email" required id="Correo2" class="form-control" name="Correo2" value="">
									</div>
								  </div>

								</div>								  	
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal" value="Recuperar">Cerrar</button>
									<button type="submit" class="btn btn-primary">Enviar Solicitud</button> 
								  </div>
							</form>		  
								</div>
							  </div>
							</div>			
						<!-- Modal -->						
				</div>
			</div>
			</form>
            <form class="form-horizontal" method="POST" action="<?php echo e(url('Cliente/postLogin')); ?>">
            <?php echo e(csrf_field()); ?>			
			<div class="sign-up-htm">
				<div class="group">
					<label for="user" class="label">Nombre</label>
					<input id="user" name="Nombre" type="text" class="input" required="">
				</div>
				<div class="group">
					<label for="pass" class="label">Correo</label>
					<input id="pass" name="Correo" type="email" class="input" data-type="password" required="">
				</div>
				<div class="group">
					<label for="user" class="label">Teléfono</label>
					<input id="user" name="Teléfono" type="text" class="input" required="">
				</div>	
				<div class="group">
					<label for="user" class="label">Dirección</label>
					<input id="user" name="Address" type="text" class="input" required="">
				</div>				
				<div class="group">
					<input type="submit" class="button" value="Registrar">
				</div>
				<div class="hr"></div>
			</div>
			</form>

		</div>
	</div>

</div>
  <script
			  src="https://code.jquery.com/jquery-3.2.1.js"
			  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
			  crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>			  
  
</body>
</html>
