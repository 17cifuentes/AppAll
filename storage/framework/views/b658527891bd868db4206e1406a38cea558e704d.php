 <footer class="app-footer"> 
  <div class="row">
    <div class="col-xs-12" align="right">
      <div class="footer-copyright">
		<hr>
		<span class="label label-success">
        Desarrollado por SAGAZ SAS © - AppALL v.1.0
		</span>
      </div>
    </div>
  </div>
</footer>
</div>

  </div>
  
  <script type="text/javascript" src="<?php echo e(asset('dash/assets/js/vendor.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo e(asset('dash/assets/js/app.js')); ?>"></script>  
  <script type="text/javascript" src="<?php echo e(asset('js/propios/scripts.js')); ?>"></script>

				<!-- ModalBODY -->
					<div class="modal fade" id="cpassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<h4 class="modal-title" id="myModalLabel">Cambiar Contraseña</h4>
						  </div>
					  <form action="<?php echo e(url('usuario/updatepassword')); ?>" method="POST">		  
					  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"> 
					  <div class="col-md-12">
					  <div class="card">
						<div class="card-body">
						  <div class="row">
						  <div class="col-md-12">
						  <label>Ingresar Nueva Contraseña</label>
						  <input type="password" required="" name="password" class="form-control" maxlength="250">  
							<input type="hidden" class="form-control date"  value="<?php echo e(Auth::user()->id); ?>" name="Id">           						  
							</div>
						  </div>
						</div>
						</div>
						</div>						
						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-primary">Actualizar</button>
						  </div>
					</form>		  
						</div>
					  </div>
					</div>			
				<!-- Modal -->	  
				<!-- ModalBODY -->
					<div class="modal fade" id="cfoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<h4 class="modal-title" id="myModalLabel">Cambiar Fotografía</h4>
						  </div>
					  <form action="<?php echo e(url('usuario/updatepic')); ?>" method="POST" enctype="multipart/form-data">		  
					  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"> 
					  <div class="col-md-12">
					  <div class="card">
						<div class="card-body">
						<div class="row">
						<div class="col-md-12">
						<center>
						<img src="<?php echo e(asset(Auth::user()->pic)); ?>" width="50%">
						<input type="file"  id="venci" class="form-control date" name="Pic">
						<input type="hidden" id="Pic_ruta" name="Pic_ruta" value="<?php echo e(Auth::user()->pic); ?>">				  				  				
						</center>						  
						<input type="hidden" class="form-control date"  value="<?php echo e(Auth::user()->id); ?>" name="Id">           						  
						</div>
						 </div>
						</div>
						</div>
						</div>						
						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-primary">Actualizar</button>
						  </div>
					</form>		  
						</div>
					  </div>
					</div>			
				<!-- Modal -->  
</body>
</html>

  <script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
