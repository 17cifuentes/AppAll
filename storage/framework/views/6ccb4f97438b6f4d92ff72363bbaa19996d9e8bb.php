<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[2] == '1'): ?>
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Gestión de Proveedores</th>
            <th align="center" valign="middle">
			<a href="#" data-toggle="modal" data-target="#ingresar" class="btn btn-info" title="Agregar un Proveedor" data-toggle="tooltip" data-placement="left"><span class="fa fa-plus"> </span><span class="fa fa-exchange"></span> Agregar Proveedor</a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="ingresar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<form action="<?php echo e(url('Providers/postregistro')); ?>" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">     	 
							  <div class="col-md-12">
							  <div class="card">
								<div class="card-header">
								  <h2>Registro De Proveedores</h2>
								</div>
								<div class="card-header">
								  <h4>Datos De Usuario: </h4>
								</div>
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									<label>Empresa </label>
									<input type="text" id="venci" class="form-control date" name="Empresa" maxlength="250">
									<label>Teléfono </label>
									<input type="text"  id="venci" class="form-control date" name="Teléfono" maxlength="250">
									</div>
									<div class="col-md-6">
									<label>Correo *</label>
									<input type="email" required="" class="form-control" name="correo" maxlength="250">
									<label>Dirección</label>	
									<input type="text"  id="venci" class="form-control date" name="Adress" maxlength="250">
									</div>
								  </div>
								</div>
							  </div>
							  </div>
							<div class="col-md-12">
							  <div class="card">
								<div class="card-header">
								  <h4>Datos Adicionales: </h4>
								</div>
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									  <label>Nit *</label>
										  <input type="text" required="" id="venci" class="form-control date" name="Nit" maxlength="250">
									  <label>Cámara de Comercio:</label>
										  <input type="file"  id="venci" class="form-control date" name="CámaraC">
									</div>
									<div class="col-md-6">
									  <label>Rut :</label>
										  <input type="file"  id="venci" class="form-control date" name="Rut">
									  <label>Certificado </label>
										  <input type="file"  id="venci" class="form-control date" name="Certificado">
									</div>
								  </div>
								</div>
							  </div>
							</div>
							<div class="col-md-12">
							  <center><input type="submit" class="btn btn-success" value="Registrar Proveedor"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  

						<!-- Modal -->			
			</th>
            <th align="center" valign="middle">
			<?php if($ver == "Inactivo"): ?>
			<a href="<?php echo e(url('Providers/index?ver=Activo')); ?>" class="btn btn-success" title="Ver proveedores activos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Activos</a>
			<?php else: ?>
			<a href="<?php echo e(url('Providers/index?ver=Inactivo')); ?>" class="btn btn-danger" title="Ver proveedores inactivos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Inactivos</a>			
			<?php endif; ?>
			</th>
        </tr>
    </thead>	
	</table>	
        </div>
		<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>EMPRESA</th>
              <th>TELÉFONO</th>
              <th>CORREO</th>
              <th>DIRECCIÓN</th>
              <th>ESTADO</th>
              <th>NIT</th>			  
              <th>CÁMARA DE COMERCIO</th>
              <th>RUT</th>
              <th>CERTIFICADO</th>		
<?php if($permisos[3] == '1'): ?>			  
              <th>Activar / Desactivar</th>		  
			  <th>Editar</th>
<?php endif; ?>			  
            </tr>
          </thead>
          <tbody>
          <?php $__currentLoopData = $usuarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		  <?php if($usuario->estado === $ver): ?>
            <tr>
              <td><?php echo e(ucwords($usuario->empresa)); ?></td>
              <td><?php echo e($usuario->tel); ?></td>
              <td><?php echo e($usuario->correo); ?></td>
              <td><?php echo e($usuario->address); ?></td>
              <td align="center" valign="middle">
			  <?php if( $usuario->estado === "Activo"): ?>
			  <span class="label label-success">
			  <?php echo e($usuario->estado); ?>

			  </span>
			  <?php elseif( $usuario->estado === "Inactivo"): ?>
			  <span class="label label-danger">
			  <?php echo e($usuario->estado); ?>

			  </span>
			  <?php endif; ?>
			  </td>
              <td><?php echo e($usuario->nit); ?></td>				  
              <td align="center" valign="middle">
			  <a data-toggle="modal" data-target="#verC<?php echo e($usuario->id); ?>" class="btn-sm btn-primary" title="Ver el registro de la cámara de comercio <?php echo e(ucwords($usuario->empresa)); ?>" data-toggle="tooltip" data-placement="left">Ver</a>
				<div id="verC<?php echo e($usuario->id); ?>" class="modal fade" role="dialog">  
				<div class="modal-dialog">
					<div class="modal-content">      
						<div class="modal-header">        
							<button type="button" class="close" data-dismiss="modal">×</button>        
							<h4 class="modal-title">Cámara de Comercio</h4>      </div>      
						<div class="modal-body"><img src="<?php echo e(asset( $usuario->camara)); ?>" class="img-rounded" alt="Modal Pic" width="304" height="236" />   </div>      
						<div class="modal-footer">        
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>     
						</div>  
					</div>  
				</div>
			  </td>
              <td align="center" valign="middle">
			  <a data-toggle="modal" data-target="#verR<?php echo e($usuario->id); ?>" class="btn-sm btn-primary" title="Ver el registro del rut <?php echo e(ucwords($usuario->empresa)); ?>" data-toggle="tooltip" data-placement="left">Ver</a>
				<div id="verR<?php echo e($usuario->id); ?>" class="modal fade" role="dialog">  
				<div class="modal-dialog">
					<div class="modal-content">      
						<div class="modal-header">        
							<button type="button" class="close" data-dismiss="modal">×</button>        
							<h4 class="modal-title">Rut</h4>      </div>      
						<div class="modal-body"><img src="<?php echo e(asset( $usuario->rut)); ?>" class="img-rounded" alt="Modal Pic" width="304" height="236" />   </div>      
						<div class="modal-footer">        
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>     
						</div>  
					</div>  
				</div>			  
			  </td>	  
              <td align="center" valign="middle">
			  <a data-toggle="modal" data-target="#vercer<?php echo e($usuario->id); ?>" class="btn-sm btn-primary" title="Ver el registro del certificado <?php echo e(ucwords($usuario->empresa)); ?>">Ver</a>
				<div id="vercer<?php echo e($usuario->id); ?>" class="modal fade" role="dialog">  
				<div class="modal-dialog">
					<div class="modal-content">      
						<div class="modal-header">        
							<button type="button" class="close" data-dismiss="modal">×</button>        
							<h4 class="modal-title">Certificado</h4>      </div>      
						<div class="modal-body"><img src="<?php echo e(asset( $usuario->certificado)); ?>" class="img-rounded" alt="Modal Pic" width="304" height="236" />   </div>      
						<div class="modal-footer">        
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>     
						</div>  
					</div>  
				</div>				  
			  </td>	
<?php if($permisos[3] == '1'): ?>
              <td align="center" valign="middle">
			  <?php if($usuario->estado === "Activo"): ?>
			  <a href="<?php echo e(url('Providers/cambiarestado?id='.$usuario->id)); ?>" class="btn-sm btn-danger" title="Desactivar al proveedor <?php echo e(ucwords($usuario->empresa)); ?>" data-toggle="tooltip" data-placement="left">Desactivar</a>
				<?php else: ?>
			  <a href="<?php echo e(url('Providers/cambiarestado?id='.$usuario->id)); ?>" class="btn-sm btn-info" title="Activar al proveedor <?php echo e(ucwords($usuario->empresa)); ?>" data-toggle="tooltip" data-placement="left">Activar</a>			  
			  <?php endif; ?>
			  </td>
			  <td align="center" valign="middle">
			  <a href="<?php echo e(url('Providers/editar?id='.$usuario->id)); ?>" class="btn-sm btn-warning" title="Editar datos al proveedor <?php echo e(ucwords($usuario->empresa)); ?>" data-toggle="tooltip" data-placement="left">Editar</a>
			  </td>
<?php endif; ?>			  
            </tr>
			<?php endif; ?>			
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
		</div> 
      </div>
    </div>
	
  </div>
<?php endif; ?>  
<?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
