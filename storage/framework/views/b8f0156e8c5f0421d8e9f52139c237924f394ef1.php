<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php $array = array(); //array_push($array, "item", "another item");?>
<?php if($permisos[8] == '1'): ?>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Gestión de Productos</th>
            <th align="center" valign="middle">
			<a href="#" data-toggle="modal" data-target="#ingresar" class="btn btn-info" title="Registrar Producto" data-toggle="tooltip" data-placement="left"><span class="fa fa-plus"></span> <span class="fa fa-exchange"> Registrar Producto</a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="ingresar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						  <form action="<?php echo e(url('ProductoTerminado/postregistro')); ?>" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">     	 
						<div class="col-md-12">
						  <form action="<?php echo e(url('ProductoTerminado/postregistro')); ?>" method="POST">
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">     	 
						<div class="col-md-12">
						<div class="card">
						<div class="card-header">
							  <h2>
							  REGISTRAR PRODUCTO 
							  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							  REGISTRO INSUMOS
							  </h2>
						</div>
							<div class="card-body">
							<div class="row">
							<div class="col-md-6">
								<label>Nombre </label>
								<input type="text" required="" id="venci" class="form-control date" name="Código" value="Artículo">
								<label>Detalle </label>
								<input type="text"  id="venci" class="form-control date" name="Detalle">
								<label>Unidad de Medida </label>
								<input type="text"  id="venci" class="form-control date" name="Medida">					
							</div>
							<div class="col-md-6"  id="contenidoinsumos">
								<a href="javascript::void()" onclick="registrar()" class="btn btn-primary"> Registrar Insumo</a>
								<a href="javascript::void()" onclick="limpiarinsumo()" class="btn btn-danger"> Limpiar</a>								
								<br>
							</div>
							</div>
							<div class="row">
						<div class="card-header">
							  <h2>
							  AÑADIR REQUISITOS
							  </h2>
						</div>							
							<div class="col-md-12"  id="contenido">
								<a href="javascript::void()" onclick="addinsumo()" class="btn btn-success">+ Agregar</a>
								<a href="javascript::void()" onclick="limpiar()" class="btn btn-danger"> Limpiar</a>
								<br>															
							</div>							
							</div>
							</div>
							</div>
							</div>	
							
						<div class="col-md-12">
							<center><input type="submit" class="btn btn-success" value="Registrar Datos"></center>
							<center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
						</div>
						</form>
						</div>						  

						<!-- Modal -->			
			</th>
            <th align="center" valign="middle">
			<?php if($ver == "Inactivo"): ?>
			<a href="<?php echo e(url('ProductoTerminado/index?ver=Activo')); ?>" class="btn btn-success" title="Ver usuarios activos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"> Ver Activos</a>
			<?php else: ?>
			<a href="<?php echo e(url('ProductoTerminado/index?ver=Inactivo')); ?>" class="btn btn-danger" title="Ver usuarios inactivos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"> Ver Inactivos</a>			
			<?php endif; ?>
			</th>
        </tr>
    </thead>	
	</table>	
        </div>
		<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>		
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>NOMBRE</th>
              <th>DETALLE</th>
              <th>ESTADO</th>			  
              <th>Precio $</th>
			  <th>Impuestos %</th>
<?php if($permisos[9] == '1'): ?>
              <th>Activar / Desactivar</th>
			  <th>Editar</th>
<?php endif; ?>			  
            </tr>
          </thead>
          <tbody>
		  <?php for($i = 0; $i < count($Lista); $i++): ?>
		  <?php if($Lista[$i]->estado === $ver): ?>
            <tr>
              <td><?php echo e(ucwords($Lista[$i]->art_nombre)); ?></td> 
              <td><?php echo e(ucwords($Lista[$i]->detalle)); ?></td>
              <td align="center" valign="middle">
				<?php if( $Lista[$i]->estado === "Activo"): ?>
				<span class="label label-success">			  
			  <?php echo e($Lista[$i]->estado); ?>

				</span>
			  <?php elseif( $Lista[$i]->estado === "Inactivo"): ?>
			  <span class="label label-danger">
			  <?php echo e($Lista[$i]->estado); ?>

			  </span>
			  <?php endif; ?>			  
			  </td>
              <td align= "right"><?php echo e(number_format($Lista[$i]->precio_unitario,0)); ?></td>
			  <td align= "right"><?php echo e($Lista[$i]->impuesto); ?></td>
<?php if($permisos[9] == '1'): ?>			  
            <td align="center" valign="middle">
			  <?php if($Lista[$i]->estado === "Activo"): ?>			  
            <a href="<?php echo e(url('ProductoTerminado/cambiarestado?id_productoterminado='.$Lista[$i]->id)); ?>" class="btn-sm btn-danger" title="Desactivar el producto terminado <?php echo e(ucwords($Lista[$i]->art_nombre)); ?>" data-toggle="tooltip" data-placement="left">Desactivar</a>			
				<?php else: ?>	
            <a href="<?php echo e(url('ProductoTerminado/cambiarestado?id_productoterminado='.$Lista[$i]->id)); ?>" class="btn-sm btn-info" title="Activar el producto terminado <?php echo e(ucwords($Lista[$i]->art_nombre)); ?>" data-toggle="tooltip" data-placement="left">Activar</a>							
			  <?php endif; ?>				
			</td>
            <td align="center" valign="middle">		  
            <a href="<?php echo e(url('ProductoTerminado/editar?id_productoterminado='.$Lista[$i]->id)); ?>" class="btn-sm btn-info" title="Editar los datos del producto terminado <?php echo e(ucwords($Lista[$i]->art_nombre)); ?>" data-toggle="tooltip" data-placement="left">Editar</a>						
			</td>				
<?php endif; ?>
            </tr>
			<?php endif; ?>			
          <?php endfor; ?>
          </tbody>
        </table>     
      </div>
    </div>
  </div>
<?php endif; ?>  
<?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
$(document).ready(function(){
var arr = [];  
});

	function addinsumo(){	
		$("#contenido").append('<br class="limpiar"><div class="col-md-6"><label  class="limpiar">Materia Prima *</label><select required="" class="form-control limpiar" name="MPnombre[]"><?php foreach($MateriaPrima as $MP){?><option value="<?php echo e($MP->id); ?>"><?php echo e($MP->cod); ?></option><?php }?><?php foreach($array as $MP){?><option value="<?php echo e(count($MateriaPrima)+count($array)); ?>"><?php echo e($MP); ?></option><?php }?></select></div><div class="col-md-6"><label class="limpiar">Cantidad de requisito</label><input required="" step="any" placeholder="0.00" type="number"  id="venci" class="form-control date limpiar" name="insumos[]"></div>');

	}
	function limpiar(){
		$("input").remove(".limpiar");
		$("label").remove(".limpiar");
		$("select").remove(".limpiar");
		$("br").remove(".limpiar");
	}
	function limpiarinsumo(){
		$("input").remove(".limpiarinsumo");
		$("label").remove(".limpiarinsumo");
		$("select").remove(".limpiarinsumo");
		$("br").remove(".limpiarinsumo");
	}	
	function registrar(){	
		$("#contenidoinsumos").append('<br class="limpiarinsumo"><div class="col-md-4"><label  class="limpiarinsumo">Nombre *</label><input required="" type="text"  class="form-control limpiarinsumo" name="nombre[]"></div><div class="col-md-4"><label  class="limpiarinsumo">Detalle *</label><input required="" type="text"  class="form-control limpiarinsumo" name="detalle[]"></div><div class="col-md-4"><label class="limpiarinsumo">Unidad de Medida</label><input required="" type="text"  class="form-control limpiarinsumo" name="unidad[]"><label class="limpiarinsumo">Cantidad Requisito</label><input required="" step="any" placeholder="0.00" type="number"  id="venci" class="form-control date limpiarinsumo" name="insumosreq[]"></div>');

	}	

</script>
<!-- mb_strtoupper -->