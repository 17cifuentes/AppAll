<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[18] == '1'): ?>
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Facturación</th>
            <th align="center" valign="middle">
<?php if($permisos[19] == '1'): ?>			
			<a href="<?php echo e(url('Factura/registrar')); ?>" class="btn btn-success" title="Ver usuarios activos"><span class="fa fa-shopping-cart"></span> <span class="fa fa-exchange"> Nueva Factura</a>
<?php endif; ?>			
			</th>
            <th align="center" valign="middle">
<?php if($permisos[22] == '1'): ?>				
			<a target="_blank" href="<?php echo e(url('Factura/facturero')); ?>" class="btn btn-danger" title="Ver Facturero"><span class="fa fa-eye"></span> <span class="fa fa-exchange"> Facturero</a>
<?php endif; ?>			
			</th>			
        </tr>
    </thead>	
	</table>	
        </div>
		<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
			  <th># Número</th>			
              <th>Fecha</th>
              <th>Cliente</th>
              <th>Vendedor</th>
			  <th>Ruta</th>
              <th>Pago</th>
              <th>Total Efectuado</th>
			  <th>Estado</th>		
<?php if($permisos[21] == '1'): ?>				  
              <th>Cambiar Estado</th>
<?php endif; ?>			  
<?php if($permisos[20] == '1'): ?>				  
			  <th>Editar</th>
<?php endif; ?>			  
            </tr>
          </thead>
          <tbody>
          <?php $__currentLoopData = $clientes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		  <?php if($usuario->id == Auth::user()->id ||  Auth::user()->rol == "Administrador"): ?>		  
            <tr>
			  <td class='text-center'>
			  <a href="pdf?num_factura=<?php echo e($usuario->num_factura); ?>&ID=0" onclick="" title="Descargar Factura" data-toggle="tooltip" data-placement="right"><i class="fa fa-download"></i></a>
			  <a href="pdf?num_factura=<?php echo e($usuario->num_factura); ?>&ID=1" target="_blank" class="btn-sm btn-info">Ver#<?php echo e(str_pad( $usuario->num_factura, 4, "0", STR_PAD_LEFT )); ?></a> 
			  </td>			
              <td><?php echo e($usuario->fecha); ?></td>
              <td>
			  <a title="Correo: <?php echo e(($usuario->correo_cliente)); ?>" data-toggle="tooltip" data-placement="left" href="mailto:
		<?php echo e(($usuario->correo_cliente)); ?>

		?subject=
		Factura%20de%20Compra
		&body=
		<?php echo e(ucwords($usuario->nombre_clientes)); ?>, la siguiente es la factura de su compra
		">			  
			  <?php echo e(ucwords($usuario->nombre_clientes)); ?>

			  </a>
			  </td>
              <td><?php echo e(ucwords($usuario->nombre)); ?></td>
			  <td><?php echo e(ucwords($usuario->ciudad)); ?></td>
              <td>
			  <?php if( $usuario->tipo_pago === "Efectivo"): ?>
				  <span class="label label-success">
				  <?php echo e($usuario->tipo_pago); ?>

				  </span>
			  <?php elseif( $usuario->tipo_pago != "Efectivo"): ?>
				  <span class="label label-warning">
				  <?php echo e($usuario->tipo_pago); ?>

				  </span>			  
			  <?php endif; ?>
			  </td>
			  <td align= "right">
			  <?php if( $usuario->tipo_pago === "Efectivo"): ?>
				  <span class="label label-success">
				  $ <?php echo e(number_format($usuario->total_venta,0)); ?>

				  </span>
			  <?php elseif( $usuario->tipo_pago != "Efectivo"): ?>
					<?php if( $usuario->total_venta == 0): ?>
					  <span class="label label-success">
					  $ <?php echo e(number_format($usuario->total_venta,0)); ?>

					  </span>
					<?php else: ?>
					  <span class="label label-warning">
					  $ <?php echo e(number_format($usuario->total_venta,0)); ?>

					  </span>					  
					<?php endif; ?>
			  <?php endif; ?>			  
			  
			  </td>
              <td align="center" valign="middle">
			  <?php if( $usuario->estado_factura === "Pendiente"): ?>
				  <!--<span class="label label-danger"><?php echo e($usuario->estado_factura); ?></span>-->
				<span class="badge badge-warning badge-icon"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Pendiente</span></span>				  
			  <?php elseif( $usuario->estado_factura === "Pagado"): ?>
				  <!--<span class="label label-success"><?php echo e($usuario->estado_factura); ?></span>-->
				  <span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span>Pagado</span></span>
			  <?php elseif( $usuario->estado_factura === "Entregado"): ?>
				  <!--<span class="label label-info"><?php echo e($usuario->estado_factura); ?></span>-->
				  <span class="badge badge-primary badge-icon"><i class="fa fa-truck" aria-hidden="true"></i><span>Entregado</span></span>
			  <?php elseif( $usuario->estado_factura === "Cuota"): ?>
				  <span class="label label-warning">
				  <?php echo e($usuario->estado_factura); ?>

				  </span>
			  <?php endif; ?>			  
			  </td>			
<?php if($permisos[21] == '1'): ?>				  
              <td align="center" valign="middle">
				<?php if($usuario->tipo_pago === "Efectivo"): ?>
					<?php if($usuario->estado_factura === "Pendiente"): ?>
						<a href="<?php echo e(url('Factura/cambiarestado?id='.$usuario->id_facturas)); ?>" class="btn-sm btn-success" title="La factura #<?php echo e($usuario->num_factura); ?> ha sido pagada" data-toggle="tooltip" data-placement="left">Pagado</a>
					<?php elseif($usuario->estado_factura === "Pagado"): ?>
						<a href="<?php echo e(url('Factura/cambiarestado?id='.$usuario->id_facturas)); ?>" class="btn-sm btn-info" title="La factura #<?php echo e($usuario->num_factura); ?> va a ser entregada" data-toggle="tooltip" data-placement="left">Entregar</a>			  
					<?php endif; ?>
				<?php elseif( $usuario->tipo_pago != "Efectivo"): ?>				  
					<?php if($usuario->estado_factura === "Pendiente"): ?>
						<a href="<?php echo e(url('Factura/cambiarestado?id='.$usuario->id_facturas)); ?>" class="btn-sm btn-info" title="La factura #<?php echo e($usuario->num_factura); ?> va a ser entregada, es pagada como crédito" data-toggle="tooltip" data-placement="left">Entregar</a>
					<?php elseif(($usuario->estado_factura === "Entregado"  || $usuario->estado_factura === "Cuota") && $usuario->total_venta != 0): ?>
						<!-- Modal -->			
						<a type="button" class="btn-sm btn-warning " data-toggle="modal" data-target="#cuota<?php echo e($usuario->id_facturas); ?>" title="Registrar en la factura #<?php echo e($usuario->num_factura); ?> pago de cuota crédito" data-toggle="tooltip" data-placement="left">Cuota</a>
						<!-- ModalBODY -->
							<div class="modal fade" id="cuota<?php echo e($usuario->id_facturas); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<h4 class="modal-title" id="myModalLabel">Pagar cuota del crédito</h4>
								  </div>
							  <form action="<?php echo e(url('Factura/cuota')); ?>" method="POST">		  
								<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"> 
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									<label>Adeudado </label>
									<input readonly type="text" readonly id="venci" class="form-control date" name="Adeudado" value="<?php echo e($usuario->total_venta); ?>" >
									<input type="hidden" class="form-control date"  value="<?php echo e($usuario->id_facturas); ?>" name="Id">           
									</div>
									<div class="col-md-6">
									<label>A pagar</label>	
									<input type="number"  required="" id="Cantidad" class="form-control date" name="Cantidad" value= "0">
									</div>
								  </div>
								</div>			
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
									<button type="submit" class="btn btn-primary">Registrar</button> 
								  </div>
							</form>		  
								</div>
							  </div>
							</div>			
						<!-- Modal <?php echo e(url('Factura/ver?id='.$usuario->id_facturas)); ?> -->
			
					<?php endif; ?>
				<?php endif; ?>
			  </td>
<?php endif; ?>			  
<?php if($permisos[20] == '1'): ?>				  			  
			  <td>
			  <?php if($usuario->estado_factura === "Pendiente"): ?>
			  <a href="<?php echo e(url('Factura/editar?id='.$usuario->id_facturas)); ?>" class="btn-sm btn-primary" title="Editar datos de la factura #<?php echo e($usuario->num_factura); ?>" data-toggle="tooltip" data-placement="left">Editar</a>
			  <?php endif; ?>
			  </td>
<?php endif; ?>			  			  
            </tr>
			<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
		</div> 
      </div>
    </div>
	
  </div>
<?php endif; ?>  
<?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
