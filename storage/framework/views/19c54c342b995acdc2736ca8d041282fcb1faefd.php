<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Inventario de Productos</th>
        </tr>
    </thead>	
	</table>	
        </div>
		<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>			
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>NOMBRE</th>
              <th>DISPONIBLE</th>
			  <th>Unidad de Medida</th>
			  <th>Precio Unitario</th>
			  <th>Impuesto</th>
			  <th>Ver Requerimientos</th>
			  <th>Agregar Requisito</th>
            </tr>
          </thead>
          <tbody>
		  <?php for($i = 0; $i < count($Lista); $i++): ?>
		  <?php if($Lista[$i]->estado == "Activo"): ?>
            <tr>
              <td><?php echo e(ucwords($Lista[$i]->art_nombre)); ?></td>
              <td align="right"><?php echo e($Inventario[$i]->disponible); ?></td>
			  <td><?php echo e(ucwords($Lista[$i]->medida)); ?></td>
              <td align="right"><?php echo e($Lista[$i]->precio_unitario); ?></td>
              <td align="right"><?php echo e($Lista[$i]->impuesto); ?></td>		  
            <td align="center" valign="middle">		  
            <a href="<?php echo e(url('ProductoTerminado/verREQ?id_productoterminado='.$Lista[$i]->id)); ?>" class="btn-sm btn-info" title="Ver los requisitos que se necesitan para crear un artículo de <?php echo e(ucwords($Lista[$i]->art_nombre)); ?>" data-toggle="tooltip" data-placement="left">Ver</a>						
			</td>
            <td align="center" valign="middle">		  
            <!-- <a href="<?php echo e(url('ProductoTerminado/addreqview?id_productoterminado='.$Lista[$i]->id)); ?>" class="btn btn-success">Agregar</a> -->
			<!-- Modal -->			
			<a type="button" class="btn-sm btn-success btn-sm" data-toggle="modal" data-target="#addreq<?php echo e($Lista[$i]->id); ?>" title="Agregar requisitos para crear un artículo de <?php echo e(ucwords($Lista[$i]->art_nombre)); ?>" data-toggle="tooltip" data-placement="left">Agregar</a>			
			<!-- ModalBODY -->
				<div class="modal fade" id="addreq<?php echo e($Lista[$i]->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">AÑADIR REQUISITO PARA <?php echo e($Lista[$i]->cod); ?></h4>
					  </div>
				  <form action="<?php echo e(url('ProductoTerminado/addreq')); ?>" method="POST">		  
				  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"> 
					<div class="card-body">
					  <div class="row">
						<div class="col-md-6">
							<label>Materia Prima *</label>
						  <select required="" class="form-control" name="MateriaPrima">
							<?php $__currentLoopData = $MateriaPrima; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $MP): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($MP->id); ?>"><?php echo e($MP->cod); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>				
						  </select>	
						  <input type="hidden" class="form-control date"  value="<?php echo e($Lista[$i]->id); ?>" name="Id">           
						</div>
						<input type="hidden" readonly id="venci" class="form-control date" name="Detalle" value="<?php echo e($Lista[$i]->art_nombre); ?>">			
						<div class="col-md-6">
						<label>Cantidad de requisito</label>	
						<input required="" step="any" placeholder="0.00" type="number"  id="venci" class="form-control date" name="Cantidad">
						</div>
					  </div>
					</div>			
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary">Agregar</button>
					  </div>
			  </form>		  
					</div>
				  </div>
				</div>			
			<!-- Modal -->
			</td>				
            </tr>
		<?php endif; ?>
          <?php endfor; ?>
          </tbody>
        </table>      
      </div>
    </div>
  </div>
<?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
