<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- Jquery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Datepicker Files -->
    <script src="<?php echo e(asset('datePicker/js/bootstrap-datepicker.js')); ?>"></script>
    <!-- Languaje -->
    <script src="<?php echo e(asset('datePicker/locales/bootstrap-datepicker.es.min.js')); ?>"></script>
<style>
    input[type=number] {
       width: 70px;
    }
	
.col-centered{
    float: none;
    margin: 0 auto;
}	
</style>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[20] == '1'): ?>
  <div class="row">
    <div class="col-md-12">    
	<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">   
      <div class="card">
        <div class="card-header">
          <h2><center>Editar Factura <?php echo e(str_pad($Factura[0]->num_factura, 4, "0", STR_PAD_LEFT )); ?> </center></h2>
        </div>    
        <?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
			<label>Número Factura </label>
			<input type="text" readonly id="venci" class="form-control date" name="Num" value="<?php echo e(str_pad($Factura[0]->num_factura, 4, '0', STR_PAD_LEFT )); ?>">
			<input type="hidden" readonly id="Id" class="form-control date" name="Id" value="<?php echo e($Factura[0]->id_facturas); ?>">
            <label>Clientes *</label>
			<input type="text" readonly id="venci" class="form-control date" name="Num" value="<?php echo e($Factura[0]->nombre_clientes); ?>">
            <label>Vendedor *</label>
			<input type="text" readonly id="venci" class="form-control date" name="Num" value="<?php echo e($Factura[0]->nombre); ?>">
            </div>
            <div class="col-md-6">
			<label>Tipo de Pago</label>	
			<input type="text" readonly id="venci" class="form-control date" name="Num" value="<?php echo e($Factura[0]->tipo_pago); ?>">
			<label>Fecha</label>	
			<input type="text" id="Fecha" class="form-control datepicker" name="Fecha" value="<?php echo date('d/m/Y'); ?>">
			<button type="button" class="btn btn-success " data-toggle="modal" data-target="#myModal">
				<span class="fa fa-search"></span> Agregar productos
			</button>			
            </div>		  
          </div>
        </div>
      </div>
    </div>		
	
			<!-- ModalBODY -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Buscar Artículos</h4>
					</div>
		<table class="datatable table table-striped primary">
		<thead>
            <tr>
              <th>Artículo</th>	  
              <th>Precio</th>
			  <th>Medida</th>
              <th>Cantidad</th>				  
			  <th>Agregar</th>
            </tr>
		</thead>
		<tbody>
				<?php $__currentLoopData = $artículos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $objeto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>				
				<td><?php echo e($objeto->art_nombre); ?></td>
				<td>
				<input type="text" readonly size="4" id="Precio<?php echo e($objeto->id); ?>" name="Precio<?php echo e($objeto->id); ?>" value="<?php echo e($objeto->precio_unitario); ?>">				
				</td>
				<td><?php echo e($objeto->medida); ?></td>
				<td>
				<input type="number" required="" id="Cantidad<?php echo e($objeto->id); ?>" name="Cantidad<?php echo e($objeto->id); ?>" value="0">
				</td>
				<td class='text-center'>
				<a class='btn btn-info'href="#" onclick="agregar(<?php echo e($objeto->id); ?>,Cantidad<?php echo e($objeto->id); ?>,Precio<?php echo e($objeto->id); ?>)"><i class="glyphicon glyphicon-plus"></i></a>
				</td>				
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>			
		</tbody>
        </table>
					  
					</div>
				  </div>
				</div>			
			<!-- Modal -->
	


    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
		<table class="table table-striped primary">
		<thead>
            <tr>
			<th> </th>			
              <th>Artículo</th>	  
              <th>Cantidad</th>
			  <th>Precio Unitario</th>
              <th>Impuesto</th>				  
			  <th>Precio Total</th>
            </tr>
		</thead>
		<tbody>
				<?php $subtotal= 0; $impuestos= 0; $totalventa= 0; ?>
				<?php $__currentLoopData = $detalle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $objeto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>				
				<td class='text-center'>
				<a href="eliminardetalledeeditar?Id=<?php echo e($objeto->id_detalle); ?>&num=<?php echo e($Factura[0]->id_facturas); ?>&precio=<?php echo e($objeto->precio_venta * $objeto->cantidad); ?>" onclick="">
				<i class="fa fa-trash"></i></a>
				</td>
				<td><?php echo e($objeto->art_nombre); ?></td>
				<td><?php echo e($objeto->cantidad); ?></td>
				<td><?php echo e($objeto->precio_venta); ?></td>
				<td align="right">
				<?php $impuestos= $impuestos + round(($objeto->precio_venta * $objeto->cantidad) - ($objeto->precio_venta * $objeto->cantidad)/($objeto->impuesto/100+1),0);?>
				<?php echo e(round(($objeto->precio_venta * $objeto->cantidad)-($objeto->precio_venta * $objeto->cantidad) / ($objeto->impuesto/100+1),0)); ?>

				</td>
				<td align="right">
				<?php $totalventa= $totalventa + $objeto->precio_venta * $objeto->cantidad; ?>
				<?php echo e($objeto->precio_venta * $objeto->cantidad); ?></td>				
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
				<?php $subtotal= $totalventa - $impuestos; ?>
		</tbody>
        </table>

		<table class="table table-striped primary" >
		<thead>
            <tr>
			  <th> </th>			
              <th> </th>	  
              <th> </th>
			  <th> </th>
              <th> </th>				  
			  <th> </th>
            </tr>
		</thead>
		<tbody>
				<tr> 
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td align="right">Subtotal $ </td>
				<td align="right"> <?php echo $subtotal; ?> </td>
				</tr>
	
				<tr> 
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td align="right">Impuestos $ </td>
				<td align="right"> <?php echo $impuestos; ?> </td>
				</tr>

				<tr> 
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td align="right">Total a Pagar $ </td>
				<td align="right"> 
				<?php echo $totalventa; ?> 
				<input type="hidden" readonly id="TVenta" class="form-control date" name="TVenta" value="<?php echo $totalventa; ?> ">
				</td>
				</tr>				
		</tbody>
        </table>		
		
        </div>
      </div>
    </div>    
	
    <div class="col-md-12">
	  	<center><a href="index" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>
    </div>
	
	</div>
      </div>
    </div>
<script>
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true
    });
	
function agregar(Id, Cantidad, Precio){
var nuumero = $("#Id").val();
//alert(nuumero);

window.location.href = "agregardeeditar?Id="+ Id + "&Cantidad=" + Cantidad.value + "&Precio=" + Precio.value + "&num=" + nuumero;
	}
</script>
	
<?php endif; ?>	
  <?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
