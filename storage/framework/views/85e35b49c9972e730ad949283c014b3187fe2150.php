<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[6] == '1'): ?>
  <div class="row">
    <div class="col-md-12">    
      <form action="<?php echo e(url('MateriaPrima/addprovider')); ?>" method="POST">
	<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">     	 
      <div class="card">
        <div class="card-header">
          <h2>VER MOVIMIENTO PARA (<?php echo e($artiiculo[0]->cod); ?>) </h2>
        </div>
		
	
		<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>		
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>DETALLE</th>
			  <th>COSTO x UNIDAD $</th>
              <th>SUBIDA</th>
              <th>BAJADA</th>			  
			  <th>FECHA</th>
            </tr>
          </thead>
          <tbody>
		  <?php for($i = 0; $i < count($det_prov); $i++): ?>
		  <tr>
		  
			<td>
				<?php echo e($det_prov[$i]->detalle); ?>

			</td>
			<td align="right">
				<?php echo e($det_prov[$i]->costo); ?>

			</td>			
		  <td>
		  <?php if( $det_prov[$i]->tipo == 1): ?> <?php echo e($det_prov[$i]->cantidad); ?>

		  <?php endif; ?>
		  </td>
		  <td>
		  <?php if( $det_prov[$i]->tipo == 0): ?> <?php echo e($det_prov[$i]->cantidad); ?>

		  <?php endif; ?>		  
		  </td>		  
		  <td><?php echo e($det_prov[$i]->updated_at); ?></td>	
		  </tr>
          <?php endfor; ?>
          </tbody>
        </table>     
      </div>	
		<label> . Costo promedio de la unidad $ <?php echo e($Inventario[0]->totalcosto); ?></label>
		<center><a href="listar" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>		
	
  </div>
</div>
<?php endif; ?>
    <?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

