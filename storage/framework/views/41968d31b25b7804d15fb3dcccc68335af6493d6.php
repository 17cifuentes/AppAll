<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[0] == '1'): ?>
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Gestión de Usuarios</th>
            <th align="center" valign="middle">
			<a href="#" data-toggle="modal" data-target="#ingresar" class="btn btn-info" title="Agregar Usuario" data-toggle="tooltip" data-placement="left"><span class="fa fa-plus"></span> <span class="fa fa-exchange"></span> Agregar Usuario</a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="ingresar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<form method="POST" action="<?php echo e(url('usuario/postregistrar')); ?>" > 
								<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">     
								<input type="hidden" name="user_estado" value="Activo"  > 
							  <div class="col-md-12">
							  <div class="card">
								<div class="card-header">
								  <h4>Datos De Usuario: </h4>
								</div>
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									  <label>Nombre *</label>
									  <input type="text" required="" class="form-control" name="name" maxlength="250">  
									  <label>Contraseña *</label>
									  <input type="password" required="" name="password" class="form-control" maxlength="250">  
									</div>
									<div class="col-md-6">
									  <label>Correo *</label>
									  <input type="email" name="email" class="form-control" maxlength="250">
									  <br><br>
									  <label>Cargo *</label>
									  <select class="select" name="rol">
										<option value="">Selección</option>
										<option value="R.Humanos">Recursos Humanos</option>
										<option value="Vendedor">Vendedor</option>
										<option value="Almacenista">Almacenista</option>
										<option value="Operario">Operario</option>
										<option value="Administrador">Administrador</option>
									  </select>		  
									</div>
								  </div>
								</div>
							  </div>
							  </div>
							<div class="col-md-12">
							  <div class="card">
								<div class="card-header">
								  <h4>Datos Adicionales: </h4>
								</div>
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									  <label>Teléfono </label>
									  <input type="text"  class="form-control" name="Teléfono" maxlength="250">  
									  <label>Dirección</label>
									  <input type="text" name="address" class="form-control" maxlength="250">  
									</div>
									<div class="col-md-6">
									  <label>Salario $</label>
									  <input type="number"  name="Salario" class="form-control" value="0">
										<label>Cuota</label>
									  <input type="number"  name="Cuota" class="form-control" value="0">									  
									</div>
								  </div>
								</div>
							  </div>
							</div>
							<div class="col-md-12">
							  <center><input type="submit" class="btn btn-success" value="Registrar Usuario"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  

						<!-- Modal -->
			</th>
            <th align="center" valign="middle">
			<?php if($ver == "Inactivo"): ?>
			<a href="<?php echo e(url('usuario/listar?ver=Activo')); ?>" class="btn btn-success" title="Ver usuarios activos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Activos</a>
			<?php else: ?>
			<a href="<?php echo e(url('usuario/listar?ver=Inactivo')); ?>" class="btn btn-danger" title="Ver usuarios inactivos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Inactivos</a>			
			<?php endif; ?>
			</th>
        </tr>
    </thead>	
	</table>	
        </div>
        <?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Cargo</th>
            <th>Estado</th>
<?php if($permisos[1] == '1'): ?>
            <th>Activar / Desactivar</th>
            <th>Editar</th>
<?php endif; ?>
            <th>Fotografía</th>			
        </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php if($user->user_estado === $ver && $user->rol != "Cliente" ): ?>
        <tr>
            <td><?php echo e(ucwords($user->name)); ?></td>
            <td><?php echo e($user->email); ?></td>
            <td><?php echo e($user->rol); ?></td>
            <td>
			<?php if($user->user_estado == "Activo"): ?>
			<span class="label label-success">
			<?php echo e($user->user_estado); ?>

			</span>	
			<?php elseif( $user->user_estado == "Inactivo"): ?>
				  <span class="label label-danger">
			<?php echo e($user->user_estado); ?>

				  </span>			  
			<?php endif; ?>			
			</td>
<?php if($permisos[1] == '1'): ?>			
            <td align="center" valign="middle">
			<?php if($user->user_estado === "Activo"): ?>			
			<a href="<?php echo e(url('usuario/cambiarestado?id='.$user->id)); ?>" class="btn-sm btn-danger" title="Desactivar el usuario <?php echo e(ucwords($user->name)); ?>" data-toggle="tooltip" data-placement="left">Desactivar</a>
			<?php else: ?>
			<a href="<?php echo e(url('usuario/cambiarestado?id='.$user->id)); ?>" class="btn-sm btn-info" title="Activar el usuario <?php echo e(ucwords($user->name)); ?>" data-toggle="tooltip" data-placement="left" >Activar</a>
			<?php endif; ?>
			</td>
			
            <td><a href="<?php echo e(url('usuario/editar?id='.$user->id)); ?>" class="btn-sm btn-warning" title="Editar el registro del usuario <?php echo e(ucwords($user->name)); ?>" data-toggle="tooltip" data-placement="left">Editar</a></td>			
<?php endif; ?>			
            <td>
			<a data-toggle="modal" data-target="#ver<?php echo e($user->id); ?>" class="btn-sm btn-primary" title="Ver fotografía de <?php echo e(ucwords($user->name)); ?>" data-toggle="tooltip" data-placement="left">Ver</a>
				<div id="ver<?php echo e($user->id); ?>" class="modal fade" role="dialog">  
				<div class="modal-dialog">
					<div class="modal-content">      
						<div class="modal-header">        
							<button type="button" class="close" data-dismiss="modal">×</button>        
							<h4 class="modal-title">Fotografía <?php echo e(ucwords($user->name)); ?></h4>      </div>      
						<div class="modal-body"><center><img src="<?php echo e(asset( $user->pic)); ?>" class="img-rounded" alt="Modal Pic" width="304" height="236" /> </center>  </div>      
						<div class="modal-footer">        
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>     
						</div>  
					</div>  
				</div>
			</td>			
			
        </tr>
		<?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
        </div>
      </div>
    </div>
<?php endif; ?>
<?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

