<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[8] == '1'): ?>
  <div class="row">
    <div class="col-md-12">    
      <form action="<?php echo e(url('ProductoTerminado/posteditar')); ?>" method="POST">
	<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">     	 
      <div class="card">
        <div class="card-header">
          <h2>EDITAR ARTÍCULO (<?php echo e($artiiculo[0]->art_nombre); ?>)</h2>
        </div>
        <?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-header">
          <h4>Datos del Artículo: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
			<label>Código </label>
			<input type="text" required="" id="venci" class="form-control date" name="Código" value="<?php echo e($artiiculo[0]->art_nombre); ?>">
			<input type="hidden" class="form-control date"  value="<?php echo e($artiiculo[0]->id); ?>" name="Id">								
			<label>Detalle </label>
			<input type="text"  id="venci" class="form-control date" name="Detalle" value="<?php echo e($artiiculo[0]->detalle); ?>">
			<label>Unidad de Medida </label>
			<input type="text"  id="venci" class="form-control date" name="Medida" value="<?php echo e($artiiculo[0]->medida); ?>">
			<label>Precio Unitario </label>
			<input type="number"  id="venci" class="form-control date" name="Precio" value="<?php echo e($artiiculo[0]->precio_unitario); ?>">
			<label>Impuesto % </label>
			<input type="number"  id="venci" class="form-control date" name="Impuesto" value="<?php echo e($artiiculo[0]->impuesto); ?>">				
            </div>
          </div>
        </div>
      </div>
    </div>		
	
    <div class="col-md-12">
      <center><input type="submit" class="btn btn-success" value="Actualizar Artículo"></center>
    </div>	
	
  </form>
  </div>
  
  <div class="row">
    <div class="col-md-12">    
      <div class="card">
        <div class="card-header">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
		<tr>	
          <th ><p style="font-size:40px">VER REQUERIMIENTOS PARA (<?php echo e($artiiculo[0]->art_nombre); ?>)</th>
		  <th align="center" valign="middle">
		  <a type="button" class="btn btn-success" data-toggle="modal" data-target="#addreq" title="Agregar requisitos para crear artículo" data-toggle="tooltip" data-placement="left">Agregar Requisito</a>
		  </th>
		</tr>		  
    </thead>	
	</table>		
        </div>
		
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
				<th> </th>
              <th>MATERIA PRIMA</th>
              <th>CANTIDAD</th>
			  <th>FECHA</th>
            </tr>
          </thead>
          <tbody>
		  <?php for($i = 0; $i < count($det_prov); $i++): ?>
		  <tr>
				<td class='text-center'>
				<a href="eliminardetalle?Id=<?php echo e($det_prov[$i]->id_requisitoart); ?>&IdPT=<?php echo e($id_pt); ?>" onclick="">
				<i class="fa fa-trash" title="Eliminar requisito"></i></a>
				</td>		  
		  <td><?php echo e($MP[$det_prov[$i]->id_mp-1]->cod); ?></td>
		  <td align="right"><?php echo e($det_prov[$i]->cantidad); ?>

				<a href="#" data-toggle="modal" data-target="#editar<?php echo e($det_prov[$i]->id_requisitoart); ?>">
				<i class="fa fa-edit" title="Editar Cantidad"></i></a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="editar<?php echo e($det_prov[$i]->id_requisitoart); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
							<form action="editardetalle" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">     	 
							<div class="modal-content">
					  <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Editar Cantidad del Requisito de <?php echo e($MP[$det_prov[$i]->id_mp-1]->cod); ?></h4>
					  </div>							
								<div class="card-body">
								  <div class="row">
									<div class="col-md-12">
									<label>Cantidad </label>
									<input type="text" id="Cantidad" class="form-control date" name="Cantidad" maxlength="250" value="<?php echo e($det_prov[$i]->cantidad); ?>">
									<input type="hidden" id="Id" class="form-control date" name="Id" value="<?php echo e($det_prov[$i]->id_requisitoart); ?>">
									<input type="hidden" id="IdPT" class="form-control date" name="IdPT" value="<?php echo e($id_pt); ?>">
									</div>
								  </div>
								</div>
							<div class="modal-footer">
							  <center><input type="submit" class="btn btn-success" value="Actualizar Cantidad"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  
						  </div>
						</div>	
						<!-- Modal -->				
			<!-- ModalBODY -->
				<div class="modal fade" id="addreq" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">AÑADIR REQUISITO</h4>
					  </div>
				  <form action="<?php echo e(url('ProductoTerminado/addreq')); ?>" method="POST">		  
				  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"> 
					<div class="card-body">
					  <div class="row">
						<div class="col-md-6">
							<label>Materia Prima *</label>
						  <select required="" class="form-control" name="MateriaPrima">
							<?php $__currentLoopData = $MP; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $elemento): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($elemento->id); ?>"><?php echo e($elemento->cod); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>				
						  </select>	
						  <input type="hidden" class="form-control date"  value="<?php echo e($id_pt); ?>" name="Id">           
						</div>
						<div class="col-md-6">
						<label>Cantidad de requisito</label>	
						<input required="" step="any" placeholder="0.00" type="number"  id="venci" class="form-control date" name="Cantidad">
						</div>
					  </div>
					</div>			
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary">Agregar</button>
					  </div>
			  </form>		  
					</div>
				  </div>
				</div>			
			<!-- Modal -->
						
			</td>		  
		  <td><?php echo e($det_prov[$i]->created_at); ?></td>
		  </tr>
          <?php endfor; ?>
          </tbody>
        </table>     
      </div>	
		<center><a href="index" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>	  

	
	
  </div>
</div>  
</div>
<?php endif; ?>
    <?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
