<!DOCTYPE html>
<html>
<head>
<?php $nombre = Session::get('empresa') ?>
  <title><?php echo $nombre ?></title>
  <META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('dash/assets/css/vendor.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('dash/assets/css/flat-admin.css')); ?>">

  <!-- Theme -->
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('dash/assets/css/theme/blue-sky.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('dash/assets/css/theme/blue.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('dash/assets/css/theme/red.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('dash/assets/css/theme/yellow.css')); ?>">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	
<style>	
body {
	background: linear-gradient(-90deg, #001015, #002015, #fdfef8, #edefe0, #102233, #122434, #ffeeff, #fdf0f9);
	background-size: 400% 400%;
	-webkit-animation: Gradient 30s ease infinite;
	-moz-animation: Gradient 30s ease infinite;
	animation: Gradient 30s ease infinite;
}
@-webkit-keyframes Gradient {
	0% {
		background-position: 0% 50%
	}
	50% {
		background-position: 100% 50%
	}
	100% {
		background-position: 0% 50%
	}
}

@-moz-keyframes Gradient {
	0% {
		background-position: 0% 50%
	}
	50% {
		background-position: 100% 50%
	}
	100% {
		background-position: 0% 50%
	}
}

@keyframes  Gradient {
	0% {
		background-position: 0% 50%
	}
	50% {
		background-position: 100% 50%
	}
	100% {
		background-position: 0% 50%
	}
}


@keyframes  circle 
{
from {transform: scale(0)}
to {transform: scale(6)}
}
.circle 
{
  margin: 0 auto;
  width: 300px;
  height: 500px;
  border: 2px solid #879380;
  border-radius: 50%;
  position: absolute;
  top: 50%;
  left: 40%;
}
.one 
{animation: circle 5s infinite linear;}
.two 
{animation: circle 4s infinite linear;}
.three 
{animation: circle 3s infinite linear;}
.four 
{animation: circle 2s infinite linear;}
.five 
{animation: circle 1s infinite linear;}

</style>
	
</head>
<body>
  <div class="app app-default">
<!--  
<div class="circle one"></div>
<div class="circle two"></div>
<div class="circle three"></div>
<div class="circle four"></div>
<div class="circle five"></div>  
-->