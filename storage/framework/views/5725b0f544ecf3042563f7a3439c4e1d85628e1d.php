<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[24] == '1'): ?>
    <div class="col-md-12">    
      <form action="<?php echo e(url('Factura/postpedido')); ?>" method="POST"">
	<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">   
		<input type="hidden" value="" id="farticulo" name="farticulo[]">
		<input type="hidden" value="" id="fcantidad" name="fcantidad[]">
		<input type="hidden" value="" id="fimpuesto" name="fimpuesto[]">
		<input type="hidden" value="" id="fbruto" name="fbruto[]">
		<input type="hidden" value="" id="fsubtotal" name="fsubtotal">
		<input type="hidden" value="" id="ftotalimpuestos" name="ftotalimpuestos">
		<input type="hidden" value="" id="fgrantotal" name="fgrantotal">
		<input type="hidden" readonly id="Cliente" class="form-control" name="Cliente" value="<?php echo e($Cliente); ?>">		
      <div class="card">
        <?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-header">	
          <h4>Pedidos </h4>
        </div>
        <div class="card-body">
		<div class="row">
            <div class="col-md-12">
			<label>Tipo de Pago</label>	
			<select required="" class="form-control" name="pago">
                <option value="Efectivo">Efectivo</option>	
				<option value="Crédito 30">Crédito 30 Días</option>	
				<option value="Crédito 60">Crédito 60 Días</option>
				<option value="Crédito 90">Crédito 90 Días</option>
			</select>	
			<br><br>
			</div>
			<div class="col-md-12">
			<div class="col-md-3">
				<label>Articulo *</label>
				<select id="idarticulo" class="form-control date" onchange="prodatos()">
					<option value="-1">Seleccione</option>
					<?php $__currentLoopData = $artículos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $articulo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php if($articulo->estado == "Activo"): ?>
						<option value="<?php echo e($articulo->id); ?>"><?php echo e($articulo->art_nombre); ?></option>
						<?php endif; ?>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
			</div>
			<div class="col-md-3">
				<label>Precio *</label>
				<input type="text" readonly required="" id="idprecio" class="form-control date" name="" value="">			
			</div>
			<div class="col-md-3">
				<label>Impuesto *</label>
				<input type="text" readonly id="idimpuesto" class="form-control date" name="" value="">			
			</div>
			<div class="col-md-3">
				<label>Cantidad *</label>
				<input type="text" required="" id="idcantidad" class="form-control date" name="" value="0">			
			</div>
			<button type="button" class="btn btn-success " onclick="gestion()">
				<span class="fa fa-search"></span> Agregar productos
			</button>			
            </div>		
		</div>
        </div>
      </div>
    </div>	
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
		<table class="table table-striped primary">
		<thead>
            <tr>
			  <th></th>	  
			  <th>Artículo</th>	 
              <th>Cantidad</th>
			  <th>Precio Unitario</th>
              <th>Impuesto</th>				  
			  <th>Precio Total</th>
            </tr>
		</thead>
		<tbody id="conten">
		</tbody>
        </table>		
        </div>
      </div>
    </div>	
	<div class="col-lg-4 col-lg-offset-8">
		<table class="table panel" >
		<tbody id="contenido">
			<tr>
			<th>Subtotal</th>
			<td>0</td>
			</tr>
			<tr>
			<th>Impuestos</th>
			<td>0</td>
			</tr>
			<tr>
			<th>Total</th>
			<td>0</td>			
			</tr>
		</tbody>
        </table>	
	</div>
	
    
    <div class="col-md-12">
      <center><input type="submit" class="btn btn-success" value="Solicitar"></center>
    </div>		
	</div>
	</form>
      </div>

	  
<?php endif; ?>	
  <?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

 <script>
var arrnomart = Array();var arrarticulo = Array();var arrcantidad = Array();var arrpreciou = Array();var arrimpuesto = Array();
var arrbruto= Array();var arrsubtotal=0;var totalmpuestos = 0;var arrgrantotal=0;


var formatNumber = {
 separador: ".", // separador para los miles
 sepDecimal: ',', // separador para los decimales
 formatear:function (num){
 num +='';
 var splitStr = num.split('.');
 var splitLeft = splitStr[0];
 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
 var regx = /(\d+)(\d{3})/;
 while (regx.test(splitLeft)) {
 splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
 }
 return this.simbol + splitLeft +splitRight;
 },
 new:function(num, simbol){
 this.simbol = simbol ||'';
 return this.formatear(num);
 }
}	
	
function prodatos(){
id = $("#idarticulo").val();
$.ajax({
    url : 'Factura/prodatos',
    data : "id="+id,
	type : 'get',
	success : function(datos){
	var arraya = JSON.parse(datos)
		$("#idprecio").val(arraya.precio);
		$("#idimpuesto").val(arraya.impuesto);
	}
});
}
function gestion(){
	if ($("#idcantidad").val() != 0 && $("#idarticulo").val() != -1)
	{
	addproducto();
	mostrar();
	}
}
function eliminar(id){
	arrarticulo.splice(id, 1);
	arrcantidad.splice(id, 1);
	arrpreciou.splice(id, 1)
	arrimpuesto.splice(id, 1);
	arrbruto.splice(id, 1);
	arrnomart.splice(id, 1);
	deduciones();
	mostrar();
}
function addproducto(){
	arrarticulo.push($("#idarticulo").val());
	arrcantidad.push($("#idcantidad").val());
	arrpreciou.push($("#idprecio").val());
	arrimpuesto.push((($("#idprecio").val() * $("#idcantidad").val())-($("#idprecio").val() * $("#idcantidad").val()) / ($("#idimpuesto").val()/100+1)).toFixed(0) );
	arrbruto.push($("#idcantidad").val() * $("#idprecio").val());
	arrnomart.push($("#idarticulo option:selected").text());
	deduciones();
	
}
function deduciones(){
	totalmpuestos = 0;
	arrgrantotal = 0;
	arrsubtotal = 0;
	for(x = 0; x < arrbruto.length;x++){
		totalmpuestos += arrimpuesto[x];
		 arrgrantotal += arrbruto[x];
	}
	arrsubtotal = arrgrantotal - totalmpuestos;
	addinput();
}
function addinput(){
	$("#farticulo").val(arrarticulo);
	$("#fcantidad").val(arrcantidad);
	$("#fimpuesto").val(arrimpuesto);
	$("#fbruto").val(arrpreciou);
	$("#fsubtotal").val(arrsubtotal);
	$("#ftotalimpuestos").val(totalmpuestos);
	$("#fgrantotal").val(arrgrantotal);
}
function mostrar(){ 
$("#conten").append("");
var string = "";
	for(var x = 0;x < arrarticulo.length;x++){
		string +=
		"<tr><td><a onclick='eliminar("+x+")'><i class='fa fa-trash' title='Eliminar requisito'></i></a></td><td>"+arrnomart[x]+"</td><td>"+arrcantidad[x]+"</td><td align='right'>"+formatNumber.new(arrpreciou[x], "$")+"</td><td align='right'>"+formatNumber.new(arrimpuesto[x], "$")+"</td><td align='right'>"+formatNumber.new(arrbruto[x], "$")+"</td></tr>";
	}
	// aqui va deducciones
	string2 = "<tr><th>Subtotal</th><td align='right'>"+formatNumber.new(arrsubtotal, "$")+"</td></tr><tr><th>Impuestos</th><td align='right'>"+formatNumber.new(totalmpuestos, "$")+"</td></tr><tr><th>Total</th><td align='right'>"+formatNumber.new(arrgrantotal, "$")+"</td></tr>";
	$("#conten").html(string);
	$("#contenido").html(string2);
}


</script>