<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[13] == '1'): ?>
  <div class="row">
    <div class="col-md-12">    
      <form action="<?php echo e(url('Cliente/update')); ?>" method="POST">
	<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">   
      <div class="card">
        <div class="card-header">
          <h2><center>Editar Cliente  (<?php echo e($usuarios[0]->nombre_clientes); ?>) </center></h2>
        </div>    
        <?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-header">	
          <h4>Datos De Usuario: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
			<label>Empresa </label>
			<input type="text" id="venci" class="form-control date" name="Nombre" value="<?php echo e($usuarios[0]->nombre_clientes); ?>">
			<input type="hidden" class="form-control date"  value="<?php echo e($usuarios[0]->id); ?>" name="Id">			
			<label>Teléfono </label>
			<input type="text"  id="venci" class="form-control date" name="Teléfono" value="<?php echo e($usuarios[0]->tel); ?>">
            </div>
            <div class="col-md-6">
            <label>Correo *</label>
			<input type="email" required="" class="form-control" name="Correo" value="<?php echo e($usuarios[0]->correo_cliente); ?>">
			<label>Dirección</label>	
			<input type="text"  id="venci" class="form-control date" name="Address" value="<?php echo e($usuarios[0]->address); ?>">
            <label>Vendedor *</label>
              <select required="" class="form-control" name="PersonalID">
				<?php $__currentLoopData = $vendedores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendedor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($vendedor->rol == "Vendedor"): ?>
                <option value="<?php echo e($vendedor->id); ?>"> <?php echo e($vendedor->nombre); ?> - <?php echo e($vendedor->correo); ?> - <?php echo e($vendedor->cargo); ?> </option>
				<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>				
            </div>
          </div>
        </div>
      </div>
    </div>		
	
    
    <div class="col-md-12">
      <center><input type="submit" class="btn btn-success" value="Actualizar Cliente"></center>
	  	<center><a href="index" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>
    </div>		
  </form>
      </div>
    </div>
<?php endif; ?>	
  <?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
