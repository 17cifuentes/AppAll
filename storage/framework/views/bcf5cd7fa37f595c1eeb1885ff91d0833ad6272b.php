<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[23] == '1'): ?>
  <div class="row">
    <div class="col-md-12">    
      <form action="<?php echo e(url('Empresa/update')); ?>" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">   
      <div class="card">
        <?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-header">	
          <h4>Datos De la Empresa: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
			<label>Empresa </label>
			<input type="text" id="venci" class="form-control date" name="Nombre" value="<?php echo e($info[0]->nombre); ?>">
			<label>Teléfono </label>
			<input type="text"  id="venci" class="form-control date" name="Teléfono" value="<?php echo e($info[0]->tel); ?>">
			<label>Nit </label>
			<input type="text"  id="venci" class="form-control date" name="Nit" value="<?php echo e($info[0]->nit); ?>">			
			<label>Ciudad </label>
			<input type="text"  id="venci" class="form-control date" name="Ciudad" value="<?php echo e($info[0]->ciudad); ?>">
			<label>País </label>
			<input type="text"  id="venci" class="form-control date" name="País" value="<?php echo e($info[0]->paiis); ?>">
			<label>Lema </label>
			<input type="text"  id="venci" class="form-control date" name="Lema" value="<?php echo e($info[0]->lema1); ?>">
			<label>Responsabilidad </label>
			<input type="text"  id="venci" class="form-control date" name="Responsabilidad" value="<?php echo e($info[0]->lema2); ?>">						
			<label>Inicia Factura # </label>
			<input type="number"  id="venci" class="form-control date" name="Factura" value="<?php echo e($info[0]->factura); ?>">									
            </div>
            <div class="col-md-6">
            <label>Correo </label>
			<input type="email" class="form-control" name="Correo" value="">
			<label>Dirección</label>	
			<input type="text"  id="venci" class="form-control date" name="Address" value="<?php echo e($info[0]->address); ?>">
			<label>Código Postal </label>
			<input type="text"  id="venci" class="form-control date" name="CPostal" value="<?php echo e($info[0]->cod_postal); ?>">
			<label>Imagen </label>	
			<img src="<?php echo e(asset($info[0]->pic)); ?>" width="50%">			
            <input type="file"  class="form-control date" name="PIC" id="PIC">	  				
			<input type="hidden" id="PIC_ruta" name="PIC_ruta" value="<?php echo e($info[0]->pic); ?>">			
            </div>
          </div>
        </div>
      </div>
    </div>		
	
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Responsabilidades Empleador: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <label>Salario Básico </label>
              <input type="number" step="any" class="form-control" name="salario" value="<?php echo e($info[0]->salario); ?>">
              <label>Auxilio de Transporte</label>
              <input type="number" step="any" name="auxilio" class="form-control" value="<?php echo e($info[0]->auxilio); ?>">
              <label>% Pensión</label>
              <input type="number" step="any" name="pensioon" class="form-control" value="<?php echo e($info[0]->pensioon); ?>">
              <label>% Salud</label>
              <input type="number" step="any" name="salud" class="form-control" value="<?php echo e($info[0]->salud); ?>">
            </div>
            <div class="col-md-6">
              <label>% Cesantías </label>
              <input type="number" step="any" class="form-control" name="cesantiia" value="<?php echo e($info[0]->cesantiia); ?>">
              <label>% Intereses de Cesantías</label>
              <input type="number" step="any" name="intereses" class="form-control" value="<?php echo e($info[0]->intereses); ?>">
              <label>% Prima</label>
              <input type="number" step="any" name="prima" class="form-control" value="<?php echo e($info[0]->prima); ?>">
              <label>% Vacaciones</label>
              <input type="number" step="any" name="vacaciones" class="form-control" value="<?php echo e($info[0]->vacaciones); ?>">
            </div>
          </div>
        </div>
      </div>
    </div>	
    
    <div class="col-md-12">
      <center><input type="submit" class="btn btn-success" value="Actualizar Datos"></center>
    </div>		
	</div>
	</form>
      </div>
    </div>
<?php endif; ?>	
  <?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
