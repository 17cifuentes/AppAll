<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[12] == '1'): ?>
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Gestión de Clientes</th>
            <th align="center" valign="middle">
			<a href="#" data-toggle="modal" data-target="#ingresar" class="btn btn-info" title="Agregar Cliente"><span class="fa fa-plus"></span> <span class="fa fa-exchange"> Agregar Cliente</a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="ingresar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						  <form action="<?php echo e(url('Cliente/postregistro')); ?>" method="POST">
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">     	 
							  <div class="col-md-12">
						  <div class="card">
							<div class="card-header">
							  <h2>Registro De Cliente</h2>
							</div>
							<div class="card-header">
							  <h4>Datos del Cliente: </h4>
							</div>
							<div class="card-body">
							  <div class="row">
								<div class="col-md-6">
								<label>Nombre </label>
								<input type="text" id="venci" class="form-control date" name="Nombre">
								<label>Teléfono </label>
								<input type="text"  id="venci" class="form-control date" name="Teléfono">
								</div>
								<div class="col-md-6">
								<label>Correo *</label>
								<input type="email" required="" class="form-control" name="Correo" placeholder="">
								<label>Dirección</label>	
								<input type="text"  id="venci" class="form-control date" name="Address">
								<label>Vendedor *</label>
								  <select required="" class="select" name="PersonalID">
									<?php $__currentLoopData = $vendedores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php if($usuario->rol == "Vendedor"): ?>
									<option value="<?php echo e($usuario->id); ?>"> <?php echo e($usuario->nombre); ?> - <?php echo e($usuario->correo); ?> - <?php echo e($usuario->cargo); ?> </option>
									<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								  </select>				
								</div>
							  </div>
							</div>
						  </div>
							  </div>

							<div class="col-md-12">
							  <center><input type="submit" class="btn btn-success" value="Registrar Cliente"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  

						<!-- Modal -->			
			</th>
            <th align="center" valign="middle">
			<?php if($ver == "Inactivo"): ?>
			<a href="<?php echo e(url('Cliente/index?ver=Activo')); ?>" class="btn btn-success" title="Ver usuarios activos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"> Ver Activos</a>
			<?php else: ?>
			<a href="<?php echo e(url('Cliente/index?ver=Inactivo')); ?>" class="btn btn-danger" title="Ver usuarios inactivos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"> Ver Inactivos</a>			
			<?php endif; ?>
			</th>
        </tr>
    </thead>	
	</table>	
        </div>
		<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>NOMBRE</th>
              <th>TELÉFONO</th>
              <th>CORREO</th>
              <th>DIRECCIÓN</th>
			  <th>Vendedor Asignado</th>
              <th>ESTADO</th>
<?php if($permisos[13] == '1'): ?>			  
              <th>Activar / Desactivar</th>		  
			  <th>Editar</th>
<?php endif; ?>			  
            </tr>
          </thead>
          <tbody>
          <?php $__currentLoopData = $usuarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		  <?php if($usuario->estado === $ver): ?>
            <tr>
              <td><?php echo e(ucwords($usuario->nombre_clientes)); ?></td>
              <td><?php echo e($usuario->tel); ?></td>
              <td><?php echo e($usuario->correo_cliente); ?></td>
              <td><?php echo e($usuario->address); ?></td>
			  <td><?php echo e($usuario->nombre); ?></td>
              <td align="center" valign="middle">
			  <?php if( $usuario->estado === "Activo"): ?>
			  <span class="label label-success">
			  <?php echo e($usuario->estado); ?>

			  </span>
			  <?php elseif( $usuario->estado === "Inactivo"): ?>
			  <span class="label label-danger">
			  <?php echo e($usuario->estado); ?>

			  </span>
			  <?php endif; ?>			  
			  </td>  
<?php if($permisos[13] == '1'): ?>				  
              <td align="center" valign="middle">
			  <?php if($usuario->estado === "Activo"): ?>
			  <a href="<?php echo e(url('Cliente/cambiarestado?id='.$usuario->id)); ?>" class="btn-sm btn-danger" title="Desactivar el cliente <?php echo e(ucwords($usuario->nombre_clientes)); ?>" data-toggle="tooltip" data-placement="left">Desactivar</a>
				<?php else: ?>
			  <a href="<?php echo e(url('Cliente/cambiarestado?id='.$usuario->id)); ?>" class="btn-sm btn-info" title="Activar el cliente <?php echo e(ucwords($usuario->nombre_clientes)); ?>" data-toggle="tooltip" data-placement="left">Activar</a>			  
			  <?php endif; ?>
			  </td>
			  <td>
			  <a href="<?php echo e(url('Cliente/editar?id='.$usuario->id)); ?>" class="btn-sm btn-warning" title="Editar los datos del cliente <?php echo e(ucwords($usuario->nombre_clientes)); ?>" data-toggle="tooltip" data-placement="left">Editar</a>
			  </td>
<?php endif; ?>			  
            </tr>
			<?php endif; ?>			
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
		</div> 
      </div>
    </div>
	
  </div>
<?php endif; ?>  
<?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
