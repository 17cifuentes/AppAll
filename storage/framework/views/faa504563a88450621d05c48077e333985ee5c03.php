<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="row">
    <div class="col-md-12">    
      <div class="card">
        <div class="card-header">
          <h2>VER REQUERIMIENTOS PARA (<?php echo e($artiiculo[0]->art_nombre); ?>) </h2>
        </div>
		
	
		<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>		
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
				<th> </th>
              <th>MATERIA PRIMA</th>
              <th>CANTIDAD</th>
			  <th>FECHA</th>
            </tr>
          </thead>
          <tbody>
		  <?php for($i = 0; $i < count($det_prov); $i++): ?>
		  <tr>
				<td class='text-center'>
				<a href="eliminardetalle?Id=<?php echo e($det_prov[$i]->id_requisitoart); ?>&IdPT=<?php echo e($id_pt); ?>" onclick="">
				<i class="fa fa-trash" title="Eliminar requisito"></i></a>
				</td>		  
		  <td><?php echo e($MP[$det_prov[$i]->id_mp-1]->cod); ?></td>
		  <td align="right"><?php echo e($det_prov[$i]->cantidad); ?>

				<a href="#" data-toggle="modal" data-target="#editar<?php echo e($det_prov[$i]->id_requisitoart); ?>">
				<i class="fa fa-edit" title="Editar Cantidad"></i></a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="editar<?php echo e($det_prov[$i]->id_requisitoart); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<form action="editardetalle" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">     	 
							  <div class="col-md-12">
							  <div class="card">
								<div class="card-body">
								  <div class="row">
									<div class="col-md-12">
									<label>Cantidad </label>
									<input type="text" id="Cantidad" class="form-control date" name="Cantidad" maxlength="250" value="<?php echo e($det_prov[$i]->cantidad); ?>">
									<input type="hidden" id="Id" class="form-control date" name="Id" value="<?php echo e($det_prov[$i]->id_requisitoart); ?>">
									<input type="hidden" id="IdPT" class="form-control date" name="IdPT" value="<?php echo e($id_pt); ?>">
									</div>
								  </div>
								</div>
							  </div>
							  </div>
							<div class="col-md-12">
							  <center><input type="submit" class="btn btn-success" value="Actualizar Cantidad"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  

						<!-- Modal -->				
			</td>		  
		  <td><?php echo e($det_prov[$i]->created_at); ?></td>
		  </tr>
          <?php endfor; ?>
          </tbody>
        </table>     
      </div>	
		<center><a href="listar" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>	  

	
	
  </div>
</div>
    <?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
