<?php echo $__env->make('layouts.dash.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- Jquery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Datepicker Files -->
    <script src="<?php echo e(asset('datePicker/js/bootstrap-datepicker.js')); ?>"></script>
    <!-- Languaje -->
    <script src="<?php echo e(asset('datePicker/locales/bootstrap-datepicker.es.min.js')); ?>"></script>
<?php echo $__env->make('layouts.dash.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
 
$total_1N= 0; 
$total_2N=0; 
$total_3N= 0;

?>
<?php $permisos = Session::get('permisos')   ?>
<?php if($permisos[16] == '1'): ?>  
    <div class="col-md-12">
      <div class="card">
       <div class="card-header" style="overflow-x:auto;">
		<table class="table" width="100%" cellspacing="0" >
		<thead>
			<tr>
            <th ><p style="font-size:50px">Nómina Quincenal</th>
            <th align="center" valign="middle">
			<a href="<?php echo e(url('Finanza/listar')); ?>" class="btn btn-success" title="Ver nómina mensual" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Mensual</a>
			</th>
            <th align="center" valign="middle">
<?php if($permisos[17] == '1'): ?>  			
			<a target="_blank" href="#" data-toggle="modal" data-target="#desprendibles"  class="btn btn-danger" title="Ver Desprendibles"><span class="fa fa-eye"></span> <span class="fa fa-exchange"> Desprendibles</a>
						<!-- ModalBODY -->
							<div class="modal fade" id="desprendibles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<h4 class="modal-title" id="myModalLabel">Generar Desprendibles de Nómina</h4>
								  </div>
							  <form target="_blank" action="<?php echo e(url('Finanza/pdf_todos')); ?>" method="POST">		  
								<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"> 
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									<label>Desde </label>
									<input type="text"  id="FechaInicio" class="form-control datepicker" name="FechaInicio" value="<?php echo date('Y-m-d'); ?>">
									</div>
									<div class="col-md-6">
									<label>Hasta</label>	
									<input type="text"  id="FechaFinal" class="form-control datepicker" name="FechaFinal" value="<?php echo date('Y-m-d'); ?>">
									</div>
								  </div>

								</div>								  	
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
									<button type="submit" class="btn btn-primary">Ver Desprendibles</button> 
								  </div>
							</form>		  
								</div>
							  </div>
							</div>			
						<!-- Modal -->		
<?php endif; ?>						
			</th>			
			
				</tr>
			</thead>	
			</table>	
				</div>
		<?php echo $__env->make('alerts.validacion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
		<thead>
            <tr>
			  <th>Desprendibles</th>
              <th>NOMBRE</th>
			  <th>Cargos</th>
              <th align="center">Salario</th>
			  <th align="center">Contrato</th>
			  <th align="center"><?php echo e(16-$info[0]->pensioon); ?>% Pensión $</th> <!-- SEGURIDAD SOCIAL         -->
			  <th align="center"><?php echo e(12.5 -$info[0]->salud); ?>% Salud $</th> <!-- SEGURIDAD SOCIAL         -->
			  <th align="center">Auxilio de Transporte $</th>
			  <th align="center">Comisiones $</th>
			  <th align="center">Otros Pagos $</th>
			  <th align="center">Descontar Seguridad Social Quincenal</th>			  
			  <th align="center">Total Pago</th>
            </tr>
          </thead>
          <tbody>
          <?php $__currentLoopData = $usuarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php if($usuario->user_estado == "Activo" && $usuario->nombre != "admin" && $usuario->rol != "Cliente"): ?>
            <tr>
			<td>
<?php if($permisos[17] == '1'): ?> 			
			<center>
						<!-- Modal -->			
			<a type="button" class="btn-sm btn-info " data-toggle="modal" data-target="#liquidar<?php echo e($usuario->id_personal); ?>" title="Liquidar Días <?php echo e($usuario->nombre); ?>" data-toggle="tooltip" data-placement="left">Liquidar</a>
						<!-- ModalBODY -->
							<div class="modal fade" id="liquidar<?php echo e($usuario->id_personal); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<h4 class="modal-title" id="myModalLabel">Desprendible de Nómina</h4>
								  </div>
							  <form target="_blank" action="<?php echo e(url('Finanza/pdf')); ?>" method="POST">		  
								<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>"> 
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									<label>Desde </label>
									<input type="hidden" class="form-control date"  value="<?php echo e($usuario->id_personal); ?>" name="Id">           									
									<input type="text"  id="FechaInicio" class="form-control datepicker" name="FechaInicio" value="<?php echo date('Y-m-d'); ?>">
									<label>Nombre: </label>									
									<input readonly type="text" readonly id="venci" class="form-control date" name="Nombre" value="<?php echo e(ucwords($usuario->nombre)); ?>" >
									<label>Sueldo: </label>									
									<input readonly type="text" style="text-align:right;" readonly id="venci" class="form-control date" name="Sueldo" value="<?php echo e($usuario->valor_a_pagar); ?>" >									
									</div>
									<div class="col-md-6">
									<label>Hasta</label>	
									<input type="text"  id="FechaFinal" class="form-control datepicker" name="FechaFinal" value="<?php echo date('Y-m-d'); ?>">
									<label>Cargo: </label>									
									<input readonly type="text" readonly id="venci" class="form-control date" name="Cargo" value="<?php echo e(ucwords($usuario->rol)); ?>" >
									<label>Contrato: </label>
									<?php if($usuario->contrato == 0): ?>
									<input readonly type="text" readonly id="venci" class="form-control date" name="Contrato" value="Prestación de Servicios" >
									<?php else: ?>
									<input readonly type="text" readonly id="venci" class="form-control date" name="Contrato" value="Labor Contratada" >
									<?php endif; ?>									
									</div>
								  </div>
								<div class="row">
								<div class="row">
								<div class="col-md-6">
								<label>Deducción Pensión $ </label>
								</div>
								<div class="col-md-6">									
									<input readonly type="text" style="text-align:right;" readonly id="venci" class="form-control date" name="Pensión" value="<?php echo e(round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*(0.16-$info[0]->pensioon/100))); ?>" >
								</div>
								</div>
								<div class="row">
								<div class="col-md-6">
									<label>Deducción Salud $ </label>
								</div>
								<div class="col-md-6">
									<input readonly type="text" style="text-align:right;" readonly id="venci" class="form-control date" name="Salud" value="<?php echo e(round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*(0.125-$info[0]->salud/100))); ?>" >
								</div>									
								</div>
								<div class="row">
								<div class="col-md-6">								
									<label>Auxilio de Transporte $ </label>	
								</div>
								<div class="col-md-6">		
								<?php if($usuario->contrato == 0): ?>
									<input readonly type="text" style="text-align:right;" readonly id="venci" class="form-control date" name="Auxilio" value="0" >
								<?php else: ?>
									<input readonly type="text" style="text-align:right;" readonly id="venci" class="form-control date" name="Auxilio" value="<?php echo e($info[0]->auxilio); ?>" >
								<?php endif; ?>
								</div>
								</div>
								<div class="row">
								<div class="col-md-6">								
									<label>Comisiones $ </label>	
								</div>
								<div class="col-md-6">									
									<input readonly type="text" style="text-align:right;" readonly id="venci" class="form-control date" name="Comisiones" value="<?php echo e($usuario->comisiones); ?>" >
								</div>
								<div class="row">
								<div class="col-md-6">								
									<label>Otros Pagos, extras $ </label>	
								</div>
								<div class="col-md-6">									
									<input readonly type="text" style="text-align:right;" readonly id="venci" class="form-control date" name="Otros Pagos" value="<?php echo e($usuario->otros); ?>" >
								</div>
								</div>								
								</div>								
								</div>
								</div>								  	
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
									<button type="submit" class="btn btn-primary">Ver Desprendible</button> 
								  </div>
							</form>		  
								</div>
							  </div>
							</div>			
						<!-- Modal <?php echo e(url('Factura/ver?id='.$usuario->id_facturas)); ?> -->
			</center>
<?php endif; ?>			
			</td>
              <td>
			  <a href="mailto:
		<?php echo e(($usuario->email)); ?>

		?subject=
		Tirilla%20Nómina
		&body=
		<?php echo e(ucwords($usuario->nombre)); ?>, la siguiente es la tirrilla de nómina
		">
			  <?php echo e(ucwords($usuario->nombre)); ?>

				</a> <?php $total_1= 0; $total_2=0; $total_3= 0;?>
			  </td>
			  <td><?php echo e(ucwords($usuario->rol)); ?></td>
              <td align="right"><span class="label label-primary">$ <?php echo e(number_format($usuario->valor_a_pagar/2,0)); ?></span> <?php $total_3+= $usuario->valor_a_pagar/2;$total_3N+=$total_3;?> </td>
			  <td>
			  <?php if($usuario->contrato == 0): ?>
			  <span class="label label-success">Servicios</span>
			  <?php else: ?>
			  <span class="label label-primary">Laboral</span>
			  <?php endif; ?>			  
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format(round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*(0.16-$info[0]->pensioon/100)/2),0)); ?> <?php $total_1-= round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*(0.16-$info[0]->pensioon/100)/2);?> 
			  <?php else: ?>
			  0 <?php $total_1+= 0;?>
			  <?php endif; ?>
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format(round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*(0.125-$info[0]->salud/100)/2),0)); ?> <?php $total_1-= round(($usuario->valor_a_pagar+$usuario->comisiones+$usuario->otros)*(0.125-$info[0]->salud/100)/2);?> 
			  <?php else: ?>
			  0 <?php $total_1+= 0;?>
			  <?php endif; ?>
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
				  <?php if($usuario->valor_a_pagar <= $usuario->valor_a_pagar*2): ?>
				  <?php echo e(number_format($info[0]->auxilio/2,0)); ?> <?php $total_3+= $info[0]->auxilio/2;?>
				  <?php else: ?>
				  0
				  <?php endif; ?>
			  <?php else: ?>
			  0 <?php $total_2+= 0;?>
			  <?php endif; ?>				  
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format($usuario->comisiones,0)); ?> <?php $total_3+= $usuario->comisiones?> 
			  <?php else: ?>
			  0 <?php $total_3+= 0;?>
			  <?php endif; ?>			  
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <?php echo e(number_format($usuario->otros,0)); ?> <?php $total_3+= $usuario->otros?> 
			  <?php else: ?>
			  0 <?php $total_3+= 0;?>
			  <?php endif; ?>
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <span class="label label-primary"><?php echo "$ ".number_format($total_1,0);$total_1N+=$total_1;?></span><?php $total_3+= $total_1;?> 
			  <?php else: ?>
			  0 <?php $total_3+= 0;$total_1N+= 0;?>
			  <?php endif; ?>			  
			  </td>
			  <td align="right">
			  <?php if($usuario->contrato == 1): ?>
			  <span class="label label-info"><?php echo "$ ".number_format($total_3,0);?></span>
			  <?php else: ?>
			  0 
			  <?php endif; ?>			  
			  </td>
 
            </tr>		
		
		<?php endif; ?>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
		</div> 
		
			
		
      </div>
    </div>
	
    <div class="col-md-12">
      <div class="card">
		
       <div class="card-header" style="overflow-x:auto;">
		<table class="table" width="100%" cellspacing="0" >
		<thead>
			<tr>
            <th ><p style="font-size:50px"> Reporte de Nómina Quincenal <?php echo $meses[date('n')-1] ?>:  </th>
				</tr>
			</thead>	
			</table>	
				</div>		
		
        <div class="card-body">
          <div class="row">
            <div class="col-md-12" align="right">
              <label>Total Salarios Quincenales </label>
              <input type="text" style="text-align:right;" readonly class="form-control" name="SalarioMensual" value="$ <?php echo e(number_format($total_3N,0)); ?>" maxlength="250">
              <label>Total Seguridad Social Quincenal a Descontar</label>
              <input type="text" style="text-align:right;" name="Seguridad" readonly class="form-control" value="$ <?php echo e(number_format($total_1N*-1,0)); ?>" maxlength="250">			  
			  </div>
          </div>
        </div>
      </div>
    </div>			
	
  </div>
 
 <script>
     $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        language: "es",
        autoclose: true
    });
 </script>
 
 
<?php endif; ?>
<?php echo $__env->make('layouts.dash.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
