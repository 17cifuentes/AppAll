-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 15-11-2017 a las 10:27:12
-- Versión del servidor: 10.0.32-MariaDB-cll-lve
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sagazsa1_appall`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `artiiculos`
--

CREATE TABLE `artiiculos` (
  `id` int(11) NOT NULL,
  `cod` varchar(250) DEFAULT NULL,
  `detalle` varchar(250) DEFAULT NULL,
  `precio_unitario` int(11) DEFAULT NULL,
  `impuesto` tinyint(2) DEFAULT NULL,
  `medida` varchar(20) DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `tel` char(30) DEFAULT NULL,
  `correo` varchar(64) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL,
  `valida` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_art`
--

CREATE TABLE `detalle_art` (
  `id_detalleart` int(11) NOT NULL,
  `id_artiiculos` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `detalle_art`
--
DELIMITER $$
CREATE TRIGGER `disponible_artiiculo` AFTER INSERT ON `detalle_art` FOR EACH ROW UPDATE 
inventario_artiiculos
SET 
inventario_artiiculos.disponible = NEW.cantidad + inventario_artiiculos.disponible
where 
inventario_artiiculos.id_artiiculos = NEW.id_artiiculos
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_factura`
--

CREATE TABLE `detalle_factura` (
  `id_detalle` int(11) NOT NULL,
  `num_factura` int(11) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_venta` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_mp`
--

CREATE TABLE `detalle_mp` (
  `id_detallemp` int(11) NOT NULL,
  `id_mp` int(11) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `tipo` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `detalle_mp`
--
DELIMITER $$
CREATE TRIGGER `disponible_mpP` AFTER INSERT ON `detalle_mp` FOR EACH ROW UPDATE 
inventario_mp
SET 
inventario_mp.disponible = IF(NEW.tipo = 1, inventario_mp.disponible + NEW.cantidad, inventario_mp.disponible - NEW.cantidad)
where 
inventario_mp.id_mp = NEW.id_mp
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pedidos`
--

CREATE TABLE `detalle_pedidos` (
  `id_detallepedido` int(11) NOT NULL,
  `num_pedido` int(11) DEFAULT NULL,
  `id_artiiculos` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_venta` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id` int(1) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `tel` char(30) DEFAULT NULL,
  `correo` varchar(64) DEFAULT NULL,
  `nit` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `paiis` varchar(255) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `cod_postal` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `nombre`, `tel`, `correo`, `nit`, `address`, `ciudad`, `paiis`, `pic`, `cod_postal`, `created_at`, `updated_at`) VALUES
(1, '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `id_facturas` int(11) NOT NULL,
  `num_factura` int(11) DEFAULT NULL,
  `id_clientes` int(11) DEFAULT NULL,
  `id_vendedor` int(11) DEFAULT NULL,
  `tipo_pago` tinyint(2) DEFAULT NULL,
  `total_venta` double DEFAULT NULL,
  `estado_factura` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `id_gastos` int(11) NOT NULL,
  `id_personal` int(11) DEFAULT NULL,
  `concepto` varchar(250) DEFAULT NULL,
  `comprobante` varchar(250) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_artiiculos`
--

CREATE TABLE `inventario_artiiculos` (
  `id_inventario_art` int(11) NOT NULL,
  `id_artiiculos` int(11) DEFAULT NULL,
  `disponible` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_mp`
--

CREATE TABLE `inventario_mp` (
  `id_inventario_mp` int(11) NOT NULL,
  `id_mp` int(11) DEFAULT NULL,
  `disponible` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia_prima`
--

CREATE TABLE `materia_prima` (
  `id` int(11) NOT NULL,
  `cod` varchar(250) DEFAULT NULL,
  `detalle` varchar(250) DEFAULT NULL,
  `medida` varchar(250) DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id_pedidos` int(11) NOT NULL,
  `num_pedido` int(11) DEFAULT NULL,
  `id_vendedor` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `tipo_pago` tinyint(2) DEFAULT NULL,
  `total_venta` int(11) DEFAULT NULL,
  `estado_factura` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `id_personal` int(11) NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `tel` varchar(250) DEFAULT NULL,
  `correo` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `cargo` varchar(250) DEFAULT NULL,
  `valor_a_pagar` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`id_personal`, `nombre`, `tel`, `correo`, `address`, `cargo`, `valor_a_pagar`, `created_at`, `updated_at`) VALUES
(1, 'admin', '', 'admin@admin', '', 'Administrador', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Cris', '12345', '17cifuentes@gmail.com', '$2y$10$MlQn1T5K9VcwdaaLAFCTA.FRvkGVRABYRTVjRrN6p1O.p7kNsm3pm', 'Administrador', 700, '2017-10-25 02:18:34', '2017-10-25 02:18:34'),
(3, 'jeff', '23456', 'jeff@gmail.com', '$2y$10$Wx2cuahqY1gPSgYeu.2zhudd/m36BorYde3bvOCY6M7WD3xhg0YNe', 'Almacenista', 900, '2017-11-02 01:48:42', '2017-11-02 01:48:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id` int(11) NOT NULL,
  `empresa` varchar(250) DEFAULT NULL,
  `tel` varchar(250) DEFAULT NULL,
  `correo` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL,
  `nit` varchar(250) DEFAULT NULL,
  `camara` varchar(250) DEFAULT NULL,
  `rut` varchar(250) DEFAULT NULL,
  `certificado` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `requisito_art`
--

CREATE TABLE `requisito_art` (
  `id_requisitoart` int(11) NOT NULL,
  `id_artiiculos` int(11) DEFAULT NULL,
  `id_mp` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `rol` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `id_personal` int(11) DEFAULT NULL,
  `user_estado` varchar(200) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `rol`, `email`, `password`, `id_personal`, `user_estado`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrador', 'admin@admin', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', '4KkB0JfJbS0tuXuvAxTdm8D4FiVnKpjD9cOUkJRHD8b2u25i8Pp9HqH0ZuSp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Cris', 'Administrador', '17cifuentes@gmail.com', '$2y$10$MlQn1T5K9VcwdaaLAFCTA.FRvkGVRABYRTVjRrN6p1O.p7kNsm3pm', 2, 'Activo', '9NrU6PhhnNFfjgRgeh23i820GFyqWYQRdOYEEJKE6yYNSRP1ysGpwd9lGIWm', '2017-10-25 07:18:34', '2017-10-25 07:18:34'),
(3, 'jeff', 'Almacenista', 'jeff@gmail.com', '$2y$10$Wx2cuahqY1gPSgYeu.2zhudd/m36BorYde3bvOCY6M7WD3xhg0YNe', 3, 'Activo', '1xHlShtjvUc1wOmWI0xZkytlahUbOul1PaitPHEOZDM6DsAWoGA14LhcBJJ5', '2017-11-02 06:48:42', '2017-11-02 06:48:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendedor`
--

CREATE TABLE `vendedor` (
  `id` int(11) NOT NULL,
  `id_personal` int(11) DEFAULT NULL,
  `cuota` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `artiiculos`
--
ALTER TABLE `artiiculos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cod` (`cod`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indices de la tabla `detalle_art`
--
ALTER TABLE `detalle_art`
  ADD PRIMARY KEY (`id_detalleart`),
  ADD KEY `fk_detart_artiiculos` (`id_artiiculos`);

--
-- Indices de la tabla `detalle_factura`
--
ALTER TABLE `detalle_factura`
  ADD PRIMARY KEY (`id_detalle`),
  ADD KEY `fk_detfactura_factura` (`num_factura`);

--
-- Indices de la tabla `detalle_mp`
--
ALTER TABLE `detalle_mp`
  ADD PRIMARY KEY (`id_detallemp`),
  ADD KEY `fk_detmp_proveedores` (`id_proveedor`),
  ADD KEY `fk_detmp_materiaprima` (`id_mp`);

--
-- Indices de la tabla `detalle_pedidos`
--
ALTER TABLE `detalle_pedidos`
  ADD PRIMARY KEY (`id_detallepedido`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`id_facturas`),
  ADD UNIQUE KEY `num_factura` (`num_factura`),
  ADD KEY `fk_facturas_clientes` (`id_clientes`),
  ADD KEY `fk_facturas_vendedor` (`id_vendedor`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`id_gastos`);

--
-- Indices de la tabla `inventario_artiiculos`
--
ALTER TABLE `inventario_artiiculos`
  ADD PRIMARY KEY (`id_inventario_art`),
  ADD KEY `fk_invartt_art` (`id_artiiculos`);

--
-- Indices de la tabla `inventario_mp`
--
ALTER TABLE `inventario_mp`
  ADD PRIMARY KEY (`id_inventario_mp`),
  ADD KEY `fk_invmp_mp` (`id_mp`);

--
-- Indices de la tabla `materia_prima`
--
ALTER TABLE `materia_prima`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cod` (`cod`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id_pedidos`),
  ADD UNIQUE KEY `num_pedido` (`num_pedido`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`id_personal`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `correo` (`correo`),
  ADD UNIQUE KEY `nit` (`nit`);

--
-- Indices de la tabla `requisito_art`
--
ALTER TABLE `requisito_art`
  ADD PRIMARY KEY (`id_requisitoart`),
  ADD KEY `fk_reqart_artiiculos` (`id_artiiculos`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `fk_usuario_personal` (`id_personal`);

--
-- Indices de la tabla `vendedor`
--
ALTER TABLE `vendedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vendedor_personal` (`id_personal`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `artiiculos`
--
ALTER TABLE `artiiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalle_art`
--
ALTER TABLE `detalle_art`
  MODIFY `id_detalleart` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalle_factura`
--
ALTER TABLE `detalle_factura`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalle_mp`
--
ALTER TABLE `detalle_mp`
  MODIFY `id_detallemp` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalle_pedidos`
--
ALTER TABLE `detalle_pedidos`
  MODIFY `id_detallepedido` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `id_facturas` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `id_gastos` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `inventario_artiiculos`
--
ALTER TABLE `inventario_artiiculos`
  MODIFY `id_inventario_art` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `inventario_mp`
--
ALTER TABLE `inventario_mp`
  MODIFY `id_inventario_mp` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `materia_prima`
--
ALTER TABLE `materia_prima`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id_pedidos` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `personal`
--
ALTER TABLE `personal`
  MODIFY `id_personal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `requisito_art`
--
ALTER TABLE `requisito_art`
  MODIFY `id_requisitoart` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `vendedor`
--
ALTER TABLE `vendedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle_art`
--
ALTER TABLE `detalle_art`
  ADD CONSTRAINT `fk_detart_artiiculos` FOREIGN KEY (`id_artiiculos`) REFERENCES `artiiculos` (`id`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `detalle_factura`
--
ALTER TABLE `detalle_factura`
  ADD CONSTRAINT `fk_detfactura_factura` FOREIGN KEY (`num_factura`) REFERENCES `facturas` (`num_factura`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `detalle_mp`
--
ALTER TABLE `detalle_mp`
  ADD CONSTRAINT `fk_detmp_materiaprima` FOREIGN KEY (`id_mp`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `fk_detmp_proveedores` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD CONSTRAINT `fk_facturas_clientes` FOREIGN KEY (`id_clientes`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `fk_facturas_vendedor` FOREIGN KEY (`id_vendedor`) REFERENCES `vendedor` (`id`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `inventario_artiiculos`
--
ALTER TABLE `inventario_artiiculos`
  ADD CONSTRAINT `fk_invartt_art` FOREIGN KEY (`id_artiiculos`) REFERENCES `artiiculos` (`id`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `inventario_mp`
--
ALTER TABLE `inventario_mp`
  ADD CONSTRAINT `fk_invmp_mp` FOREIGN KEY (`id_mp`) REFERENCES `materia_prima` (`id`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `requisito_art`
--
ALTER TABLE `requisito_art`
  ADD CONSTRAINT `fk_reqart_artiiculos` FOREIGN KEY (`id_artiiculos`) REFERENCES `artiiculos` (`id`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_usuario_personal` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`id_personal`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `vendedor`
--
ALTER TABLE `vendedor`
  ADD CONSTRAINT `fk_vendedor_personal` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`id_personal`) ON DELETE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
