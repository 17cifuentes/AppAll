﻿

cd /D C:\xampp\mysql\bin
mysql -u root

Use appweb

set names utf8;

INSERT INTO empresa
(
id,
nombre,
tel,
correo,
nit,
address,
ciudad,
paiis,
pic,
cod_postal,
lema1 ,
lema2 ,
factura,
pensioon,
salud,
cesantiia,
intereses,
prima,
vacaciones,
salario,
auxilio,
created_at,
updated_at
) VALUES
(1, 'PEGA VALLE', '3185605914', 'info@pegavalle.com', '1112106537-7', 'Callejón el diamante obreros de Cristo Aguaclara', 'Tuluá valle del cauca ', 'Colombia', 'images/empresa/LOGO.jpg', '763021', 'COMERCIALIZADORA DE PEGANTES DEL VALLE', 'Resolución DIAN No 50000397902 de 25/08/2015 del N° SAG 6650 hasta 10000 a papel y GAZ 5002 hasta el 10000 a computador. Esta factura se asimila para todos sus efectos legales a la letra de cambio Art. 774 del codigo 08679 del 17/10/2013.', 0, 12, 8.5, 8.33, 1, 8.33, 4.17, 781242, 88211,'0000-00-00 00:00:00', '0000-00-00 00:00:00');

INSERT INTO personal
(
id_personal,
nombre,
tel,
correo,
address,
cargo,
valor_a_pagar,
created_at,
updated_at
) VALUES
(1, 'admin', '', 'admin@admin', '', 'Administrador', 0, 'Always', 'Always'),
(2, 'customer', '12345', 'customer@customer', '$2y$10$MlQn1T5K9VcwdaaLAFCTA.FRvkGVRABYRTVjRrN6p1O.p7kNsm3pm', 'Administrador', 0, 'Always', 'Always'),
(3, 'jeff', '23456', 'jeff@gmail.com', '$2y$10$Wx2cuahqY1gPSgYeu.2zhudd/m36BorYde3bvOCY6M7WD3xhg0YNe', 'Administrador', 0, '2017-11-01 20:48:42', '2017-11-01 20:48:42'),
(4, 'Felipe Dávila', '3185605914', '23pipe9716@pegavalle.com', 'Cra 24a # 3 - 04', 'Administrador', 1500000, '2017-10-24 21:18:34', '2017-10-24 21:18:34'),
(5, 'Luzmar   Montoya', '318 579 28 67 ', 'lmmontoya@pegavalle.com', 'Cra 24a # 3 - 04', 'Administrador', 1000000, '2017-10-24 21:18:34', '2017-10-24 21:18:34'),
(6, 'Luis Carlos', '123456', 'luis@gmail.com', '', 'Vendedor', 700000, '2017-11-29 13:16:32', '2017-11-29 13:16:32'),
(7, 'Álvaro José', '3122328616', 'alvaro@pegavalle.com', 'Aguaclara', 'Operario', 800000, '2017-12-02 01:37:17', '2017-12-02 01:37:17'),
(8, 'Luis Fernando', '3164878137', 'Luisfernando@pegavalle.com', 'Crr22#3-02', 'Vendedor', 1000000, '2017-12-02 01:42:52', '2017-12-02 01:42:52'),
(9, 'Ramíro Dávila', '3185233526', 'muebles_napoles@pegavalle.com', 'Crr24#3-04', 'Administrador', 1500000, '2017-12-02 01:47:17', '2017-12-02 01:47:17'),
(10, 'Ferney de Jesús ', '', 'Ferney@pegavalle.com', 'Crr32#24-07', 'Vendedor', 1000000, '2017-12-02 01:49:28', '2017-12-02 01:49:28');

INSERT INTO users
(
id,
name,
rol,
email,
password,
id_personal,
user_estado,
pic,
cuota,
remember_token,
created_at,
updated_at
) VALUES
(1, 'admin', 'Administrador', 'admin@admin', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/empresa/LOGO.jpg', 0, NULL, 'Always', 'Always'),
(2, 'customer', 'Administrador', 'customer@customer', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Inactivo', 'images/defaultPersonal.png', 0, NULL, 'Always', 'Always'),
(3, 'jeff', 'Administrador', 'jeff@gmail.com', '$2y$10$Wx2cuahqY1gPSgYeu.2zhudd/m36BorYde3bvOCY6M7WD3xhg0YNe', 3, 'Inactivo', 'images/defaultPersonal.png', 0, NULL, '2017-11-02 01:48:42', '2017-11-02 01:48:42'),
(4, 'Felipe Dávila', 'Administrador', '23pipe9716@pegavalle.com', '$2y$10$Wx2cuahqY1gPSgYeu.2zhudd/m36BorYde3bvOCY6M7WD3xhg0YNe', 4, 'Activo', 'images/defaultPersonal.png', 0, NULL, '2017-11-02 01:48:42', '2017-11-02 01:48:42'),
(5, 'Luzmar   Montoya', 'Administrador', 'lmmontoya@pegavalle.com', '$2y$10$Wx2cuahqY1gPSgYeu.2zhudd/m36BorYde3bvOCY6M7WD3xhg0YNe', 5, 'Activo', 'images/defaultPersonal.png', 0, NULL, '2017-11-02 01:48:42', '2017-11-02 01:48:42'),
(6, 'Luis Carlos', 'Vendedor', 'luis@gmail.com', '$2y$10$Wx2cuahqY1gPSgYeu.2zhudd/m36BorYde3bvOCY6M7WD3xhg0YNe', 6, 'Inactivo', 'images/defaultPersonal.png', 0, NULL, '2017-11-29 13:16:32', '2017-12-01 22:46:00'),
(7, 'Álvaro José', 'Operario', 'alvaro@pegavalle.com', '$2y$10$Wx2cuahqY1gPSgYeu.2zhudd/m36BorYde3bvOCY6M7WD3xhg0YNe', 7, 'Activo', 'images/defaultPersonal.png', 0, NULL, '2017-12-02 01:37:17', '2017-12-02 01:37:17'),
(8, 'Luis Fernando', 'Vendedor', 'Luisfernando@pegavalle.com', '$2y$10$Wx2cuahqY1gPSgYeu.2zhudd/m36BorYde3bvOCY6M7WD3xhg0YNe', 8, 'Activo', 'images/defaultPersonal.png', 0, NULL, '2017-12-02 01:42:52', '2017-12-02 01:42:52'),
(9, 'Ramiro Dávila', 'Administrador', 'muebles_napoles@pegavalle.com', '$2y$10$Wx2cuahqY1gPSgYeu.2zhudd/m36BorYde3bvOCY6M7WD3xhg0YNe', 9, 'Activo', 'images/defaultPersonal.png', 0, NULL, '2017-12-02 01:47:17', '2017-12-02 01:47:17'),
(10, 'Ferney de Jesús ', 'Vendedor', 'Ferney@pegavalle.com', '$2y$10$Wx2cuahqY1gPSgYeu.2zhudd/m36BorYde3bvOCY6M7WD3xhg0YNe', 10, 'Activo', 'images/defaultPersonal.png', 0, NULL, '2017-12-02 01:49:28', '2017-12-02 01:49:28'),
(11, 'Ferre paisa la marina', 'Cliente', 'ferrepaisa@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 1, NULL, 'Always', 'Always'),
(12, 'Ferreteria Electricos RyR', 'Cliente', 'FerreteriaElectricosRyR@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 2, NULL, 'Always', 'Always'),
(13, 'Ferreteria Mundo Corona   Liliana', 'Cliente', 'sincorreo1-@gemail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 3, NULL, 'Always', 'Always'),
(14, 'Ferreteria Ferre Equipos del Pacífico Druman', 'Cliente', 'sincorreo2@hotmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 4, NULL, 'Always', 'Always'),
(15, 'Ferreteria Jazmin Diego', 'Cliente', 'FerreJazminDiego@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 5, NULL, 'Always', 'Always'),
(16, 'Ferreteria Estiben', 'Cliente', 'FerreteriaEstiben@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 6, NULL, 'Always', 'Always'),
(17, 'ferre Doble AA Alberto Londoño', 'Cliente', 'ferreDobleAA@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 7, NULL, 'Always', 'Always'),
(18, 'Ferreteria Materiales la Gta gloria', 'Cliente', 'FerreteriaMaterialeslGtagloria@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 8, NULL, 'Always', 'Always'),
(19, 'Constru Pasifico Mariela Quiseno', 'Cliente', 'ConstruPasificoMarielaQuiseno@gamail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 9, NULL, 'Always', 'Always'),
(20, 'Ferreteria Super Rebaja Rubiel Gallego', 'Cliente', 'FerreteriaSuperRebajaRubielGallego@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 10, NULL, 'Always', 'Always'),
(21, 'Ferreteria Choco Jose Murrillo', 'Cliente', 'FerreteriaChocoJoseMurrillo@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 11, NULL, 'Always', 'Always'),
(22, 'Ferreteria Las Piedras Nelson Perez', 'Cliente', 'FerreteriaLasPiedrasNelsonPerez@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 12, NULL, 'Always', 'Always'),
(23, 'Ferreteria Vinasco Liliana', 'Cliente', 'FerreteriaVinascoLiliana@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 13, NULL, 'Always', 'Always'),
(24, 'Ferreteria Ferro Libertad Silvia', 'Cliente', 'FerreteriaFerroLibertadSilvia@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 14, NULL, 'Always', 'Always'),
(25, 'Ferreteria Mi Buenaventura Jawes', 'Cliente', 'FerreteriaMiBuenaventuraJawes@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 15, NULL, 'Always', 'Always'),
(26, 'Ferreteria Las Americas Ruben Dario', 'Cliente', 'FerreteriaLasAmericasRubenDario@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 16, NULL, 'Always', 'Always'),
(27, 'Ferreteria Nueva Granada Jorge', 'Cliente', 'FerreteriaNuevaGranadaJorge@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 17, NULL, 'Always', 'Always'),
(28, 'Ferreteria Construmar Beatriz Adriana', 'Cliente', 'FerreteriaConstrumarBeatrizAdriana@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 2, 'Activo', 'images/defaultCustomer.png', 18, NULL, 'Always', 'Always'),
(29, 'Ferreteria Hierros Londoño Katerine', 'Cliente', 'FerreteriaHierrosLondonoKaterine@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 19, NULL, 'Always', 'Always'),
(30, 'Ferreteria Los Construtores Hugo', 'Cliente', 'FerreteriaLosConstrutoresHugo@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 20, NULL, 'Always', 'Always'),
(31, 'Ferreteria Torre Mar Amparo', 'Cliente', 'FerreteriaTorreMarAmparo@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 21, NULL, 'Always', 'Always'),
(32, 'Ferreteria El Carmen Yesenia', 'Cliente', 'FerreteriaElCarmenYesica@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 22, NULL, 'Always', 'Always'),
(33, 'Ferreteria La Torre Robinson', 'Cliente', 'FerreteriaLaTorreRobinson@gamail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 23, NULL, 'Always', 'Always'),
(34, 'Ferreteria Horizonte Jose Abel', 'Cliente', 'FerreteriaHorizonteJoseAbel@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 24, NULL, 'Always', 'Always'),
(35, 'Ferreteria Costa Sra Margarita', 'Cliente', 'FerreteriaCostaSraMargarita@gamail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 25, NULL, 'Always', 'Always'),
(36, 'Ferreteria Panel Y Super Board Matato', 'Cliente', 'FerreteriaPanelYSuperBoardMatato@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 26, NULL, 'Always', 'Always'),
(37, 'Ferreteria Eléctricos Banco Juan Gabriel', 'Cliente', 'FerreteriaElectricosBancoJuanGabriel@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 27, NULL, 'Always', 'Always'),
(38, 'Ferreteria Shary Sro Fernando', 'Cliente', 'FerreteriaSharySroFernando@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 28, NULL, 'Always', 'Always'),
(39, 'Eléctrico La Otra Esquina Sro Carlos', 'Cliente', 'ElectricoLaOtraEsquinaSroCarlos@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 29, NULL, 'Always', 'Always'),
(40, 'Ferreteria Mariana Sro Alejandro', 'Cliente', 'FerreteriaMarianaSroAlejandro@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 30, NULL, 'Always', 'Always'),
(41, 'Ferreteria Materiales Gonzalez Sro Sebastian', 'Cliente', 'FerreteriaMaterialesGonzalezSroSebastian@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 31, NULL, 'Always', 'Always'),
(42, 'Ferreteria El Punto Clave Del Jorge Sro Victor', 'Cliente', 'FerreteriaElPuntoClaveDelJorgeSroVictor@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 32, NULL, 'Always', 'Always'),
(43, 'Ferreteria Naya Brinlly', 'Cliente', 'FerreteriaNayaBrinlly@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 33, NULL, 'Always', 'Always'),
(44, 'Ferreteria Maderas La 41 Octavio Murrillo', 'Cliente', 'FerreteriaMadera@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 34, NULL, 'Always', 'Always'),
(45, 'Ferreteria Ferragro Sro Diego', 'Cliente', 'FerreteriaFerragroSroDiego@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 35, NULL, 'Always', 'Always'),
(46, 'Ferreteria Economía Del Pacifico William', 'Cliente', 'FerreteriaEconomia@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 36, NULL, 'Always', 'Always'),
(47, 'Maria Eugenia Davila', 'Cliente', 'MariaEugeniaDavila@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 37, NULL, 'Always', 'Always'),
(48, 'Ferreteria Única Sro Guillermo', 'Cliente', 'FerreteriaUnicaSroGuillerm@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 38, NULL, 'Always', 'Always'),
(49, 'Ferreteria El Cruzero Sro Arley', 'Cliente', 'FerreteriaElCruzeroSroArley@gmail.com', '$2y$10$6VSFsljHHeIlSjgLfUo3JelEjYknVrWqjtGuUMoyvbwll.74dVOqa', 1, 'Activo', 'images/defaultCustomer.png', 39, NULL, 'Always', 'Always');



INSERT INTO clientes (id, nombre_clientes, tel, correo_cliente, address, password, estado, vendedor, valida, created_at, updated_at) VALUES
(1, 'Ferre paisa la marina', '0322242424', 'ferrepaisa@gmail.com', '', '$2y$10$evJgxA45ZWazP4d8tEqozeQ9HNI8C02i9Cf7cpBz1B8p2JzFlV7cu', 'Activo', 6, 'D7lYWZvz3RzT959fisCk', '2017-12-02 04:24:29', '2017-12-02 04:24:29'),
(2, 'Ferreteria Electricos RyR', '', 'FerreteriaElectricosRyR@gmail.com', '', '$2y$10$HYCSkst5AJOy51l0TaE15.voNohmzyBfnpeqlpFbeIz81KmJ607.G', 'Activo', 6, 'cmVJceqcKt5QuMgjHkwp', '2018-01-10 17:23:36', '2018-01-10 17:23:36'),
(3, 'Ferreteria Mundo Corona   Liliana', '3045460125', 'sincorreo1-@gemail.com', 'Buenaventura', '$2y$10$yh9ZZE2nbp9uwGYTCgBCquXNIpo0TrVmxWfoFncVtkukY16SLAVde', 'Activo', 8, 'zYKtY8jVhFOCwHenznhN', '2017-12-08 00:45:20', '2017-12-08 00:45:20'),
(4, 'Ferreteria Ferre Equipos del Pacífico Druman', '2446405   2425884', 'sincorreo2@hotmail.com', 'Buenaventura', '$2y$10$th3.n6SyArNmXfoXSWA94e4pFAKYqDDXhuVmkhIIEexYwbgpwNy1O', 'Activo', 8, 'qyG3EmhuwGbJxXYr8PPI', '2017-12-08 00:48:13', '2017-12-08 00:48:13'),
(5, 'Ferreteria Jazmin Diego', '3167683313-3117715714', 'FerreJazminDiego@gmail.com', '', '$2y$10$kOwJHD1FPcSq.TzL/sZW6uCJ0rVmQMwrCH2edDwHID49bn9pBwotO', 'Activo', 6, 'Bhb5D44SMy0dlRRh10x4', '2018-01-10 16:35:40', '2018-01-10 16:35:40'),
(6, 'Ferreteria Estiben', '', 'FerreteriaEstiben@gmail.com', '', '$2y$10$1jTaClzlJ7sLhhK..N7LcexQKzfaFvOScJC05vJVskVEfoPeQjvT2', 'Activo', 6, 'bgfRCM4MAoaiZiNvDBnF', '2018-01-10 17:24:16', '2018-01-10 17:24:16'),
(7, 'ferre Doble AA Alberto Londoño', '2446405-2425884', 'ferreDobleAA@gmail.com', '', '$2y$10$Piiszj/oDyv3sQ1hHHYYKerN/gbXfbG8peXxhUckTg1RM1aDXsgRG', 'Activo', 6, '1LCGsM0PB6edKRg8QyhA', '2018-01-10 16:39:23', '2018-01-10 16:39:23'),
(8, 'Ferreteria Materiales la Gta gloria', '', 'FerreteriaMaterialeslGtagloria@gmail.com', '', '$2y$10$jyL2NgJiw3EtdtfieMn/kOyDyBTcr6c2ouQDlSixlplXLBi7F/ehG', 'Activo', 8, 'tbP0d0o75E9O8bVGANWQ', '2018-01-10 16:45:02', '2018-01-10 16:45:02'),
(9, 'Constru Pasifico Mariela Quiseno', '3134999557', 'ConstruPasificoMarielaQuiseno@gamail.com', '', '$2y$10$0a08WadSiY3PO9/AuqTsVetL3xMi0gKnwWf9q.rAlkkjT/erqiPrW', 'Activo', 8, 'QfS4hen49vDUdMMwh1pL', '2018-01-10 16:46:51', '2018-01-10 16:46:51'),
(10, 'Ferreteria Super Rebaja Rubiel Gallego', '3173734212', 'FerreteriaSuperRebajaRubielGallego@gmail.com', '', '$2y$10$QJziJj9t0YXM/RF2oL9VQu5HZvM62kht90SKDtc7l/vNVs5GTnZ0a', 'Activo', 6, '6EYLWz0QzMvRbT7zuAsI', '2018-01-10 16:48:11', '2018-01-10 16:48:11'),
(11, 'Ferreteria Choco Jose Murrillo', '3155944242', 'FerreteriaChocoJoseMurrillo@gmail.com', '', '$2y$10$l7jsuZ7oV2S4I5JCgzx/o.DW4wzWDbkXBlmVKeXqSSaYKbNj2moDG', 'Activo', 6, 'xjTmnFPYjKERlE1SGzwN', '2018-01-10 16:49:35', '2018-01-10 16:49:35'),
(12, 'Ferreteria Las Piedras Nelson Perez', '3148878593', 'FerreteriaLasPiedrasNelsonPerez@gmail.com', '', '$2y$10$6GdzZuEh.f9eTEaHaXhVbuRk5Wp0Nx/OGTtPwIiyPNk8wBFmecWDC', 'Activo', 6, 'VAPz0IbLzxMwHa0uEOyr', '2018-01-10 16:50:44', '2018-01-10 16:50:44'),
(13, 'Ferreteria Vinasco Liliana', '3104524369', 'FerreteriaVinascoLiliana@gmail.com', '', '$2y$10$yjNTGwUCkrVSCxGbnEhS6OdGpLIHRk2oqr6vksU9DgzKefqHVE2Nm', 'Activo', 6, 'ZyaqP5FvjoYafUJcGcQl', '2018-01-10 16:51:46', '2018-01-10 16:51:46'),
(14, 'Ferreteria Ferro Libertad Silvia', '3165216314', 'FerreteriaFerroLibertadSilvia@gmail.com', '', '$2y$10$qsbreQgAVxIpS2TnkjEfhuah75fRVnCAzUsL9jPeTCiMLC246TMMq', 'Activo', 6, 'rYmf7QzBcNxmmy7aiSCc', '2018-01-10 16:52:56', '2018-01-10 16:52:56'),
(15, 'Ferreteria Mi Buenaventura Jawes', '3117487800', 'FerreteriaMiBuenaventuraJawes@gmail.com', '', '$2y$10$WCqdwpACzK2RV2M2lC.gxei/OMapKFBnOMp33.Yd5zAn1ItfiA7MC', 'Activo', 6, 'CRJXdD3iCCCh1biKPHR7', '2018-01-10 16:54:19', '2018-01-10 16:54:19'),
(16, 'Ferreteria Las Americas Ruben Dario', '3107180904', 'FerreteriaLasAmericasRubenDario@gmail.com', '', '$2y$10$wpi0Td62xG6KEuMavl5h5.4b.hE9mSTft.z4j/qWCL3IMMqEEwKlG', 'Activo', 6, 'in9HjrGcoHpiqNAnOx7m', '2018-01-10 16:55:35', '2018-01-10 16:55:35'),
(17, 'Ferreteria Nueva Granada Jorge', '', 'FerreteriaNuevaGranadaJorge@gmail.com', '', '$2y$10$lcG/C/..NZkKfqYHljbSBuh/kFe7IdSquv7bq0ba5Uj5JrKLq1DES', 'Activo', 6, 'E442OXcysDCZ0qUWGhrI', '2018-01-10 16:56:30', '2018-01-10 16:56:30'),
(18, 'Ferreteria Construmar Beatriz Adriana', '2428631-3206734494', 'FerreteriaConstrumarBeatrizAdriana@gmail.com', '', '$2y$10$ktdccECfo7fWg1rbAAtq9.6Naq/WylcmS4XlIb8W9bUrIa4Jbvu9u', 'Activo', 6, 'x5v3PqueGC22bam8SnAZ', '2018-01-10 16:57:40', '2018-01-10 16:57:40'),
(19, 'Ferreteria Hierros Londoño Katerine', '3177304523', 'FerreteriaHierrosLondonoKaterine@gmail.com', '', '$2y$10$R6ALmGva9urJCEZr8Qu0Z.itT.2rKbHVlldHrNP5nFQMTYmByJrve', 'Activo', 6, 'pFLdOV5mbSKaRdTOBDPt', '2018-01-10 17:01:29', '2018-01-10 17:01:29'),
(20, 'Ferreteria Los Construtores Hugo', '', 'FerreteriaLosConstrutoresHugo@gmail.com', '', '$2y$10$.hQjpkovLmy4xOkXY/aE8udAMsVAFaNBQNCYdg.4Z5viXJC1bHYtC', 'Activo', 6, 'DgDMCfiEknKb3GvkN3lf', '2018-01-10 17:02:11', '2018-01-10 17:02:11'),
(21, 'Ferreteria Torre Mar Amparo', '', 'FerreteriaTorreMarAmparo@gmail.com', '', '$2y$10$cQEt8apDNXyNkFAHZfMuju1lRT47ZWsoV3hqBESGhfq2.AD8fnycy', 'Activo', 6, 'FYKXlfaR7OjxdRxHurkO', '2018-01-10 17:03:04', '2018-01-10 17:03:04'),
(22, 'Ferreteria El Carmen Yesenia', '', 'FerreteriaElCarmenYesica@gmail.com', '', '$2y$10$LN36PP0fNISf4KIi8IxNwu7g61o5f/G3xdgGbih8UKYtPrSKRL4EO', 'Activo', 6, 'MkJmPamkUMk054wJU3TJ', '2018-01-10 17:04:13', '2018-01-10 17:04:13'),
(23, 'Ferreteria La Torre Robinson ', '', 'FerreteriaLaTorreRobinson@gamail.com', '', '$2y$10$wobKROip2y1tfukT8qx16ef7HmEJ17D/kifg67Lo8Awpw13x2s1Ai', 'Activo', 6, 'HaG55Ce1fFSHrjbeCHjL', '2018-01-10 17:05:22', '2018-01-10 17:05:22'),
(24, 'Ferreteria Horizonte Jose Abel', '3178616537', 'FerreteriaHorizonteJoseAbel@gmail.com', '', '$2y$10$rpi.YprhHRPoIIaym8LFVODszBHO2jisqndFuzyFuY3Y9qyK1DFRi', 'Activo', 6, '1r44LD1X69vOkvg0u24p', '2018-01-10 17:06:21', '2018-01-10 17:06:21'),
(25, 'Ferreteria Costa Sra Margarita', '', 'FerreteriaCostaSraMargarita@gamail.com', '', '$2y$10$nVULxoLiL8SGluFbIeMXoefyJtmNvypKbWCd.gRFZ1LDfSb6XCHqG', 'Activo', 6, '9E1aXZv6CC17SInXvqdI', '2018-01-10 17:07:24', '2018-01-10 17:07:24'),
(26, 'Ferreteria Panel Y Super Board Matato', '', 'FerreteriaPanelYSuperBoardMatato@gmail.com', '', '$2y$10$Ui9gr0r/JVeN5cW4n8MJg.fhMdAkK5pjO3s5EynBaz3VGuVKajYuO', 'Activo', 6, '6lG9JDHTpt6zT9KSSjd9', '2018-01-10 17:08:30', '2018-01-10 17:08:30'),
(27, 'Ferreteria Eléctricos Banco Juan Gabriel', '', 'FerreteriaElectricosBancoJuanGabriel@gmail.com', '', '$2y$10$92TT0lR26sWbFsC5ADHUr.MBBLoRPsJVH443XEHAtOL1EZekRTXD6', 'Activo', 6, 'h18DI9zEtAuE6105hHBX', '2018-01-10 17:09:34', '2018-01-10 17:09:34'),
(28, 'Ferreteria Shary Sro Fernando', '', 'FerreteriaSharySroFernando@gmail.com', '', '$2y$10$owwZQLexwiW5zUyfjlQhcucpRaVldzo5dmp8ohdxYk.XbIuMYFeo2', 'Activo', 6, 'iqsZqIFdotzdv6oFgd1w', '2018-01-10 17:10:59', '2018-01-10 17:10:59'),
(29, 'Eléctrico La Otra Esquina Sro Carlos', '', 'ElectricoLaOtraEsquinaSroCarlos@gmail.com', '', '$2y$10$rVRTJm.eSAZlpGaoTWJFROMvEYDB6VhjcbFAnc/Qvxixrepr4SVMi', 'Activo', 6, 'ctyeCphjV1NlIlNkvi6j', '2018-01-10 17:12:02', '2018-01-10 17:12:02'),
(30, 'Ferreteria Mariana Sro Alejandro', '', 'FerreteriaMarianaSroAlejandro@gmail.com', '', '$2y$10$ZApcMM7yzB3hyoUwE.Z6M.ymdEb.o/pW5/XrkrviI1wqMpgIxjWNu', 'Activo', 6, 'sAG7ujSRhGwrXO0serqr', '2018-01-10 17:12:53', '2018-01-10 17:12:53'),
(31, 'Ferreteria Materiales Gonzalez Sro Sebastian', '', 'FerreteriaMaterialesGonzalezSroSebastian@gmail.com', '', '$2y$10$cX02d/H99EUKiVPVIrV//OVi4z8io9h5P3J86A9w9/Jw8DFAa3OYO', 'Activo', 6, 'gX1mgDHpyJ5yWixxZ5MH', '2018-01-10 17:14:17', '2018-01-10 17:14:17'),
(32, 'Ferreteria El Punto Clave Del Jorge Sro Victor', '', 'FerreteriaElPuntoClaveDelJorgeSroVictor@gmail.com', '', '$2y$10$G8UwZVXS9qzoBOS9VK13duMt8Q4loV453CUibnExRa/eF36atssMW', 'Activo', 6, '1DNFwvUzv423LOgkWK7p', '2018-01-10 17:15:32', '2018-01-10 17:15:32'),
(33, 'Ferreteria Naya Brinlly', '', 'FerreteriaNayaBrinlly@gmail.com', '', '$2y$10$hxBrBWWdW9Nnd.daINRv5OGvbCwycV5yy5zld5.UoONQCxYa6TpBq', 'Activo', 6, 'ligdXx7mqC1ZGNhrXPmT', '2018-01-10 17:16:20', '2018-01-10 17:16:20'),
(34, 'Ferreteria Maderas La 41 Octavio Murrillo', '', 'FerreteriaMadera@gmail.com', '', '$2y$10$6ZZwljKHpPYRfT2t.tshNe3J7RzYNgSgkwocJmbaFVof4E4dHheMi', 'Activo', 6, 'Ukaw7ey5h5xeKjCxCzPQ', '2018-01-10 17:17:17', '2018-01-10 17:17:17'),
(35, 'Ferreteria Ferragro Sro Diego', '', 'FerreteriaFerragroSroDiego@gmail.com', '', '$2y$10$2LGhy6GL60R0c7uCDgctoO3LwLqoTBLpCXNtikEPpL1CoZKKM7EUe', 'Activo', 6, 'hKJRkE9P2skDGHnL43h3', '2018-01-10 17:18:16', '2018-01-10 17:18:16'),
(36, 'Ferreteria Economía Del Pacifico William', '', 'FerreteriaEconomia@gmail.com', '', '$2y$10$ascpLc9r5FmiOLKVxqfnCuqhdVvfjwyEyBcqq8cPLhzZ1IV9goCIO', 'Activo', 6, 'nQDQ0R8maBuMlhMBH3rV', '2018-01-10 17:19:13', '2018-01-10 17:19:13'),
(37, 'Maria Eugenia Davila', '', 'MariaEugeniaDavila@gmail.com', '', '$2y$10$ZGpVitg1jxbt7313BxjH6erCELRvZWQkmToIwMsgh591oTah9GmMK', 'Activo', 6, 'gk0ubNvOKErZ7DefYt0a', '2018-01-10 17:19:46', '2018-01-10 17:19:46'),
(38, 'Ferreteria Única Sro Guillermo', '', 'FerreteriaUnicaSroGuillerm@gmail.com', '', '$2y$10$1y6I3NrvDXnCD.7bpx4GkOinU3x/hvEix6cuMNYBnfRLN.5KDqbs.', 'Activo', 6, 'GNQm8rUSY42MhbPTHBGI', '2018-01-10 17:21:27', '2018-01-10 17:21:27'),
(39, 'Ferreteria El Cruzero Sro Arley', '', 'FerreteriaElCruzeroSroArley@gmail.com', '', '$2y$10$ONGl3ur3sIezLUJ51p.KMufv2.cBo5c8JBME2gI5WkWqkYOH.blp.', 'Activo', 6, 'mYbXZ9h1rCAZG6q7WC5Q', '2018-01-10 17:22:38', '2018-01-10 17:22:38');

INSERT INTO proveedores (id, empresa, tel, correo, address, estado, nit, camara, rut, certificado, created_at, updated_at) VALUES
(1, 'Aquaterra s.a.s.', '3216448107', 'ventascali1@aquaterra.com.co', 'carrera 6 # 24-28 cali', 'Activo', '811027317-9', 'images/default.jpg', 'images/default.jpg', 'images/default.jpg', '2017-12-07 01:21:13', '2017-12-07 01:21:13'),
(2, 'Romeflex SAS', '2642141 y 4501033', 'johann9205@hotmail.com', 'carrera 72 m bis # 37 a 25 sur bogota', 'Activo', '900006256-0', 'images/default.jpg', 'images/default.jpg', 'images/default.jpg', '2017-12-07 01:34:20', '2017-12-07 01:34:20'),
(3, 'Argos', '', 'argos@argos.com', '', 'Activo', '12345678', 'images/default.jpg', 'images/default.jpg', 'images/default.jpg', '2018-01-10 16:40:43', '2018-01-10 16:40:43'),
(4, 'Millo', '', 'millo@millo.com', '', 'Activo', '121354546453', 'images/default.jpg', 'images/default.jpg', 'images/default.jpg', '2018-01-10 16:41:55', '2018-01-10 16:41:55');

INSERT INTO materia_prima (id, cod, detalle, medida, estado, created_at, updated_at) VALUES
(1, 'Viaje Arena Mojado', '', 'Atado', 'Activo', '2018-01-12 10:34:59', '2018-01-12 10:34:59'),
(2, 'Viaje Arena Seca', '', 'Atado', 'Activo', '2018-01-12 10:35:11', '2018-01-12 10:35:11'),
(3, 'Químico 2', '', 'Gramos', 'Activo', '2018-01-12 10:35:46', '2018-01-12 10:35:46'),
(4, 'Químico 1', '', 'Gramos', 'Activo', '2018-01-12 10:36:00', '2018-01-12 10:36:00'),
(5, 'Aceite Quemado', '', 'Galón', 'Activo', '2018-01-12 10:36:34', '2018-01-12 10:36:34'),
(6, 'Bolsas Vacías', '', 'Unidad', 'Activo', '2018-01-12 10:36:48', '2018-01-12 10:36:48'),
(7, 'Fraguas', '', 'Unidad', 'Activo', '2018-01-12 10:37:00', '2018-01-12 10:37:00'),
(8, 'Mineral Amarrillo', '', 'Kilogramos', 'Activo', '2018-01-12 10:37:20', '2018-01-12 10:37:20'),
(9, 'Mineral Blanco', '', 'Kilogramos', 'Activo', '2018-01-12 10:38:08', '2018-01-12 10:38:08'),
(10, 'Cajas Vacías', '', 'Unidad', 'Activo', '2018-01-12 10:38:21', '2018-01-12 10:38:21'),
(11, 'Arena', '', 'Kilogramos', 'Activo', '2018-01-12 10:41:27', '2018-01-12 10:41:27'),
(12, 'Cemento', '', 'Kilogramos', 'Activo', '2018-01-12 10:41:42', '2018-01-12 10:41:42');

INSERT INTO artiiculos (id, art_nombre, detalle, precio_unitario, impuesto, medida, estado, created_at, updated_at) VALUES
(1, 'Pega Valle Porcelanato', 'Pegante para el pegado de baldosa ', 11000, 0, 'Bulto', 'Activo', '2017-11-29 18:18:28', '2017-12-02 03:57:22'),
(2, 'Pega Valle cerámica', 'Pegante para pegado cerámica', 9500, 0, 'Bulto', 'Activo', '2017-12-03 10:15:41', '2017-12-03 10:15:41');

INSERT INTO inventario_artiiculos (id_inventario_art, id_artiiculos, disponible, created_at, updated_at) VALUES
(1, 1, 0, '2017-11-29 13:18:28', '2017-11-29 13:18:28'),
(2, 2, 0, '2017-12-03 05:15:41', '2017-12-03 05:15:41');

INSERT INTO inventario_mp (id_inventario_mp, id_mp, disponible, totalcosto, created_at, updated_at) VALUES
(1, 1, 0, 0, '2018-01-12 10:35:00', '2018-01-12 10:35:00'),
(2, 2, 0, 0, '2018-01-12 10:35:12', '2018-01-12 10:35:12'),
(3, 3, 0, 0, '2018-01-12 10:35:46', '2018-01-12 10:35:46'),
(4, 4, 0, 0, '2018-01-12 10:36:00', '2018-01-12 10:36:00'),
(5, 5, 0, 0, '2018-01-12 10:36:34', '2018-01-12 10:36:34'),
(6, 6, 0, 0, '2018-01-12 10:36:48', '2018-01-12 10:36:48'),
(7, 7, 0, 0, '2018-01-12 10:37:00', '2018-01-12 10:37:00'),
(8, 8, 0, 0, '2018-01-12 10:37:20', '2018-01-12 10:37:20'),
(9, 9, 0, 0, '2018-01-12 10:38:08', '2018-01-12 10:38:08'),
(10, 10, 0, 0, '2018-01-12 10:38:21', '2018-01-12 10:38:21'),
(11, 11, 0, 0, '2018-01-12 10:41:27', '2018-01-12 10:41:27'),
(12, 12, 0, 0, '2018-01-12 10:41:42', '2018-01-12 10:41:42');

INSERT INTO requisito_art (id_requisitoart, id_artiiculos, id_mp, cantidad, created_at, updated_at) VALUES
(1, 2, 11, 22.72, '2018-01-12 10:42:04', '2018-01-12 10:42:04'),
(2, 2, 12, 4.54, '2018-01-12 10:42:17', '2018-01-12 10:42:17'),
(3, 2, 4, 36.36, '2018-01-12 10:42:44', '2018-01-12 10:42:44'),
(4, 2, 5, 0.08, '2018-01-12 10:43:02', '2018-01-12 10:43:02'),
(5, 2, 6, 1, '2018-01-12 10:43:12', '2018-01-12 10:43:12'),
(6, 1, 11, 22.72, '2018-01-12 10:47:10', '2018-01-12 10:47:10'),
(7, 1, 12, 4.54, '2018-01-12 10:48:09', '2018-01-12 10:48:09'),
(8, 1, 3, 36.36, '2018-01-12 10:48:31', '2018-01-12 10:48:31'),
(9, 1, 6, 1, '2018-01-12 10:49:04', '2018-01-12 10:49:04'),
(10, 1, 5, 0.09, '2018-01-12 10:49:29', '2018-01-12 10:49:29');


INSERT INTO plantilla
(
id_plantilla,
id_personal,
contrato,
arl_categoria,
auxilio,
comisiones,
otros,
created_at,
updated_at
) VALUES
(1, 1, 1, 1, 1, 0, 0, 'Always', 'Always'),
(2, 2, 1, 1, 1, 0, 0, 'Always', 'Always'),
(3, 3, 1, 1, 1, 0, 0, 'Always', 'Always'),
(4, 4, 1, 1, 1, 0, 0, 'Always', 'Always'),
(5, 5, 1, 1, 1, 0, 0, 'Always', 'Always'),
(6, 6, 1, 1, 1, 0, 0, 'Always', 'Always'),
(7, 7, 1, 1, 1, 0, 0, 'Always', 'Always'),
(8, 8, 1, 1, 1, 0, 0, 'Always', 'Always'),
(9, 9, 1, 1, 1, 0, 0, 'Always', 'Always'),
(10, 10,1,  1, 1, 0, 0, 'Always', 'Always');
