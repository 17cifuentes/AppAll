cd /D C:\xampp\mysql\bin
mysql -u root
--elimino pacientes si ya está creada
drop database if EXISTS appweb;
create database if not EXISTS appweb;
--uso la base de datos que he creado
Use appweb
-- .1n

select '-- recursos humanos --' AS '';

select 'Tabla personal' AS '';
-- .1n .2n
CREATE TABLE personal 
(
id_personal int(11) primary key,
nombre varchar(250),
tel varchar(250),
correo varchar(250),
address varchar(250),
cargo varchar(250),
valor_a_pagar int(11),
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE personal
ADD UNIQUE KEY correo(correo),
MODIFY id_personal int(11)  AUTO_INCREMENT;


select 'Tabla usuarios' AS '';
create table users
(
id int(11) primary key,
name varchar(250), 
rol varchar(250),
email varchar(250),
password varchar(255),
id_personal int(11),
user_estado varchar(200),
pic varchar(200),
cuota int(11) , 
remember_token varchar(100),
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL,

constraint fk_usuario_personal
foreign key(id_personal)
references personal(id_personal)
on delete no action
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE users
ADD UNIQUE KEY email(email),
MODIFY id int(11)  AUTO_INCREMENT;

select 'Tabla materia prima' AS '';
-- n6. -- n4.
create table materia_prima
(
id int(11)  primary key,
cod varchar(250) ,
detalle varchar(250) ,
medida varchar(250),
estado varchar(20) ,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE materia_prima
ADD UNIQUE KEY cod(cod),
MODIFY id int(11)  AUTO_INCREMENT;

select '-- Inventarios --' AS '';

select 'Tabla materia prima' AS '';
-- n6.
create table inventario_mp
(
id_inventario_mp int(11)  primary key,
id_mp int(11),
disponible double ,
totalcosto double ,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL,

constraint fk_invmp_mp
foreign key(id_mp)
references materia_prima(id)
on delete no action

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE inventario_mp
MODIFY id_inventario_mp int(11)  AUTO_INCREMENT;

select 'Tabla producto terminado' AS '';
-- n7. -- n10. -- n5. -- 
create table artiiculos
(
id int(11)  primary key,
art_nombre varchar(250) ,
detalle varchar(250) ,
precio_unitario int(11) ,
impuesto tinyint(2) ,
medida varchar(20),
estado varchar(20),
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL  
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE artiiculos
ADD UNIQUE KEY art_nombre(art_nombre),
MODIFY id int(11)  AUTO_INCREMENT;

select 'Tabla artículos' AS '';
-- n7.
create table inventario_artiiculos
(
id_inventario_art int(11)  primary key,
id_artiiculos int(11),
disponible double ,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL,

constraint fk_invartt_art
foreign key(id_artiiculos)
references artiiculos(id)
on delete no action

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE inventario_artiiculos
MODIFY id_inventario_art int(11)  AUTO_INCREMENT;

select '-- Tablas facturas de ventas --' AS '';

select 'Crear tabla clientes' AS '';
-- .9. -- .14.
CREATE TABLE clientes 
(
id int(11)  primary key,
nombre_clientes varchar(255) ,
tel char(30) ,
correo_cliente varchar(64) ,
address varchar(255) ,
password varchar(255) ,
estado varchar(20)  ,
vendedor int(11) ,
valida varchar(20)  ,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE clientes
ADD UNIQUE KEY correo(correo_cliente),
MODIFY id int(11)  AUTO_INCREMENT; 

select 'Crear tabla facturas' AS '';
-- .13n -- .14. -- .15.
CREATE TABLE facturas
(
id_facturas int(11)  primary key,
num_factura int(11) ,
id_clientes int(11),
id_vendedor int(11),
tipo_pago varchar(20) ,
total_venta int(11)  DEFAULT NULL,
total_cuota int(11)  DEFAULT NULL,
estado_factura varchar(20) ,
fecha varchar(20),
ciudad varchar(100),
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL,

constraint fk_facturas_clientes
foreign key(id_clientes)
references clientes(id)
on delete no action,

constraint fk_facturas_vendedor
foreign key(id_vendedor)
references users(id)
on delete no action

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE facturas
ADD UNIQUE KEY num_factura(num_factura),
MODIFY id_facturas int(11)  AUTO_INCREMENT;

select 'Tabla del contenido de la factura' AS '';
-- n15.
CREATE TABLE detalle_factura
(
id_detalle int(11)  primary key,
num_factura int(11),
id_producto int(11) ,
cantidad double ,
impuesto double ,
precio_venta double DEFAULT NULL,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL

-- constraint fk_detfactura_factura
-- foreign key(num_factura)
-- references facturas(num_factura)
-- on delete no action

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE detalle_factura
MODIFY id_detalle int(11)  AUTO_INCREMENT;

select 'Tabla detalle artiiculo' AS '';
-- n5.
CREATE TABLE detalle_art
(
id_detalleart int(11)  primary key,
id_artiiculos int(11),
cantidad double ,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL,

constraint fk_detart_artiiculos
foreign key(id_artiiculos)
references artiiculos(id)
on delete no action

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE detalle_art
MODIFY id_detalleart int(11)  AUTO_INCREMENT;

CREATE TRIGGER 
disponible_artiiculo 
AFTER INSERT ON 
detalle_art
FOR EACH ROW UPDATE 
inventario_artiiculos
SET 
inventario_artiiculos.disponible = NEW.cantidad + inventario_artiiculos.disponible
where 
inventario_artiiculos.id_artiiculos = NEW.id_artiiculos
;

CREATE TABLE requisito_art
(
id_requisitoart int(11)  primary key,
id_artiiculos int(11),
id_mp int(11) ,
cantidad double ,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL,

constraint fk_reqart_artiiculos
foreign key(id_artiiculos)
references artiiculos(id)
on delete no action

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE requisito_art
MODIFY id_requisitoart int(11)  AUTO_INCREMENT;

select 'Tabla proveedores' AS '';
-- n3n
CREATE TABLE proveedores 
(
id int(11)  primary key,
empresa varchar(250) ,
tel varchar(250) ,
correo varchar(250) ,
address varchar(250) ,
estado varchar(20) ,
nit varchar(250) ,
camara varchar(250) ,
rut varchar(250) ,
certificado varchar(250) ,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE proveedores
ADD UNIQUE KEY correo(correo),
ADD UNIQUE KEY nit(nit),
MODIFY id int(11)  AUTO_INCREMENT;

select 'Tabla detalle materia prima' AS '';
-- n3n -- n4.
CREATE TABLE detalle_mp
(
id_detallemp int(11)  primary key,
id_mp int(11) ,
detalle varchar(60),
cantidad double ,
costo double ,
tipo tinyint(1) ,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL,

-- constraint fk_detmp_proveedores
-- foreign key(id_proveedor)
-- references proveedores(id)
-- on delete no action,

constraint fk_detmp_materiaprima
foreign key(id_mp)
references materia_prima(id)
on delete no action

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE detalle_mp
MODIFY id_detallemp int(11)  AUTO_INCREMENT;

CREATE TRIGGER 
disponible_mpP
AFTER INSERT ON 
detalle_mp 
FOR EACH ROW UPDATE 
inventario_mp
SET 
inventario_mp.disponible = IF(NEW.tipo = 1, inventario_mp.disponible + NEW.cantidad, inventario_mp.disponible - NEW.cantidad),
inventario_mp.totalcosto = IF(NEW.tipo = 1,(((inventario_mp.totalcosto*(inventario_mp.disponible-NEW.cantidad)) + (NEW.costo*NEW.cantidad)) / inventario_mp.disponible),(inventario_mp.totalcosto))
where 
inventario_mp.id_mp = NEW.id_mp
;

--CREATE TRIGGER 
--disponible_mpM
--BEFORE DELETE ON 
--detalle_mp 
--FOR EACH ROW UPDATE 
--inventario_mp
--SET 
--inventario_mp.disponible = inventario_mp.disponible - OLD.cantidad
--where 
--inventario_mp.id_mp = OLD.id_mp
--;

select 'Tabla pedidos' AS '';
-- n9. -- n11. -- .12.
CREATE TABLE pedidos
(
id_pedidos int(11)  primary key,
num_pedido int(11) ,
id_cliente int(11) ,
tipo_pago varchar(20) ,
total_venta int(11) ,
estado_factura varchar(20) ,
fecha varchar(20),
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE pedidos
ADD UNIQUE KEY num_pedido(num_pedido),
MODIFY id_pedidos int(11)  AUTO_INCREMENT;

select 'Tabla detalle pedidos' AS '';
-- .12n -- n10.
CREATE TABLE detalle_pedidos
(
num_pedido int(11) ,
id_producto int(11) ,
cantidad double ,
impuesto double ,
precio_venta double  DEFAULT NULL,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE detalle_pedidos;

select '-- Gestión vendedores --' AS '';

select 'Tabla gastos laborales' AS '';
-- n8.
CREATE TABLE gastos 
(
id_gastos int(11)  primary key,
id_personal int(11) ,
concepto varchar(250) ,
comprobante varchar(250) ,
valor double DEFAULT NULL,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE gastos
MODIFY id_gastos int(11)  AUTO_INCREMENT;


CREATE TABLE empresa 
(
id int(1) primary key ,
nombre varchar(255) ,
tel char(30) ,
correo varchar(64) ,
nit varchar(255) ,
address varchar(255) ,
ciudad varchar(255) ,
paiis varchar(255) ,
pic varchar(255) ,
cod_postal varchar(255) ,
lema1 varchar(255) ,
lema2 varchar(255) ,
factura int(6) ,
pensioon double ,
salud double ,
cesantiia double ,
intereses double ,
prima double ,
vacaciones double ,
salario double ,
auxilio double ,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


select 'Tabla nómina' AS '';
create table plantilla
(
id_plantilla int(11) primary key,
id_personal int(11),
contrato int(1),
arl_categoria double DEFAULT NULL,
auxilio int(1),
comisiones double DEFAULT NULL,
otros double DEFAULT NULL,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL,

constraint fk_plantilla_personal
foreign key(id_personal)
references personal(id_personal)
on delete no action
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE plantilla
MODIFY id_plantilla int(11)  AUTO_INCREMENT;



select 'Tabla producción' AS '';
create table produccioon
(
id int(11) primary key,
id_personal int(11),
id_artiiculos int(11),
detalle varchar(255),
cantidad double DEFAULT NULL,
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL,

constraint fk_produccioon_personal
foreign key(id_personal)
references personal(id_personal)
on delete no action,

constraint fk_produccioon_art
foreign key(id_artiiculos)
references artiiculos(id)
on delete no action
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE produccioon
MODIFY id int(11)  AUTO_INCREMENT;

select 'Tabla reportes' AS '';
create table reporte
(
fecha varchar(250),
tipo varchar(250),
valor double DEFAULT NULL,
detalle varchar(250),
created_at timestamp NULL DEFAULT NULL,
updated_at timestamp NULL DEFAULT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

select 'Tabla liquidar' AS '';
create table liquidar
(
liq_fecha varchar(250),
liq_valor_diia varchar(250),
liq_diias varchar(250),
liq_auxilio varchar(250),
liq_base_total varchar(250),
liq_cesantiia varchar(250),
liq_int_cesantiia varchar(250),
liq_vacaciones varchar(250),
liq_seguro varchar(250),
liq_pensioon varchar(250),
liq_total varchar(250)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;