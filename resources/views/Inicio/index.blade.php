@include('layouts.dash.header')
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- Jquery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Datepicker Files -->
    <script src="{{asset('datePicker/js/bootstrap-datepicker.js')}}"></script>
    <!-- Languaje -->
    <script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>
@include('layouts.dash.menu')
<?php

$dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
?>

  <div class="btn-floating" id="help-actions">
  <div class="btn-bg"></div>
  <button type="button" class="btn btn-default btn-toggle" data-toggle="toggle" data-target="#help-actions">
    <i class="icon fa fa-plus"></i>
    <span class="help-text">Accesos</span>
  </button>
  <div class="toggle-content">
    <ul class="actions">
      <li><a target="_blank" href="#" >Página Web</a></li>
      <li><a href="#">Ayuda</a></li>
    </ul>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card card-banner card-chart card-green no-br">
      <div class="card-header">
        <div class="card-title">
          <div class="title">Mejores Ventas por Días para <?php echo $meses[date('n')-1] ?></div>
        </div>
        <ul class="card-action">
          <li>
            <a href="Inicio">
              <i class="fa fa-refresh"></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="card-body">
        <div class="ct-chart-sale"></div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <a class="card card-banner card-green-light">
  <div class="card-body">
    <i class="icon fa fa-shopping-basket fa-4x"></i>
    <div class="content">
      <div class="title">Ventas <?php
echo $Head;
?>
</div>
      <div class="value"><span class="sign">$</span>{{ number_format((int)$totalFactura + (int)$cuotapagadasFactura,0) }}</div>
    </div>
  </div>
</a>

  </div>


</div>

<div class="row">
	<div class="col-md-3">
	</div>
  <div class="col-md-6">
    <div class="card card-mini">
      <div class="card-header">
        <div class="card-title">Facturas</div>
        <ul class="card-action">
          <li>
            <a href="Inicio">
              <i class="fa fa-refresh"></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="card-body no-padding table-responsive">
        <table class="table card-table">
          <thead>
            <tr>
              <th class="right">Valor</th>
              <th>Estado</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td align="right">
			  <a href="{{ url('Factura/index') }}">{{ $Pagadas }}</a>
			  </td>
              <td><span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span>Pagadas</span></span></td>
            </tr>
            <tr>
              <td align="right">
			  <a href="{{ url('Factura/index') }}">{{ $Entregadas }}</a>
			  </td>
              <td><span class="badge badge-primary badge-icon"><i class="fa fa-truck" aria-hidden="true"></i><span>Entregadas</span></span></td>
            </tr>			
            <tr>
              <td align="right">
			  <a href="{{ url('Factura/index') }}">{{ $Pendientes }}</a>			  
			  </td>
              <td><span class="badge badge-warning badge-icon"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Pendientes</span></span></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
	<div class="col-md-3">
	</div>  
<!--
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="card card-tab card-mini">
      <div class="card-header">
        <ul class="nav nav-tabs tab-stats">
          <li role="tab1" class="active">
            <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Productos</a>
          </li>
          <li role="tab2">
            <a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Insumos</a>
          </li>
        </ul>
      </div>
      <div class="card-body tab-content">
        <div role="tabpanel" class="tab-pane active" id="tab1">
          <div class="row">
            <div class="col-sm-8">
              <div class="chart ct-chart-browser ct-perfect-fourth"></div>
            </div>
            <div class="col-sm-4">
              <ul class="chart-label">
                <li class="ct-label ct-series-a">Pegavalle Cerámica</li>
                <li class="ct-label ct-series-b">Pegavalle Porcelanato</li>
                <li class="ct-label ct-series-c">Pegante 1</li>
                <li class="ct-label ct-series-d">Pegante 2</li>
                <li class="ct-label ct-series-e">Pegante 3</li>
              </ul>
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab2">
          <div class="row">
            <div class="col-sm-8">
              <div class="chart ct-chart-os ct-perfect-fourth"></div>
            </div>
            <div class="col-sm-4">
              <ul class="chart-label">
                <li class="ct-label ct-series-a">Arena</li>
                <li class="ct-label ct-series-b">Bolsas</li>
                <li class="ct-label ct-series-c">Cemento</li>
                <li class="ct-label ct-series-d">Químico 1</li>
                <li class="ct-label ct-series-e">Químico 2</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  -->
</div>

  <div class="row">
    <div class="col-md-12">
    <form method="POST" action="{{ url('Empresa/inicio') }}" > 
    <input type="hidden" name="_token" value="{{ csrf_token() }}">     
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Reportes 
<?php
echo $Head;
?>
</th>
            <th align="center" valign="middle">
			<label>Fecha</label>	
			<input type="text"  id="Fecha" class="form-control datepicker" name="Fecha" value="<?php echo date('d/m/Y'); ?>">
			</th>
            <th align="center" valign="middle">
			<input type="submit" class="btn btn-info" title="Buscar reporte para ésa fecha" data-toggle="tooltip" data-placement="left" value="Buscar fecha" >
			</th>			
		</tr>
		<tr>
			<th align="center" valign="middle">
			
			</th>
            <th align="center" valign="middle">
			<!--
			<select required="" class="form-control" name="Mes">
			<option value="{{ date('n') }}">{{ $meses[date('n')-1] }}</option>
			@for ($i=1;$i<13;$i++)
			<option value="{{ $i }}">{{ $meses[$i-1] }}</option>
			@endfor
			</select>	
			-->
			</th>	
            <th align="center" valign="middle">
			<!--
			<input type="submit" class="btn btn-success" title="Buscar reporte para ése mes" data-toggle="tooltip" data-placement="left" value="Buscar mes" >			
			-->
			</th>			
        </tr>
    </thead>	
	</table>	
        </div>
        @include('alerts.validacion')
        <div class="card-header">
          <h4>Ingresos por Ventas: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6" align="right">
              <label>Total Efectivos en ventas al día </label>
              <input type="text" style="text-align:right;" readonly class="form-control" name="Venta" value="$ {{ number_format($totalFactura,0) }}" maxlength="250">
            </div>
            <div class="col-md-6" align="right">
              <label>Total saldos Créditos al día</label>
              <input type="text" style="text-align:right;" name="Crédito" readonly class="form-control" value="$ {{ number_format($cuotaFactura,0) }}" >
              <label>Total cuotas Créditos al día</label>
              <input type="text" style="text-align:right;" name="Crédito" readonly class="form-control" value="$ {{ number_format($cuotapagadasFactura,0) }}" >			  
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" align="center">
              <label>Total Ventas al día </label>
              <input type="text" style="text-align:center;" readonly class="form-control" name="Venta" value="$ {{ number_format((int)$totalFactura + (int)$cuotapagadasFactura,0) }}" maxlength="250">
            </div>
          </div>		  
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Activos en almacenamiento: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6" align="right">
              <label>Costo Total Insumos</label>
              <input type="text" style="text-align:right;" required="" readonly class="form-control" name="MP" value="$ {{ $CostoMP }}" maxlength="250">  
            </div>
            <div class="col-md-6" align="right">
              <label>Costo total en Producción</label>
              <input type="PT" style="text-align:right;" name="PT" readonly class="form-control" value="$ {{ $CostoPT }}" maxlength="250">
            </div>
          </div>
        </div>
      </div>
    </div>	

    <div class="col-md-12">
    </div>
  </form>
  </div>
  
  
<script>
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true
    });

	

</script>  

  
@include('layouts.dash.footer')

<script>

Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

if ($('.ct-chart-sale').length) {
	var Mes= "";
	var valores = [<?php echo '"'.implode('","', $array).'"' ?>];
	var max= 0;
	for (var item of valores) {
	if(item > max) max= item;
	}
	/*
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,	
	*/
  new Chartist.Line('.ct-chart-sale', {
    labels: [
	"01/"+Mes, 
	"02/"+Mes,
	"03/"+Mes,
	"04/"+Mes,
	"05/"+Mes,
	"06/"+Mes,
	"07/"+Mes,
	"08/"+Mes,
	"09/"+Mes,
	"10/"+Mes,
	"11/"+Mes,
	"12/"+Mes,
	"13/"+Mes,
	"14/"+Mes,
	"15/"+Mes,
	"16/"+Mes,
	"17/"+Mes,
	"18/"+Mes,
	"19/"+Mes,
	"20/"+Mes,
	"21/"+Mes,
	"22/"+Mes,
	"23/"+Mes,
	"24/"+Mes,
	"25/"+Mes,
	"26/"+Mes,
	"27/"+Mes,
	"28/"+Mes,
	"29/"+Mes,
	"30/"+Mes,
	"31/"+Mes,

	],
    series: [[
	parseInt(valores[0]).toFixed(0),
	parseInt(valores[1]).toFixed(0),
	parseInt(valores[2]).toFixed(0),
	parseInt(valores[3]).toFixed(0),
	parseInt(valores[4]).toFixed(0),
	parseInt(valores[5]).toFixed(0),
	parseInt(valores[6]).toFixed(0),
	parseInt(valores[7]).toFixed(0),
	parseInt(valores[8]).toFixed(0),
	parseInt(valores[9]).toFixed(0),
	parseInt(valores[10]).toFixed(0),
	parseInt(valores[11]).toFixed(0),
	parseInt(valores[12]).toFixed(0),
	parseInt(valores[13]).toFixed(0),
	parseInt(valores[14]).toFixed(0),
	parseInt(valores[15]).toFixed(0),
	parseInt(valores[16]).toFixed(0),
	parseInt(valores[17]).toFixed(0),
	parseInt(valores[18]).toFixed(0),
	parseInt(valores[19]).toFixed(0),
	parseInt(valores[20]).toFixed(0),
	parseInt(valores[21]).toFixed(0),
	parseInt(valores[22]).toFixed(0),
	parseInt(valores[23]).toFixed(0),
	parseInt(valores[24]).toFixed(0),
	parseInt(valores[25]).toFixed(0),
	parseInt(valores[26]).toFixed(0),
	parseInt(valores[27]).toFixed(0),
	parseInt(valores[28]).toFixed(0),
	parseInt(valores[29]).toFixed(0),
	parseInt(valores[30]).toFixed(0)
	
	]]
  }, {
    axisX: {
      position: 'center'
    },
    axisY: {
      offset: 0,
      showLabel: false,
      labelInterpolationFnc: function labelInterpolationFnc(value) {
        return value / 1000 + 'k';
      }
    },
    chartPadding: {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0
    },
    height: 300,
    high: max*1.2,
    showArea: true,
    stackBars: true,
    fullWidth: true,
    lineSmooth: false,
    plugins: [Chartist.plugins.ctPointLabels({
      textAnchor: 'left',
      labelInterpolationFnc: function labelInterpolationFnc(value) {
        return '$ ' + parseInt(value);// / 10000
      }
    })]
  }, [['screen and (max-width: 768px)', {
    axisX: {
      offset: 0,
      showLabel: false
    },
    height: 180
  }]]);
}

if ($('.ct-chart-browser').length) {
  (function () {
    var data = {
      series: [1000, 480, 705, 105, 50]
    };

    var sum = function sum(a, b) {
      return a + b;
    };

    new Chartist.Pie('.ct-chart-browser', data, {
      labelInterpolationFnc: function labelInterpolationFnc(value) {
        return Math.round(value / data.series.reduce(sum) * 100) + '%';
      },
      labelPosition: 'inside',
      startAngle: 270
    });
  })();
}

if ($('.ct-chart-os').length) {
  (function () {
    var data = {
      series: [1300, 200, 605, 205, 100]
    };

    var sum = function sum(a, b) {
      return a + b;
    };

    new Chartist.Pie('.ct-chart-os', data, {
      labelInterpolationFnc: function labelInterpolationFnc(value) {
        return Math.round(value / data.series.reduce(sum) * 100) + '%';
      },
      startAngle: 270,
      donut: true,
      donutWidth: 20,
      labelPosition: 'outside',
      labelOffset: -30
    });
  })();
}

</script>