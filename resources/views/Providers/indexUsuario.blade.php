@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[2] == '1')
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Gestión de Proveedores</th>
            <th align="center" valign="middle">
			<a href="#" data-toggle="modal" data-target="#ingresar" class="btn btn-info" title="Agregar un Proveedor" data-toggle="tooltip" data-placement="left"><span class="fa fa-plus"> </span><span class="fa fa-exchange"></span> Agregar Proveedor</a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="ingresar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<form action="{{ url('Providers/postregistro') }}" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
							  <div class="col-md-12">
							  <div class="card">
								<div class="card-header">
								  <h2>Registro De Proveedores</h2>
								</div>
								<div class="card-header">
								  <h4>Datos De Usuario: </h4>
								</div>
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									<label>Empresa </label>
									<input type="text" id="venci" class="form-control date" name="Empresa" maxlength="250">
									<label>Teléfono </label>
									<input type="text"  id="venci" class="form-control date" name="Teléfono" maxlength="250">
									</div>
									<div class="col-md-6">
									<label>Correo *</label>
									<input type="email" required="" class="form-control" name="correo" maxlength="250">
									<label>Dirección</label>	
									<input type="text"  id="venci" class="form-control date" name="Adress" maxlength="250">
									</div>
								  </div>
								</div>
							  </div>
							  </div>
							<div class="col-md-12">
							  <div class="card">
								<div class="card-header">
								  <h4>Datos Adicionales: </h4>
								</div>
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									  <label>Nit *</label>
										  <input type="text" required="" id="venci" class="form-control date" name="Nit" maxlength="250">
									  <label>Cámara de Comercio:</label>
										  <input type="file"  id="venci" class="form-control date" name="CámaraC">
									</div>
									<div class="col-md-6">
									  <label>Rut :</label>
										  <input type="file"  id="venci" class="form-control date" name="Rut">
									  <label>Certificado </label>
										  <input type="file"  id="venci" class="form-control date" name="Certificado">
									</div>
								  </div>
								</div>
							  </div>
							</div>
							<div class="col-md-12">
							  <center><input type="submit" class="btn btn-success" value="Registrar Proveedor"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  

						<!-- Modal -->			
			</th>
            <th align="center" valign="middle">
			@if($ver == "Inactivo")
			<a href="{{ url('Providers/index?ver=Activo') }}" class="btn btn-success" title="Ver proveedores activos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Activos</a>
			@else
			<a href="{{ url('Providers/index?ver=Inactivo') }}" class="btn btn-danger" title="Ver proveedores inactivos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Inactivos</a>			
			@endif
			</th>
        </tr>
    </thead>	
	</table>	
        </div>
		@include('alerts.validacion')
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>EMPRESA</th>
              <th>TELÉFONO</th>
              <th>CORREO</th>
              <th>DIRECCIÓN</th>
              <th>ESTADO</th>
              <th>NIT</th>			  
              <th>CÁMARA DE COMERCIO</th>
              <th>RUT</th>
              <th>CERTIFICADO</th>		
@if ($permisos[3] == '1')			  
              <th>Activar / Desactivar</th>		  
			  <th>Editar</th>
@endif			  
            </tr>
          </thead>
          <tbody>
          @foreach($usuarios as $usuario)
		  @if ($usuario->estado === $ver)
            <tr>
              <td>{{ ucwords($usuario->empresa) }}</td>
              <td>{{ $usuario->tel }}</td>
              <td>{{ $usuario->correo }}</td>
              <td>{{ $usuario->address }}</td>
              <td align="center" valign="middle">
			  @if( $usuario->estado === "Activo")
			  <span class="label label-success">
			  {{ $usuario->estado }}
			  </span>
			  @elseif( $usuario->estado === "Inactivo")
			  <span class="label label-danger">
			  {{ $usuario->estado }}
			  </span>
			  @endif
			  </td>
              <td>{{ $usuario->nit }}</td>				  
              <td align="center" valign="middle">
			  <a data-toggle="modal" data-target="#verC{{$usuario->id}}" class="btn-sm btn-primary" title="Ver el registro de la cámara de comercio {{ ucwords($usuario->empresa) }}" data-toggle="tooltip" data-placement="left">Ver</a>
				<div id="verC{{$usuario->id}}" class="modal fade" role="dialog">  
				<div class="modal-dialog">
					<div class="modal-content">      
						<div class="modal-header">        
							<button type="button" class="close" data-dismiss="modal">×</button>        
							<h4 class="modal-title">Cámara de Comercio</h4>      </div>      
						<div class="modal-body"><img src="{{ asset( $usuario->camara)}}" class="img-rounded" alt="Modal Pic" width="304" height="236" />   </div>      
						<div class="modal-footer">        
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>     
						</div>  
					</div>  
				</div>
			  </td>
              <td align="center" valign="middle">
			  <a data-toggle="modal" data-target="#verR{{$usuario->id}}" class="btn-sm btn-primary" title="Ver el registro del rut {{ ucwords($usuario->empresa) }}" data-toggle="tooltip" data-placement="left">Ver</a>
				<div id="verR{{$usuario->id}}" class="modal fade" role="dialog">  
				<div class="modal-dialog">
					<div class="modal-content">      
						<div class="modal-header">        
							<button type="button" class="close" data-dismiss="modal">×</button>        
							<h4 class="modal-title">Rut</h4>      </div>      
						<div class="modal-body"><img src="{{ asset( $usuario->rut)}}" class="img-rounded" alt="Modal Pic" width="304" height="236" />   </div>      
						<div class="modal-footer">        
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>     
						</div>  
					</div>  
				</div>			  
			  </td>	  
              <td align="center" valign="middle">
			  <a data-toggle="modal" data-target="#vercer{{$usuario->id}}" class="btn-sm btn-primary" title="Ver el registro del certificado {{ ucwords($usuario->empresa) }}">Ver</a>
				<div id="vercer{{$usuario->id}}" class="modal fade" role="dialog">  
				<div class="modal-dialog">
					<div class="modal-content">      
						<div class="modal-header">        
							<button type="button" class="close" data-dismiss="modal">×</button>        
							<h4 class="modal-title">Certificado</h4>      </div>      
						<div class="modal-body"><img src="{{ asset( $usuario->certificado)}}" class="img-rounded" alt="Modal Pic" width="304" height="236" />   </div>      
						<div class="modal-footer">        
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>     
						</div>  
					</div>  
				</div>				  
			  </td>	
@if ($permisos[3] == '1')
              <td align="center" valign="middle">
			  @if ($usuario->estado === "Activo")
			  <a href="{{ url('Providers/cambiarestado?id='.$usuario->id) }}" class="btn-sm btn-danger" title="Desactivar al proveedor {{ ucwords($usuario->empresa) }}" data-toggle="tooltip" data-placement="left">Desactivar</a>
				@else
			  <a href="{{ url('Providers/cambiarestado?id='.$usuario->id) }}" class="btn-sm btn-info" title="Activar al proveedor {{ ucwords($usuario->empresa) }}" data-toggle="tooltip" data-placement="left">Activar</a>			  
			  @endif
			  </td>
			  <td align="center" valign="middle">
			  <a href="{{ url('Providers/editar?id='.$usuario->id) }}" class="btn-sm btn-warning" title="Editar datos al proveedor {{ ucwords($usuario->empresa) }}" data-toggle="tooltip" data-placement="left">Editar</a>
			  </td>
@endif			  
            </tr>
			@endif			
          @endforeach
          </tbody>
        </table>
		</div> 
      </div>
    </div>
	
  </div>
@endif  
@include('layouts.dash.footer')
