@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[3] == '1')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <div class="row">
    <div class="col-md-12">    
      <form action="{{ url('Providers/update') }}" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">   
      <div class="card">
        <div class="card-header">
          <h2><center>Editar Proveedor  ({{ $usuarios[0]->empresa }}) </center></h2>
        </div>    
        @include('alerts.validacion')
        <div class="card-header">	
          <h4>Datos De Usuario: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
			<label>Empresa </label>
			<input type="text" id="venci" class="form-control date" name="Empresa" value="{{ $usuarios[0]->empresa }}" maxlength="250">
			<input type="hidden" class="form-control date"  value="{{ $usuarios[0]->id }}" name="Id" id="Id">			
			<label>Teléfono </label>
			<input type="text"  id="venci" class="form-control date" name="Teléfono" value="{{ $usuarios[0]->tel }}" maxlength="250">
            </div>
            <div class="col-md-6">
            <label>Correo *</label>
			<input type="email" required="" class="form-control" name="Correo" value="{{ $usuarios[0]->correo }}" maxlength="250">
			<label>Dirección</label>	
			<input type="text"  id="venci" class="form-control date" name="Address" value="{{ $usuarios[0]->address }}" maxlength="250">
            </div>
          </div>
        </div>
      </div>
    </div>		
	
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Datos Adicionales: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <label>Nit *</label>
                  <input type="text" required="" id="venci" class="form-control date" name="Nit" value="{{ $usuarios[0]->nit }}">
              <label>Cámara de Comercio:</label>
				<img src="{{ asset($usuarios[0]->camara) }}" width="50%">
				<input type="file"  id="venci" class="form-control date" name="CámaraC">
					<input type="hidden" id="CámaraC_ruta" name="CámaraC_ruta" value="{{ $usuarios[0]->camara }}">				  				  				
            </div>
            <div class="col-md-6">
              <label>Rut :</label>
				<img src="{{ asset($usuarios[0]->rut) }}" width="50%">			  
                  <input type="file"  id="venci" class="form-control date" name="Rut">	  
					<input type="hidden" id="Rut_ruta" name="Rut_ruta" value="{{ $usuarios[0]->rut }}">				  				  
              <label>Certificado </label>
				<img src="{{ asset($usuarios[0]->certificado) }}" width="50%">			  
<!--  							
					<div>
						<img  id="load_img" class="img-responsive" src="{{ asset($usuarios[0]->certificado) }}" alt="Logo">					
					</div>				
-->					
                  <input type="file"  id="Certificado" class="form-control date" name="Certificado" onchange="upload_imagea();">				  
					<input type="hidden" id="Certificado_ruta" name="Certificado_ruta" value="{{ $usuarios[0]->certificado }}">				  
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <center><input type="submit" class="btn btn-success" value="Actualizar Proveedor"></center>
		<center><a href="index" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>	  
    </div>		
      <input type="hidden" id="ruta" value="{{ asset('') }}">
	</div>
      </form>
    </div>
@endif	
  @include('layouts.dash.footer')

<script>  
function upload_image()
{
	var inputFileImage = document.getElementById("Certificado");
	var ruta = $("#ruta").val();
	var id = $("#Id").val();
	var file = inputFileImage.files[0];
	alert("entra");
	if( (typeof file === "object") && (file !== null) )
		{
			var data = new FormData();
			data.append('Certificado',file);
			$.ajax({
			url: ruta+"cargar",        // PHP PARA AÑADIR IMAGEN
			type: "GET",             // MÉTODO DE ENVIAR
			data: "id="+id+"&files="+data, 			  //MÉTODO DE ENVÍO DE DATOS
			contentType: false,       //EL TIPO DE CONTENIDO
			cache: false,             // DESHABILITAR EL CACHÉ EN LAS PÁGINAS
			processData:false,        // ENVIAR LOS PROCESOS DE DATOS
			success: function(data)   // QUÉ OCURRE CUANDO HAYA TERMINADO
			{$("#load_img").attr("src",data);}
					});	
		}
}
</script>