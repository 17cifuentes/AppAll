@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[1] == '1')
  <div class="row">
    <div class="col-md-12">
    <form method="POST" action="{{ url('usuario/update') }}" > 
    <input type="hidden" name="_token" value="{{ csrf_token() }}">     
      <div class="card">
        <div class="card-header">
          <h2><center>Editar Personal  ({{ $user[0]->name }}) </center></h2>
        </div>
        @include('alerts.validacion')
        <div class="card-header">
          <h4>Datos De Usuario: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <label>Nombre *</label>
              <input type="text" required="" class="form-control" name="name" value="{{ $user[0]->name }}" maxlength="250">  
				<input type="hidden" class="form-control date"  value="{{ $user[0]->id }}" name="Id">						  
            </div>
            <div class="col-md-6">
              <label>Correo *</label>
              <input type="email" name="email" class="form-control" value="{{ $user[0]->email }}" maxlength="250">
              <label>Cargo *</label>
              <select class="select2" name="rol" >
                <option value="{{ $user[0]->rol }}">{{ $user[0]->rol }}</option>
                <option value="R.Humanos">Recursos Humanos</option>
                <option value="Vendedor">Vendedor</option>
                <option value="Almacenista">Almacenista</option>
                <option value="Operario">Operario</option>
                <option value="Administrador">Administrador</option>
              </select>		  
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4>Datos Adicionales: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <label>Teléfono </label>
              <input type="text"  class="form-control" name="Teléfono" value="{{ $user[0]->tel }}" maxlength="250">
              <label>Dirección</label>
              <input type="text" name="address" class="form-control" value="{{ $user[0]->address }}" maxlength="250">
            </div>
            <div class="col-md-6">
              <label>Salario $</label>
              <input type="number"  name="Salario" class="form-control" value="{{ $user[0]->valor_a_pagar }}" >
              <label>Cuota</label>
              <input type="number"  name="Cuota" class="form-control" value="{{ $user[0]->cuota }}" >			  
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <center><input type="submit" class="btn btn-success" value="Actualizar Persona"></center>
		<center><a href="listar" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>
    </div>
  </form>
  </div>
</div>
@endif
@include('layouts.dash.footer')