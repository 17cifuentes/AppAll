@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[0] == '1')
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Gestión de Usuarios</th>
            <th align="center" valign="middle">
			<a href="#" data-toggle="modal" data-target="#ingresar" class="btn btn-info" title="Agregar Usuario" data-toggle="tooltip" data-placement="left"><span class="fa fa-plus"></span> <span class="fa fa-exchange"></span> Agregar Usuario</a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="ingresar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<form method="POST" action="{{ url('usuario/postregistrar') }}" > 
								<input type="hidden" name="_token" value="{{ csrf_token() }}">     
								<input type="hidden" name="user_estado" value="Activo"  > 
							  <div class="col-md-12">
							  <div class="card">
								<div class="card-header">
								  <h4>Datos De Usuario: </h4>
								</div>
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									  <label>Nombre *</label>
									  <input type="text" required="" class="form-control" name="name" maxlength="250">  
									  <label>Contraseña *</label>
									  <input type="password" required="" name="password" class="form-control" maxlength="250">  
									</div>
									<div class="col-md-6">
									  <label>Correo *</label>
									  <input type="email" name="email" class="form-control" maxlength="250">
									  <br><br>
									  <label>Cargo *</label>
									  <select class="select" name="rol">
										<option value="">Selección</option>
										<option value="R.Humanos">Recursos Humanos</option>
										<option value="Vendedor">Vendedor</option>
										<option value="Almacenista">Almacenista</option>
										<option value="Operario">Operario</option>
										<option value="Administrador">Administrador</option>
									  </select>		  
									</div>
								  </div>
								</div>
							  </div>
							  </div>
							<div class="col-md-12">
							  <div class="card">
								<div class="card-header">
								  <h4>Datos Adicionales: </h4>
								</div>
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									  <label>Teléfono </label>
									  <input type="text"  class="form-control" name="Teléfono" maxlength="250">  
									  <label>Dirección</label>
									  <input type="text" name="address" class="form-control" maxlength="250">  
									</div>
									<div class="col-md-6">
									  <label>Salario $</label>
									  <input type="number"  name="Salario" class="form-control" value="0">
										<label>Cuota</label>
									  <input type="number"  name="Cuota" class="form-control" value="0">									  
									</div>
								  </div>
								</div>
							  </div>
							</div>
							<div class="col-md-12">
							  <center><input type="submit" class="btn btn-success" value="Registrar Usuario"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  

						<!-- Modal -->
			</th>
            <th align="center" valign="middle">
			@if($ver == "Inactivo")
			<a href="{{ url('usuario/listar?ver=Activo') }}" class="btn btn-success" title="Ver usuarios activos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Activos</a>
			@else
			<a href="{{ url('usuario/listar?ver=Inactivo') }}" class="btn btn-danger" title="Ver usuarios inactivos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"></span> Ver Inactivos</a>			
			@endif
			</th>
        </tr>
    </thead>	
	</table>	
        </div>
        @include('alerts.validacion')
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Cargo</th>
            <th>Estado</th>
@if ($permisos[1] == '1')
            <th>Activar / Desactivar</th>
            <th>Editar</th>
@endif
            <th>Fotografía</th>			
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
		@if ($user->user_estado === $ver && $user->rol != "Cliente" )
        <tr>
            <td>{{ ucwords($user->name) }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->rol }}</td>
            <td>
			@if ($user->user_estado == "Activo")
			<span class="label label-success">
			{{ $user->user_estado }}
			</span>	
			@elseif( $user->user_estado == "Inactivo")
				  <span class="label label-danger">
			{{ $user->user_estado }}
				  </span>			  
			@endif			
			</td>
@if ($permisos[1] == '1')			
            <td align="center" valign="middle">
			@if ($user->user_estado === "Activo")			
			<a href="{{ url('usuario/cambiarestado?id='.$user->id) }}" class="btn-sm btn-danger" title="Desactivar el usuario {{ ucwords($user->name) }}" data-toggle="tooltip" data-placement="left">Desactivar</a>
			@else
			<a href="{{ url('usuario/cambiarestado?id='.$user->id) }}" class="btn-sm btn-info" title="Activar el usuario {{ ucwords($user->name) }}" data-toggle="tooltip" data-placement="left" >Activar</a>
			@endif
			</td>
			
            <td><a href="{{ url('usuario/editar?id='.$user->id) }}" class="btn-sm btn-warning" title="Editar el registro del usuario {{ ucwords($user->name) }}" data-toggle="tooltip" data-placement="left">Editar</a></td>			
@endif			
            <td>
			<a data-toggle="modal" data-target="#ver{{$user->id}}" class="btn-sm btn-primary" title="Ver fotografía de {{ ucwords($user->name) }}" data-toggle="tooltip" data-placement="left">Ver</a>
				<div id="ver{{$user->id}}" class="modal fade" role="dialog">  
				<div class="modal-dialog">
					<div class="modal-content">      
						<div class="modal-header">        
							<button type="button" class="close" data-dismiss="modal">×</button>        
							<h4 class="modal-title">Fotografía {{ ucwords($user->name) }}</h4>      </div>      
						<div class="modal-body"><center><img src="{{ asset( $user->pic)}}" class="img-rounded" alt="Modal Pic" width="304" height="236" /> </center>  </div>      
						<div class="modal-footer">        
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>     
						</div>  
					</div>  
				</div>
			</td>			
			
        </tr>
		@endif
        @endforeach
    </tbody>
</table>
        </div>
      </div>
    </div>
@endif
@include('layouts.dash.footer')

