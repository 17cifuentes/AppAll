<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>PDF</title>

    <style>

        .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #E84E0F;
  text-decoration: none;
}

body {
  position: relative;
  width: 18cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #555555;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 14px; 
  font-family: SourceSansPro;
}

header {
  width: 100%;
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

#logo {
  float: left;
  margin-top: 8px;
}

/*#logo img {
  height: 70px;
}*/

#company {
  float: right;
  text-align: right;
}


#details {
  margin-bottom: 50px;
}

#client {
  padding-left: 6px;
  border-left: 6px solid #E84E0F;
  float: left;
}

#client .name{
  text-transform: none;
}


#invoice {
  float: right;
  text-align: right;
}

#client .to {
  color: #777777;
}

h2.name {
  color: #E84E0F;
  font-size: 1.4em;
  font-weight: bold;
  margin: 0;
}

h2.client {
  color: #173e43;
  font-size: 1.0em;
  font-weight: bold;
  margin: 0;
  width:250px;
}



#invoice h3 {
  color: #E84E0F;
  font-size: 1.4em;
  line-height: 1em;
  font-weight: normal;
  margin: 0  0 10px 0;
}

#invoice .date {
  font-size: 1.1em;
  color: #777777;
}



table td h3{
  color: #E84E0F;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}

table .no {
  /*color: #FFFFFF;*/
  font-size: 1.6em;
  background: #DDDDDD;
  text-align: center;
}

table .desc1 {
  margin-top: 100em;
  text-align: left;
  background: #DDDDDD;
}

table .desc {
  text-align: left;
}

table .unit {
  background: #DDDDDD;
  text-align: center;
}

table .qty {
  text-align: center;
}

table .total {
  background: #DDDDDD;
  text-align: center;
  /*color: #FFFFFF;*/
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table tbody tr:last-child td {
  border: none;
}

table tfoot td {
  padding: 10px 20px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1.2em;
  white-space: nowrap; 
  border-top: 1px solid #AAAAAA; 
}

table tfoot td.totalfact {
  text-align: center;
}

table tfoot tr:first-child td {
  border-top: none; 
}

table tfoot tr:last-child td {
  color: #E84E0F;
  font-size: 1.4em;
  border-top: 1px solid #E84E0F; 

}

table tfoot tr td:first-child {
  border: none;
}

#thanks{
  font-size: 1.5em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #E84E0F;  
  border-right: 6px solid #E84E0F;  
  font-size: 1.5em;
}

/*#notices .notice {
  font-size: 0.8em;
}*/

#notices .referido {
  text-transform: uppercase;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}

        
          /*background: url('../../public/images/dimension.png'); */
        
		
    </style>  
</head>
<body>
   

    <header class="clearfix">

    <table border="1" cellspacing="0" style="width: 100%;">
        <tr>
            <td style="width: 25%; color: #444444;">
                <img height="10%" src="{{ $info[0]->pic}}" alt="Logo"><br>
            </td>		
			<td style="width: 25%;text-align:right;font-weight:bold;font-size:20px">
			PEDIDO # {{ str_pad( $facturas[0]->num_pedido , 4, "0", STR_PAD_LEFT )}}
			</td>
			<td style="width: 50%; color: #34495e;font-size:12px;text-align:center">
                <span style="color: #34495e;font-size:14px;font-weight:bold">
				{{ ucwords($info[0]->nombre) }}
				</span>
				<br>
				{{ ucwords($info[0]->address) }}
				<br>
				{{ ucwords($info[0]->ciudad.' , '.$info[0]->paiis.' - '.$info[0]->cod_postal) }}
				<br> 
				Teléfono: {{ $info[0]->tel }}<br>
				Correo Electrónico: {{ $info[0]->correo }}
            </td>
        </tr>
    </table>
	
    </header>

    <main><br>

      <div style="margin-top: -40px" id="details">
      <center><h3  class="name">DOCUMENTO PROVISIONAL</h3></center>

    <table cellspacing="0" style="width: 100%;">
        <tr>
            <td style="width: 20%; ">
            </td>	
			<td style="width: 20%; ">
			</td>
			<td style="width: 20%; font-size:16px;text-align:center">
			PEDIDO PARA:
			</td>
			<td style="width: 20%; ">
            </td>
			<td style="width: 20%; ">
            </td>			
        </tr>
        <tr>
			</td>
			<td style="width: 20%; ">
            </td>		
            <td style="width: 20%; ">
            </td>
			<td style="width: 20%; font-size:12px;text-align:left">
			Nombre:
			</td>			
			<td style="width: 20%; font-size:12px;text-align:center">
			{{ ucwords($facturas[0]->nombre_clientes) }}
			</td>
			<td style="width: 20%; ">
            </td>
        </tr>
        <tr>
			</td>
			<td style="width: 20%; ">
            </td>		
            <td style="width: 20%; ">
            </td>
			<td style="width: 20%; font-size:12px;text-align:left">
			Teléfono:
			</td>			
			<td style="width: 20%; font-size:12px;text-align:center">
			{{ $facturas[0]->tel }}
			</td>
			<td style="width: 20%; ">
            </td>
        </tr>
        <tr>
			</td>
			<td style="width: 20%; ">
            </td>		
            <td style="width: 20%; ">
            </td>
			<td style="width: 20%; font-size:12px;text-align:left">
			Dirección de Correo:
			</td>			
			<td style="width: 20%; font-size:12px;text-align:center">
			{{ $facturas[0]->correo_cliente }}
			</td>
			<td style="width: 20%; ">
            </td>
        </tr>
        <tr>
			</td>
			<td style="width: 20%; ">
            </td>		
            <td style="width: 20%; ">
            </td>
			<td style="width: 20%; font-size:12px;text-align:left">
			Dirección:
			</td>			
			<td style="width: 20%; font-size:12px;text-align:center">
			{{ $facturas[0]->address }}
			</td>
			<td style="width: 20%; ">
            </td>
        </tr>

		
    </table>
<br>
	
<br>
	  
    <table border="1" cellspacing="0" style="width: 100%; text-align: left; font-size: 10pt;">
        <tr>
            <th style="width: 40%;text-align:center" class='midnight-blue' bgcolor="#cccccc">DESCRIPCIÓN</th>
            <th style="width: 15%;text-align:center" class='midnight-blue' bgcolor="#cccccc">CANTIDAD</th>			
            <th style="width: 15%;text-align: CENTER" class='midnight-blue' bgcolor="#cccccc">PRECIO UNITARIO</th>
            <th style="width: 15%;text-align: CENTER" class='midnight-blue' bgcolor="#cccccc">IMPUESTO</th>			
            <th style="width: 15%;text-align: CENTER" class='midnight-blue' bgcolor="#cccccc">PRECIO TOTAL</th>
            
        </tr>	
<?php
$total_iva= 0;
$sumador_total=0;
?>
		@foreach($cont_factura as $elemento)
		<?php
		$precio_venta= $elemento->precio_venta;
		$cantidad= $elemento->cantidad;
		$precio_total=$precio_venta*$cantidad;
		$impuesto= $elemento->impuesto;
		//$total_iva_T= ($precio_total-($precio_total/(($impuesto/100)+1)));
		$total_iva= $total_iva + round(($precio_venta * $cantidad)-($precio_venta * $cantidad) / ($impuesto/100+1),0);
		$sumador_total+=$precio_total;
		?>		
        <tr>
            <td class=' ' style="width: 40%; text-align: CENTER">{{ ucwords($elemento->art_nombre) }} </td>
            <td class=' ' style="width: 15%; text-align: center">{{ $elemento->cantidad }}</td>
            <td class=' ' style="width: 15%; text-align: right">$ {{ number_format($elemento->precio_venta,0) }}</td>
            <td class=' ' style="width: 15%; text-align: right">{{ $elemento->impuesto }} %</td>
            <td class=' ' style="width: 15%; text-align: right">$ {{ number_format($elemento->cantidad * $elemento->precio_venta,0)}}</td>
        </tr>
		@endforeach
		

        <tr>
            <td colspan="4" style="widtd: 85%; text-align: CENTER;" ><?php echo "-";?> </td>
            <td style="widtd: 15%; text-align: right;" > <?php echo "";?></td>
        </tr>	  
        <tr>
            <td colspan="4" style="widtd: 85%; text-align: CENTER;" bgcolor="#cccccc">SUBTOTAL $ </td>
            <td style="widtd: 15%; text-align: right;"> <?php echo number_format($sumador_total-$total_iva,0);?> </td>
        </tr>
		<tr>
            <td colspan="4" style="widtd: 85%; text-align: CENTER;" bgcolor="#cccccc">IMPUESTO $ </td>
            <td style="widtd: 15%; text-align: right;"> <?php echo number_format($total_iva,0);?> </td>
        </tr>
		<tr>
            <td colspan="4" style="widtd: 85%; text-align: CENTER;"> </td>
            <td style="widtd: 15%; text-align: right;">  </td>
        </tr>
    </table>		
	
<br>	

    <table border="1" cellspacing="0" style="width: 100%; text-align: left; font-size: 20pt;">
		<tr>
            <td colspan="4" style="widtd: 85%; text-align: CENTER;">TOTAL $ </td>
            <td style="widtd: 15%; text-align: right;"> <?php echo number_format($sumador_total,0);?> </td>
        </tr>
    </table>
	  
<br>	  

<br>
		<center>
      <div id="thanks" >"El cliente satisfecho traerá nuevas ventas" ~James Penney</div></center>
      
    </main>

</body>
</html> 