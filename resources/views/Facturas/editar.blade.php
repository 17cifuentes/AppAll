@include('layouts.dash.header')
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- Jquery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Datepicker Files -->
    <script src="{{asset('datePicker/js/bootstrap-datepicker.js')}}"></script>
    <!-- Languaje -->
    <script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>
<style>
    input[type=number] {
       width: 70px;
    }
	
.col-centered{
    float: none;
    margin: 0 auto;
}	
</style>
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[20] == '1')
  <div class="row">
    <div class="col-md-12">    
	<input type="hidden" name="_token" value="{{ csrf_token() }}">   
      <div class="card">
        <div class="card-header">
          <h2><center>Editar Factura {{ str_pad($Factura[0]->num_factura, 4, "0", STR_PAD_LEFT ) }} </center></h2>
        </div>    
        @include('alerts.validacion')
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
			<label>Número Factura </label>
			<input type="text" readonly id="venci" class="form-control date" name="Num" value="{{ str_pad($Factura[0]->num_factura, 4, '0', STR_PAD_LEFT ) }}">
			<input type="hidden" readonly id="Id" class="form-control date" name="Id" value="{{ $Factura[0]->id_facturas }}">
            <label>Clientes *</label>
			<input type="text" readonly id="venci" class="form-control date" name="Num" value="{{ $Factura[0]->nombre_clientes }}">
            <label>Vendedor *</label>
			<input type="text" readonly id="venci" class="form-control date" name="Num" value="{{ $Factura[0]->nombre }}">
            </div>
            <div class="col-md-6">
			<label>Tipo de Pago</label>	
			<input type="text" readonly id="venci" class="form-control date" name="Num" value="{{ $Factura[0]->tipo_pago }}">
			<label>Fecha</label>	
			<input type="text" id="Fecha" class="form-control datepicker" name="Fecha" value="<?php echo date('d/m/Y'); ?>">
			<button type="button" class="btn btn-success " data-toggle="modal" data-target="#myModal">
				<span class="fa fa-search"></span> Agregar productos
			</button>			
            </div>		  
          </div>
        </div>
      </div>
    </div>		
	
			<!-- ModalBODY -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Buscar Artículos</h4>
					</div>
		<table class="datatable table table-striped primary">
		<thead>
            <tr>
              <th>Artículo</th>	  
              <th>Precio</th>
			  <th>Medida</th>
              <th>Cantidad</th>				  
			  <th>Agregar</th>
            </tr>
		</thead>
		<tbody>
				@foreach($artículos as $objeto)
				<tr>				
				<td>{{ $objeto->art_nombre }}</td>
				<td>
				<input type="text" readonly size="4" id="Precio{{ $objeto->id }}" name="Precio{{ $objeto->id }}" value="{{ $objeto->precio_unitario }}">				
				</td>
				<td>{{ $objeto->medida }}</td>
				<td>
				<input type="number" required="" id="Cantidad{{ $objeto->id }}" name="Cantidad{{ $objeto->id }}" value="0">
				</td>
				<td class='text-center'>
				<a class='btn btn-info'href="#" onclick="agregar({{ $objeto->id }},Cantidad{{ $objeto->id }},Precio{{ $objeto->id }})"><i class="glyphicon glyphicon-plus"></i></a>
				</td>				
				</tr>
				@endforeach			
		</tbody>
        </table>
					  
					</div>
				  </div>
				</div>			
			<!-- Modal -->
	


    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
		<table class="table table-striped primary">
		<thead>
            <tr>
			<th> </th>			
              <th>Artículo</th>	  
              <th>Cantidad</th>
			  <th>Precio Unitario</th>
              <th>Impuesto</th>				  
			  <th>Precio Total</th>
            </tr>
		</thead>
		<tbody>
				<?php $subtotal= 0; $impuestos= 0; $totalventa= 0; ?>
				@foreach($detalle as $objeto)
				<tr>				
				<td class='text-center'>
				<a href="eliminardetalledeeditar?Id={{ $objeto->id_detalle }}&num={{ $Factura[0]->id_facturas }}&precio={{ $objeto->precio_venta * $objeto->cantidad}}" onclick="">
				<i class="fa fa-trash"></i></a>
				</td>
				<td>{{ $objeto->art_nombre }}</td>
				<td>{{ $objeto->cantidad }}</td>
				<td>{{ $objeto->precio_venta }}</td>
				<td align="right">
				<?php $impuestos= $impuestos + round(($objeto->precio_venta * $objeto->cantidad) - ($objeto->precio_venta * $objeto->cantidad)/($objeto->impuesto/100+1),0);?>
				{{ round(($objeto->precio_venta * $objeto->cantidad)-($objeto->precio_venta * $objeto->cantidad) / ($objeto->impuesto/100+1),0) }}
				</td>
				<td align="right">
				<?php $totalventa= $totalventa + $objeto->precio_venta * $objeto->cantidad; ?>
				{{ $objeto->precio_venta * $objeto->cantidad}}</td>				
				</tr>
				@endforeach	
				<?php $subtotal= $totalventa - $impuestos; ?>
		</tbody>
        </table>

		<table class="table table-striped primary" >
		<thead>
            <tr>
			  <th> </th>			
              <th> </th>	  
              <th> </th>
			  <th> </th>
              <th> </th>				  
			  <th> </th>
            </tr>
		</thead>
		<tbody>
				<tr> 
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td align="right">Subtotal $ </td>
				<td align="right"> <?php echo $subtotal; ?> </td>
				</tr>
	
				<tr> 
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td align="right">Impuestos $ </td>
				<td align="right"> <?php echo $impuestos; ?> </td>
				</tr>

				<tr> 
				<td> </td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td align="right">Total a Pagar $ </td>
				<td align="right"> 
				<?php echo $totalventa; ?> 
				<input type="hidden" readonly id="TVenta" class="form-control date" name="TVenta" value="<?php echo $totalventa; ?> ">
				</td>
				</tr>				
		</tbody>
        </table>		
		
        </div>
      </div>
    </div>    
	
    <div class="col-md-12">
	  	<center><a href="index" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>
    </div>
	
	</div>
      </div>
    </div>
<script>
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true
    });
	
function agregar(Id, Cantidad, Precio){
var nuumero = $("#Id").val();
//alert(nuumero);

window.location.href = "agregardeeditar?Id="+ Id + "&Cantidad=" + Cantidad.value + "&Precio=" + Precio.value + "&num=" + nuumero;
	}
</script>
	
@endif	
  @include('layouts.dash.footer')
