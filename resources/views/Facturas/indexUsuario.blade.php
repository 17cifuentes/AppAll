@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[18] == '1')
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Facturación</th>
            <th align="center" valign="middle">
@if ($permisos[19] == '1')			
			<a href="{{ url('Factura/registrar') }}" class="btn btn-success" title="Ver usuarios activos"><span class="fa fa-shopping-cart"></span> <span class="fa fa-exchange"> Nueva Factura</a>
@endif			
			</th>
            <th align="center" valign="middle">
@if ($permisos[22] == '1')				
			<a target="_blank" href="{{ url('Factura/facturero') }}" class="btn btn-danger" title="Ver Facturero"><span class="fa fa-eye"></span> <span class="fa fa-exchange"> Facturero</a>
@endif			
			</th>			
        </tr>
    </thead>	
	</table>	
        </div>
		@include('alerts.validacion')
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
			  <th># Número</th>			
              <th>Fecha</th>
              <th>Cliente</th>
              <th>Vendedor</th>
			  <th>Ruta</th>
              <th>Pago</th>
              <th>Total Efectuado</th>
			  <th>Estado</th>		
@if ($permisos[21] == '1')				  
              <th>Cambiar Estado</th>
@endif			  
@if ($permisos[20] == '1')				  
			  <th>Editar</th>
@endif			  
            </tr>
          </thead>
          <tbody>
          @foreach($clientes as $usuario)
		  @if ($usuario->id == Auth::user()->id ||  Auth::user()->rol == "Administrador")		  
            <tr>
			  <td class='text-center'>
			  <a href="pdf?num_factura={{ $usuario->num_factura }}&ID=0" onclick="" title="Descargar Factura" data-toggle="tooltip" data-placement="right"><i class="fa fa-download"></i></a>
			  <a href="pdf?num_factura={{ $usuario->num_factura }}&ID=1" target="_blank" class="btn-sm btn-info">Ver#{{ str_pad( $usuario->num_factura, 4, "0", STR_PAD_LEFT ) }}</a> 
			  </td>			
              <td>{{ $usuario->fecha }}</td>
              <td>
			  <a title="Correo: {{ ($usuario->correo_cliente) }}" data-toggle="tooltip" data-placement="left" href="mailto:
		{{ ($usuario->correo_cliente) }}
		?subject=
		Factura%20de%20Compra
		&body=
		{{ ucwords($usuario->nombre_clientes) }}, la siguiente es la factura de su compra
		">			  
			  {{ ucwords($usuario->nombre_clientes) }}
			  </a>
			  </td>
              <td>{{ ucwords($usuario->nombre) }}</td>
			  <td>{{ ucwords($usuario->ciudad) }}</td>
              <td>
			  @if( $usuario->tipo_pago === "Efectivo")
				  <span class="label label-success">
				  {{ $usuario->tipo_pago }}
				  </span>
			  @elseif( $usuario->tipo_pago != "Efectivo")
				  <span class="label label-warning">
				  {{ $usuario->tipo_pago }}
				  </span>			  
			  @endif
			  </td>
			  <td align= "right">
			  @if( $usuario->tipo_pago === "Efectivo")
				  <span class="label label-success">
				  $ {{ number_format($usuario->total_venta,0) }}
				  </span>
			  @elseif( $usuario->tipo_pago != "Efectivo")
					@if( $usuario->total_venta == 0)
					  <span class="label label-success">
					  $ {{ number_format($usuario->total_venta,0) }}
					  </span>
					@else
					  <span class="label label-warning">
					  $ {{ number_format($usuario->total_venta,0) }}
					  </span>					  
					@endif
			  @endif			  
			  
			  </td>
              <td align="center" valign="middle">
			  @if( $usuario->estado_factura === "Pendiente")
				  <!--<span class="label label-danger">{{ $usuario->estado_factura }}</span>-->
				<span class="badge badge-warning badge-icon"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Pendiente</span></span>				  
			  @elseif( $usuario->estado_factura === "Pagado")
				  <!--<span class="label label-success">{{ $usuario->estado_factura }}</span>-->
				  <span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span>Pagado</span></span>
			  @elseif( $usuario->estado_factura === "Entregado")
				  <!--<span class="label label-info">{{ $usuario->estado_factura }}</span>-->
				  <span class="badge badge-primary badge-icon"><i class="fa fa-truck" aria-hidden="true"></i><span>Entregado</span></span>
			  @elseif( $usuario->estado_factura === "Cuota")
				  <span class="label label-warning">
				  {{ $usuario->estado_factura }}
				  </span>
			  @endif			  
			  </td>			
@if ($permisos[21] == '1')				  
              <td align="center" valign="middle">
				@if ($usuario->tipo_pago === "Efectivo")
					@if ($usuario->estado_factura === "Pendiente")
						<a href="{{ url('Factura/cambiarestado?id='.$usuario->id_facturas) }}" class="btn-sm btn-success" title="La factura #{{ $usuario->num_factura }} ha sido pagada" data-toggle="tooltip" data-placement="left">Pagado</a>
					@elseif ($usuario->estado_factura === "Pagado")
						<a href="{{ url('Factura/cambiarestado?id='.$usuario->id_facturas) }}" class="btn-sm btn-info" title="La factura #{{ $usuario->num_factura }} va a ser entregada" data-toggle="tooltip" data-placement="left">Entregar</a>			  
					@endif
				@elseif( $usuario->tipo_pago != "Efectivo")				  
					@if ($usuario->estado_factura === "Pendiente")
						<a href="{{ url('Factura/cambiarestado?id='.$usuario->id_facturas) }}" class="btn-sm btn-info" title="La factura #{{ $usuario->num_factura }} va a ser entregada, es pagada como crédito" data-toggle="tooltip" data-placement="left">Entregar</a>
					@elseif (($usuario->estado_factura === "Entregado"  || $usuario->estado_factura === "Cuota") && $usuario->total_venta != 0)
						<!-- Modal -->			
						<a type="button" class="btn-sm btn-warning " data-toggle="modal" data-target="#cuota{{$usuario->id_facturas}}" title="Registrar en la factura #{{ $usuario->num_factura }} pago de cuota crédito" data-toggle="tooltip" data-placement="left">Cuota</a>
						<!-- ModalBODY -->
							<div class="modal fade" id="cuota{{$usuario->id_facturas}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<h4 class="modal-title" id="myModalLabel">Pagar cuota del crédito</h4>
								  </div>
							  <form action="{{ url('Factura/cuota') }}" method="POST">		  
								<input type="hidden" name="_token" value="{{ csrf_token() }}"> 
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									<label>Adeudado </label>
									<input readonly type="text" readonly id="venci" class="form-control date" name="Adeudado" value="{{ $usuario->total_venta }}" >
									<input type="hidden" class="form-control date"  value="{{$usuario->id_facturas}}" name="Id">           
									</div>
									<div class="col-md-6">
									<label>A pagar</label>	
									<input type="number"  required="" id="Cantidad" class="form-control date" name="Cantidad" value= "0">
									</div>
								  </div>
								</div>			
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
									<button type="submit" class="btn btn-primary">Registrar</button> 
								  </div>
							</form>		  
								</div>
							  </div>
							</div>			
						<!-- Modal {{ url('Factura/ver?id='.$usuario->id_facturas) }} -->
			
					@endif
				@endif
			  </td>
@endif			  
@if ($permisos[20] == '1')				  			  
			  <td>
			  @if ($usuario->estado_factura === "Pendiente")
			  <a href="{{ url('Factura/editar?id='.$usuario->id_facturas) }}" class="btn-sm btn-primary" title="Editar datos de la factura #{{ $usuario->num_factura }}" data-toggle="tooltip" data-placement="left">Editar</a>
			  @endif
			  </td>
@endif			  			  
            </tr>
			@endif
			@endforeach
          </tbody>
        </table>
		</div> 
      </div>
    </div>
	
  </div>
@endif  
@include('layouts.dash.footer')
