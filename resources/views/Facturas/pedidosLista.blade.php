@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[24] == '1')
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Pedidos</th>
            <th align="center" valign="middle">
			</th>
            <th align="center" valign="middle">
			</th>			
        </tr>
    </thead>	
	</table>	
        </div>
		@include('alerts.validacion')
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
			  <th># Número</th>			
              <th>Fecha</th>
              <th>Cliente</th>
              <th>Tipo de Pago</th>
              <th>Total Efectuado</th>
			  <th>Estado</th>		
@if ($permisos[25] == '1')				  
              <th>Cambiar Estado</th>
@endif			   
            </tr>
          </thead>
          <tbody>
          @foreach($clientes as $usuario)
		  @if ($usuario->id_cliente == Auth::user()->cuota ||  Auth::user()->rol == "Administrador")
            <tr>
			  <td class='text-center'>
			  <a href="pdfpedido?num_pedido={{ $usuario->num_pedido }}&ID=1" target="_blank" class="btn-sm btn-info">Ver#{{ str_pad( $usuario->num_pedido, 4, "0", STR_PAD_LEFT ) }}</a> 
			  </td>			
              <td>{{ $usuario->fecha }}</td>
              <td>
			  <a title="Correo: {{ ($usuario->correo_cliente) }}" data-toggle="tooltip" data-placement="left" href="mailto:
		{{ ($usuario->correo_cliente) }}
		?subject=
		Factura%20de%20Compra
		&body=
		{{ ucwords($usuario->nombre_clientes) }}, la siguiente es la factura de su compra
		">			  
			  {{ ucwords($usuario->nombre_clientes) }}
			  </a>
			  </td>
              <td>
			  @if( $usuario->tipo_pago === "Efectivo")
				  <span class="label label-success">
				  {{ $usuario->tipo_pago }}
				  </span>
			  @elseif( $usuario->tipo_pago != "Efectivo")
				  <span class="label label-warning">
				  {{ $usuario->tipo_pago }}
				  </span>			  
			  @endif
			  </td>
			  <td align= "right">
			  @if( $usuario->tipo_pago === "Efectivo")
				  <span class="label label-success">
				  $ {{ number_format($usuario->total_venta,0) }}
				  </span>
			  @elseif( $usuario->tipo_pago != "Efectivo")
					@if( $usuario->total_venta == 0)
					  <span class="label label-success">
					  $ {{ number_format($usuario->total_venta,0) }}
					  </span>
					@else
					  <span class="label label-warning">
					  $ {{ number_format($usuario->total_venta,0) }}
					  </span>					  
					@endif
			  @endif			  
			  
			  </td>
              <td align="center" valign="middle">
			  @if( $usuario->estado_factura === "Pendiente")
				  <!--<span class="label label-danger">{{ $usuario->estado_factura }}</span>-->
				<span class="badge badge-warning badge-icon"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Pendiente</span></span>				  
			  @elseif( $usuario->estado_factura === "Revisado")
				  <!--<span class="label label-success">{{ $usuario->estado_factura }}</span>-->
				  <span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span>Revisado</span></span>
			  @elseif( $usuario->estado_factura === "Aprobado")
				  <!--<span class="label label-info">{{ $usuario->estado_factura }}</span>-->
				  <span class="badge badge-primary badge-icon"><i class="fa fa-truck" aria-hidden="true"></i><span>Aprobado</span></span>
			  @endif			  
			  </td>			
@if ($permisos[25] == '1')				  
              <td align="center" valign="middle">
					@if ($usuario->estado_factura === "Pendiente")
						<a href="{{ url('Factura/cambiarestadopedido?id='.$usuario->id_pedidos) }}" class="btn-sm btn-success" title="El pedido #{{ $usuario->num_factura }} ha sido revisada" data-toggle="tooltip" data-placement="left">Revisado</a>
					@elseif ($usuario->estado_factura === "Revisado")
						<a href="{{ url('Factura/cambiarestadopedido?id='.$usuario->id_pedidos) }}" class="btn-sm btn-info" title="El pedido #{{ $usuario->num_factura }} fue aprobada" data-toggle="tooltip" data-placement="left">Aprobado</a>			  
					@endif
			  </td>
@endif			    			  
            </tr>
			@endif
			@endforeach
          </tbody>
        </table>
		</div> 
      </div>
    </div>
	
  </div>
@endif  
@include('layouts.dash.footer')
