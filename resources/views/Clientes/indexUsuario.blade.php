@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[12] == '1')
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Gestión de Clientes</th>
            <th align="center" valign="middle">
			<a href="#" data-toggle="modal" data-target="#ingresar" class="btn btn-info" title="Agregar Cliente"><span class="fa fa-plus"></span> <span class="fa fa-exchange"> Agregar Cliente</a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="ingresar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						  <form action="{{ url('Cliente/postregistro') }}" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
							  <div class="col-md-12">
						  <div class="card">
							<div class="card-header">
							  <h2>Registro De Cliente</h2>
							</div>
							<div class="card-header">
							  <h4>Datos del Cliente: </h4>
							</div>
							<div class="card-body">
							  <div class="row">
								<div class="col-md-6">
								<label>Nombre </label>
								<input type="text" id="venci" class="form-control date" name="Nombre">
								<label>Teléfono </label>
								<input type="text"  id="venci" class="form-control date" name="Teléfono">
								</div>
								<div class="col-md-6">
								<label>Correo *</label>
								<input type="email" required="" class="form-control" name="Correo" placeholder="">
								<label>Dirección</label>	
								<input type="text"  id="venci" class="form-control date" name="Address">
								<label>Vendedor *</label>
								  <select required="" class="select" name="PersonalID">
									@foreach ($vendedores as $usuario)
									@if ($usuario->rol == "Vendedor")
									<option value="{{ $usuario->id }}"> {{ $usuario->nombre }} - {{ $usuario->correo }} - {{ $usuario->cargo }} </option>
									@endif
									@endforeach
								  </select>				
								</div>
							  </div>
							</div>
						  </div>
							  </div>

							<div class="col-md-12">
							  <center><input type="submit" class="btn btn-success" value="Registrar Cliente"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  

						<!-- Modal -->			
			</th>
            <th align="center" valign="middle">
			@if($ver == "Inactivo")
			<a href="{{ url('Cliente/index?ver=Activo') }}" class="btn btn-success" title="Ver usuarios activos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"> Ver Activos</a>
			@else
			<a href="{{ url('Cliente/index?ver=Inactivo') }}" class="btn btn-danger" title="Ver usuarios inactivos" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"> Ver Inactivos</a>			
			@endif
			</th>
        </tr>
    </thead>	
	</table>	
        </div>
		@include('alerts.validacion')
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>NOMBRE</th>
              <th>TELÉFONO</th>
              <th>CORREO</th>
              <th>DIRECCIÓN</th>
			  <th>Vendedor Asignado</th>
              <th>ESTADO</th>
@if ($permisos[13] == '1')			  
              <th>Activar / Desactivar</th>		  
			  <th>Editar</th>
@endif			  
            </tr>
          </thead>
          <tbody>
          @foreach($usuarios as $usuario)
		  @if ($usuario->estado === $ver)
            <tr>
              <td>{{ ucwords($usuario->nombre_clientes) }}</td>
              <td>{{ $usuario->tel }}</td>
              <td>{{ $usuario->correo_cliente }}</td>
              <td>{{ $usuario->address }}</td>
			  <td>{{ $usuario->nombre }}</td>
              <td align="center" valign="middle">
			  @if( $usuario->estado === "Activo")
			  <span class="label label-success">
			  {{ $usuario->estado }}
			  </span>
			  @elseif( $usuario->estado === "Inactivo")
			  <span class="label label-danger">
			  {{ $usuario->estado }}
			  </span>
			  @endif			  
			  </td>  
@if ($permisos[13] == '1')				  
              <td align="center" valign="middle">
			  @if ($usuario->estado === "Activo")
			  <a href="{{ url('Cliente/cambiarestado?id='.$usuario->id) }}" class="btn-sm btn-danger" title="Desactivar el cliente {{ ucwords($usuario->nombre_clientes) }}" data-toggle="tooltip" data-placement="left">Desactivar</a>
				@else
			  <a href="{{ url('Cliente/cambiarestado?id='.$usuario->id) }}" class="btn-sm btn-info" title="Activar el cliente {{ ucwords($usuario->nombre_clientes) }}" data-toggle="tooltip" data-placement="left">Activar</a>			  
			  @endif
			  </td>
			  <td>
			  <a href="{{ url('Cliente/editar?id='.$usuario->id) }}" class="btn-sm btn-warning" title="Editar los datos del cliente {{ ucwords($usuario->nombre_clientes) }}" data-toggle="tooltip" data-placement="left">Editar</a>
			  </td>
@endif			  
            </tr>
			@endif			
          @endforeach
          </tbody>
        </table>
		</div> 
      </div>
    </div>
	
  </div>
@endif  
@include('layouts.dash.footer')
