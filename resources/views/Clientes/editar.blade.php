@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[13] == '1')
  <div class="row">
    <div class="col-md-12">    
      <form action="{{ url('Cliente/update') }}" method="POST">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">   
      <div class="card">
        <div class="card-header">
          <h2><center>Editar Cliente  ({{ $usuarios[0]->nombre_clientes }}) </center></h2>
        </div>    
        @include('alerts.validacion')
        <div class="card-header">	
          <h4>Datos De Usuario: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
			<label>Empresa </label>
			<input type="text" id="venci" class="form-control date" name="Nombre" value="{{ $usuarios[0]->nombre_clientes }}">
			<input type="hidden" class="form-control date"  value="{{ $usuarios[0]->id }}" name="Id">			
			<label>Teléfono </label>
			<input type="text"  id="venci" class="form-control date" name="Teléfono" value="{{ $usuarios[0]->tel }}">
            </div>
            <div class="col-md-6">
            <label>Correo *</label>
			<input type="email" required="" class="form-control" name="Correo" value="{{ $usuarios[0]->correo_cliente }}">
			<label>Dirección</label>	
			<input type="text"  id="venci" class="form-control date" name="Address" value="{{ $usuarios[0]->address }}">
            <label>Vendedor *</label>
              <select required="" class="form-control" name="PersonalID">
				@foreach ($vendedores as $vendedor)
				@if ($vendedor->rol == "Vendedor")
                <option value="{{ $vendedor->id }}"> {{ $vendedor->nombre }} - {{ $vendedor->correo }} - {{ $vendedor->cargo }} </option>
				@endif
				@endforeach
              </select>				
            </div>
          </div>
        </div>
      </div>
    </div>		
	
    
    <div class="col-md-12">
      <center><input type="submit" class="btn btn-success" value="Actualizar Cliente"></center>
	  	<center><a href="index" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>
    </div>		
  </form>
      </div>
    </div>
@endif	
  @include('layouts.dash.footer')
