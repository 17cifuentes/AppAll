@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[8] == '1')
  <div class="row">
    <div class="col-md-12">    
      <form action="{{ url('ProductoTerminado/posteditar') }}" method="POST">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
      <div class="card">
        <div class="card-header">
          <h2>EDITAR ARTÍCULO ({{ $artiiculo[0]->art_nombre }})</h2>
        </div>
        @include('alerts.validacion')
        <div class="card-header">
          <h4>Datos del Artículo: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
			<label>Código </label>
			<input type="text" required="" id="venci" class="form-control date" name="Código" value="{{ $artiiculo[0]->art_nombre }}">
			<input type="hidden" class="form-control date"  value="{{ $artiiculo[0]->id }}" name="Id">								
			<label>Detalle </label>
			<input type="text"  id="venci" class="form-control date" name="Detalle" value="{{ $artiiculo[0]->detalle }}">
			<label>Unidad de Medida </label>
			<input type="text"  id="venci" class="form-control date" name="Medida" value="{{ $artiiculo[0]->medida }}">
			<label>Precio Unitario </label>
			<input type="number"  id="venci" class="form-control date" name="Precio" value="{{ $artiiculo[0]->precio_unitario }}">
			<label>Impuesto % </label>
			<input type="number"  id="venci" class="form-control date" name="Impuesto" value="{{ $artiiculo[0]->impuesto }}">				
            </div>
          </div>
        </div>
      </div>
    </div>		
	
    <div class="col-md-12">
      <center><input type="submit" class="btn btn-success" value="Actualizar Artículo"></center>
    </div>	
	
  </form>
  </div>
  
  <div class="row">
    <div class="col-md-12">    
      <div class="card">
        <div class="card-header">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
		<tr>	
          <th ><p style="font-size:40px">VER REQUERIMIENTOS PARA ({{ $artiiculo[0]->art_nombre }})</th>
		  <th align="center" valign="middle">
		  <a type="button" class="btn btn-success" data-toggle="modal" data-target="#addreq" title="Agregar requisitos para crear artículo" data-toggle="tooltip" data-placement="left">Agregar Requisito</a>
		  </th>
		</tr>		  
    </thead>	
	</table>		
        </div>
		
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
				<th> </th>
              <th>MATERIA PRIMA</th>
              <th>CANTIDAD</th>
			  <th>FECHA</th>
            </tr>
          </thead>
          <tbody>
		  @for ($i = 0; $i < count($det_prov); $i++)
		  <tr>
				<td class='text-center'>
				<a href="eliminardetalle?Id={{ $det_prov[$i]->id_requisitoart }}&IdPT={{$id_pt}}" onclick="">
				<i class="fa fa-trash" title="Eliminar requisito"></i></a>
				</td>		  
		  <td>{{ $MP[$det_prov[$i]->id_mp-1]->cod }}</td>
		  <td align="right">{{ $det_prov[$i]->cantidad }}
				<a href="#" data-toggle="modal" data-target="#editar{{$det_prov[$i]->id_requisitoart}}">
				<i class="fa fa-edit" title="Editar Cantidad"></i></a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="editar{{$det_prov[$i]->id_requisitoart}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
							<form action="editardetalle" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
							<div class="modal-content">
					  <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Editar Cantidad del Requisito de {{ $MP[$det_prov[$i]->id_mp-1]->cod }}</h4>
					  </div>							
								<div class="card-body">
								  <div class="row">
									<div class="col-md-12">
									<label>Cantidad </label>
									<input type="text" id="Cantidad" class="form-control date" name="Cantidad" maxlength="250" value="{{ $det_prov[$i]->cantidad }}">
									<input type="hidden" id="Id" class="form-control date" name="Id" value="{{ $det_prov[$i]->id_requisitoart }}">
									<input type="hidden" id="IdPT" class="form-control date" name="IdPT" value="{{$id_pt}}">
									</div>
								  </div>
								</div>
							<div class="modal-footer">
							  <center><input type="submit" class="btn btn-success" value="Actualizar Cantidad"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  
						  </div>
						</div>	
						<!-- Modal -->				
			<!-- ModalBODY -->
				<div class="modal fade" id="addreq" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">AÑADIR REQUISITO</h4>
					  </div>
				  <form action="{{ url('ProductoTerminado/addreq') }}" method="POST">		  
				  <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
					<div class="card-body">
					  <div class="row">
						<div class="col-md-6">
							<label>Materia Prima *</label>
						  <select required="" class="form-control" name="MateriaPrima">
							@foreach($MP as $elemento)
							<option value="{{ $elemento->id }}">{{ $elemento->cod }}</option>
							@endforeach				
						  </select>	
						  <input type="hidden" class="form-control date"  value="{{$id_pt}}" name="Id">           
						</div>
						<div class="col-md-6">
						<label>Cantidad de requisito</label>	
						<input required="" step="any" placeholder="0.00" type="number"  id="venci" class="form-control date" name="Cantidad">
						</div>
					  </div>
					</div>			
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary">Agregar</button>
					  </div>
			  </form>		  
					</div>
				  </div>
				</div>			
			<!-- Modal -->
						
			</td>		  
		  <td>{{ $det_prov[$i]->created_at }}</td>
		  </tr>
          @endfor
          </tbody>
        </table>     
      </div>	
		<center><a href="index" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>	  

	
	
  </div>
</div>  
</div>
@endif
    @include('layouts.dash.footer')
