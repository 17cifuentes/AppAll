@include('layouts.dash.header')
@include('layouts.dash.menu')
  <div class="row">
    <div class="col-md-12">    
      <div class="card">
        <div class="card-header">
          <h2>VER REQUERIMIENTOS PARA ({{ $artiiculo[0]->art_nombre }}) </h2>
        </div>
		
	
		@include('alerts.validacion')		
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
				<th> </th>
              <th>MATERIA PRIMA</th>
              <th>CANTIDAD</th>
			  <th>FECHA</th>
            </tr>
          </thead>
          <tbody>
		  @for ($i = 0; $i < count($det_prov); $i++)
		  <tr>
				<td class='text-center'>
				<a href="eliminardetalle?Id={{ $det_prov[$i]->id_requisitoart }}&IdPT={{$id_pt}}" onclick="">
				<i class="fa fa-trash" title="Eliminar requisito"></i></a>
				</td>		  
		  <td>{{ $MP[$det_prov[$i]->id_mp-1]->cod }}</td>
		  <td align="right">{{ $det_prov[$i]->cantidad }}
				<a href="#" data-toggle="modal" data-target="#editar{{$det_prov[$i]->id_requisitoart}}">
				<i class="fa fa-edit" title="Editar Cantidad"></i></a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="editar{{$det_prov[$i]->id_requisitoart}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<form action="editardetalle" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
							  <div class="col-md-12">
							  <div class="card">
								<div class="card-body">
								  <div class="row">
									<div class="col-md-12">
									<label>Cantidad </label>
									<input type="text" id="Cantidad" class="form-control date" name="Cantidad" maxlength="250" value="{{ $det_prov[$i]->cantidad }}">
									<input type="hidden" id="Id" class="form-control date" name="Id" value="{{ $det_prov[$i]->id_requisitoart }}">
									<input type="hidden" id="IdPT" class="form-control date" name="IdPT" value="{{$id_pt}}">
									</div>
								  </div>
								</div>
							  </div>
							  </div>
							<div class="col-md-12">
							  <center><input type="submit" class="btn btn-success" value="Actualizar Cantidad"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  

						<!-- Modal -->				
			</td>		  
		  <td>{{ $det_prov[$i]->created_at }}</td>
		  </tr>
          @endfor
          </tbody>
        </table>     
      </div>	
		<center><a href="listar" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>	  

	
	
  </div>
</div>
    @include('layouts.dash.footer')
