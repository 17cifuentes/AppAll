@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[10] == '1') 			
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Reportes de Productos</th>
            <th align="center" valign="middle">
@if ($permisos[11] == '1') 			
			<a href="#" data-toggle="modal" data-target="#ingresar" class="btn btn-success" title="Agregar registro de una producción" data-toggle="tooltip" data-placement="left"><span class="fa fa-plus"></span> <span class="fa fa-exchange"> Agregar Movimiento</a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="ingresar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<form action="{{ url('ProductoTerminado/producir') }}" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
							  <div class="col-md-12">
							  <div class="card">
								<div class="card-header">
								  <h2>Registro De Producido</h2>
								</div>
								<div class="card-header">
								  <h4>Datos Del Artículo: </h4>
								</div>
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									<label>Nombre </label>
								  <select required="" class="select" name="artiiculo" id="artiiculo">
									@foreach ($Lista as $elemento)
									@if ($elemento->estado == "Activo")
									<option value="{{ $elemento->id }}">{{ $elemento->art_nombre }}</option>
									@endif
									@endforeach
								  </select>
								  </div>
									<div class="col-md-6">
									<label>Cantidad Registrada</label>	
									<input type="number" step="any" placeholder="0.00" id="Cantidad" class="form-control date" name="Cantidad" maxlength="250">
									</div>
								  </div>
								</div>
							  </div>
							  </div>
							<div class="col-md-12">
							  <div class="card">
								<div class="card-header">
								  <h4>Responsable: </h4>
								</div>
								<div class="card-body">
								  <div class="row">
									<div class="col-md-6">
									  <label>Usuario</label>
								  <select required="" class="select" name="usuario" id="usuario">
								@if (Auth::user()->rol == "Administrador")
									@foreach ($usuarios as $elemento)	
									@if ($elemento->user_estado == "Activo" && $elemento->rol != "Cliente" && $elemento->nombre != "admin")
									<option value="{{ $elemento->id }}">{{ $elemento->nombre }}</option>
									@endif
									@endforeach
								@else
									<option value="{{ Auth::user()->id }}">{{ Auth::user()->name }}</option>
								@endif
								  </select>	
								  </div>
									<div class="col-md-6">
									  <label>Detalle</label>
										  <input type="text" required="" id="Detalle" class="form-control date" name="Detalle" maxlength="250" value="Registrado el día <?php echo strftime("%d a las %H:%M");?>">
									</div>
								  </div>
								</div>
							  </div>
							</div>
							<div class="col-md-12">
							  <center><input type="submit" class="btn btn-success" value="Registrar Producido"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  

						<!-- Modal -->	
@endif						
			</th>
            <th align="center" valign="middle">
			<a href="{{ url('ProductoTerminado/listarMov') }}" class="btn btn-info" title="Ver movimientos de producción" data-toggle="tooltip" data-placement="left"><span class="fa fa-eye"></span> <span class="fa fa-exchange"> Ver Movimientos</a>			
			</th>			
        </tr>
    </thead>
	</table>	
        </div>
		@include('alerts.validacion')			
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>NOMBRE</th>
              <th>DISPONIBLE</th>
			  <th>Posible</th>
@if ($permisos[11] == '1') 			  
			  <th>Personalizar</th>
@endif			  
            </tr>
          </thead>
          <tbody>
		  @for ($i = 0; $i < count($Lista); $i++)
		  @if ($Lista[$i]->estado == "Activo")
            <tr>
              <td>{{ ucwords($Lista[$i]->art_nombre) }}</td>
              <td align="right">{{ $Inventario[$i]->disponible }}</td>
			  <td align="right">{{ $disponibles[$i] }}</td>	
@if ($permisos[11] == '1') 			  
            <td align="center" valign="middle">		  
            <a href="{{ url('ProductoTerminado/addmpview?id_productoterminado='.$Lista[$i]->id) }}" class="btn-sm btn-primary" title="Crear {{ ucwords($Lista[$i]->art_nombre) }} por los requisitos que necesita" data-toggle="tooltip" data-placement="left">Generar</a>						
			</td>
@endif			
            </tr>
		@endif
          @endfor
          </tbody>
        </table>      
      </div>
    </div>
  </div>
@endif  
@include('layouts.dash.footer')