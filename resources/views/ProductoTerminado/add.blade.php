@include('layouts.dash.header')
@include('layouts.dash.menu')
  <div class="row">
    <div class="col-md-12">    
      <form action="{{ url('ProductoTerminado/postregistro') }}" method="POST">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
      <div class="card">
        <div class="card-header">
          <h2>REGISTRAR PRODUCTO TERMINADO</h2>
        </div>
        @include('alerts.validacion')
        <div class="card-header">
          <h4>Datos del Artículo: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
			<label>Nombre </label>
			<input type="text" required="" id="venci" class="form-control date" name="Código">
			<label>Detalle </label>
			<input type="text"  id="venci" class="form-control date" name="Detalle">
			<label>Unidad de Medida </label>
			<input type="text"  id="venci" class="form-control date" name="Medida">			
              <label>Estado </label>
              <select class="form-control" name="Estado">
                <option value="Activo">Activo</option>
                <option value="Inactivo">Inactivo</option>
              </select>				
            </div>
          </div>
        </div>
      </div>
    </div>		
	
    <div class="col-md-12">
      <center><input type="submit" class="btn btn-success" value="Añadir Artículo"></center>
    </div>	
	
  </form>
  </div>
</div>
    @include('layouts.dash.footer')
