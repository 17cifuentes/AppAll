@include('layouts.dash.header')
@include('layouts.dash.menu')
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Inventario de Productos</th>
        </tr>
    </thead>	
	</table>	
        </div>
		@include('alerts.validacion')			
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>NOMBRE</th>
              <th>DISPONIBLE</th>
			  <th>Unidad de Medida</th>
			  <th>Precio Unitario</th>
			  <th>Impuesto</th>
			  <th>Ver Requerimientos</th>
			  <th>Agregar Requisito</th>
            </tr>
          </thead>
          <tbody>
		  @for ($i = 0; $i < count($Lista); $i++)
		  @if ($Lista[$i]->estado == "Activo")
            <tr>
              <td>{{ ucwords($Lista[$i]->art_nombre) }}</td>
              <td align="right">{{ $Inventario[$i]->disponible }}</td>
			  <td>{{ ucwords($Lista[$i]->medida) }}</td>
              <td align="right">{{ $Lista[$i]->precio_unitario }}</td>
              <td align="right">{{ $Lista[$i]->impuesto }}</td>		  
            <td align="center" valign="middle">		  
            <a href="{{ url('ProductoTerminado/verREQ?id_productoterminado='.$Lista[$i]->id) }}" class="btn-sm btn-info" title="Ver los requisitos que se necesitan para crear un artículo de {{ ucwords($Lista[$i]->art_nombre) }}" data-toggle="tooltip" data-placement="left">Ver</a>						
			</td>
            <td align="center" valign="middle">		  
            <!-- <a href="{{ url('ProductoTerminado/addreqview?id_productoterminado='.$Lista[$i]->id) }}" class="btn btn-success">Agregar</a> -->
			<!-- Modal -->			
			<a type="button" class="btn-sm btn-success btn-sm" data-toggle="modal" data-target="#addreq{{$Lista[$i]->id}}" title="Agregar requisitos para crear un artículo de {{ ucwords($Lista[$i]->art_nombre) }}" data-toggle="tooltip" data-placement="left">Agregar</a>			
			<!-- ModalBODY -->
				<div class="modal fade" id="addreq{{$Lista[$i]->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">AÑADIR REQUISITO PARA {{$Lista[$i]->cod}}</h4>
					  </div>
				  <form action="{{ url('ProductoTerminado/addreq') }}" method="POST">		  
				  <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
					<div class="card-body">
					  <div class="row">
						<div class="col-md-6">
							<label>Materia Prima *</label>
						  <select required="" class="form-control" name="MateriaPrima">
							@foreach($MateriaPrima as $MP)
							<option value="{{ $MP->id }}">{{ $MP->cod }}</option>
							@endforeach				
						  </select>	
						  <input type="hidden" class="form-control date"  value="{{ $Lista[$i]->id }}" name="Id">           
						</div>
						<input type="hidden" readonly id="venci" class="form-control date" name="Detalle" value="{{ $Lista[$i]->art_nombre }}">			
						<div class="col-md-6">
						<label>Cantidad de requisito</label>	
						<input required="" step="any" placeholder="0.00" type="number"  id="venci" class="form-control date" name="Cantidad">
						</div>
					  </div>
					</div>			
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary">Agregar</button>
					  </div>
			  </form>		  
					</div>
				  </div>
				</div>			
			<!-- Modal -->
			</td>				
            </tr>
		@endif
          @endfor
          </tbody>
        </table>      
      </div>
    </div>
  </div>
@include('layouts.dash.footer')
