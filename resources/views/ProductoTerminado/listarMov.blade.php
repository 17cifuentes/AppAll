@include('layouts.dash.header')
@include('layouts.dash.menu')
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
    <table class="table" width="100%" cellspacing="0" >
    <thead>
        <tr>
            <th ><p style="font-size:50px">Movimientos de producción</th>
        </tr>
    </thead>	
	</table>	
        </div>
		@include('alerts.validacion')			
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>Responsable</th>
              <th>Artículo producido</th>
			  <th>Cantidad</th>
			  <th>Detalle</th>
            </tr>
          </thead>
          <tbody>
		  @foreach ($Lista as $elemento)
            <tr>
              <td>{{ ucwords($elemento->nombre) }}</td>
              <td>{{ $elemento->art_nombre }}</td>
			  <td align="right">{{ $elemento->cantidad }}</td>
              <td align="right">{{ $elemento->detalle }}</td>
            </tr>
          @endforeach
          </tbody>
        </table>      
      </div>
		<center><a href="crear" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>	  
	  
    </div>
  </div>
@include('layouts.dash.footer')
