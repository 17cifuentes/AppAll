@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
<?php  $listaMP = NULL;?>
@if ($permisos[11] == '1')
  <div class="row">
    <div class="col-md-12">    
	<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
      <div class="card">
        <div class="card-header">
          <h2>AÑADIR PRODUCTO TERMINADO PARA ({{ $requisitos[0]->art_nombre }})</h2>
		  <input type="hidden" value="{{ $requisitos[0]->id  }}" id="Id">
        </div>
        @include('alerts.validacion')
        <div class="card-header">
          <h4> -------------------------------------------------------------------------------------------------------------- </h4>
        </div>

        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
			<label>Datos de los requisitos: </label>
	<form action="#" method="get" id="form">		
<table class="table">
					<tr class="info">
						<td>id</td>
						<td>Materia</td>
						<td>Cantidad Necesaria</td>
						<td>Disponible</td>
					</tr>
					<?php  $x = 0;$numero = COUNT($requisitos); $array = array();?>
			@foreach($requisitos as $requisito)
				

					<tr>
						<td><?php  echo $x+1;  $array[] = $requisito->id_mp;?></td>
						<td>{{ $requisito->cod }}</td>
						<td>{{ $requisito->cantidad }}</td>
						<td>{{ $requisito->disponible }}</td>
					</tr>					
				
			<input type="hidden" value="{{ $requisito->disponible }}" id="cant{{ $x }}">
			<input type="hidden" value="{{ $requisito->cantidad  }}" id="necesario{{ $x++ }}">
			@endforeach
			<?php  $listaMP = json_encode($array);?>
</table>			
<input id="numero" value="{{ $numero }}" type="hidden">

            </div>


          </div>
        </div>
		<div class="card-body">
			<div class="row">
			<div class="col-md-12">
			<label>Cantidad a Generar </label>
			<input type="numer" required="" step="any" placeholder="0.00" id="cantidad" class="form-control date" name="Cantidad" maxlength="250">
			</div>
			</div>

		</div>
		
      </div>
    </div>		
	<input id="cc" value="{{ $requisitos }}" type="hidden" >
    <div class="col-md-12">
      <center><input type="button"  onclick="calcular()" class="btn btn-success" value="Generar Artículos"></center>
		<center><a href="crear" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>	  
	  
    </div>	
</form>	

  </div>
  

  
</div>
@endif
    @include('layouts.dash.footer')

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>




<script>
function calcular(){
var cantidad = $("#cc").val();
	var x = 0;
	var validar = 0;
	var calculo,disponible;
	var listaidMP = JSON.parse('<?= $listaMP; ?>');
	var necesario = $("#necesario"+x).val();
	var numero = $("#numero").val();
	var cantidad = $("#cantidad").val();
	var Id = $("#Id").val();
	for(x=0; x < numero; x++ ){
	disponible = $("#cant"+x).val();	
	calculo = $("#necesario"+x).val() * cantidad;
		if(calculo > disponible){
			alert("No alcanza la materia prima ".concat((x+1)));
			//alert(calculo);
		}else{
			validar++;
			//alert("coco");
		}
	}
	
	var totalUsado = [];
	if (numero == validar){
		//alert("alcanza");
		
		for(x=0; x < numero; x++ ){
			//alert("entra " + x)
		calculo = $("#necesario"+x).val() * cantidad;
		disponible = $("#cant"+x).val()-cantidad;
		totalUsado.push(calculo.toString());
		}
		
		window.location.href = "addMP?Id="+ Id + "&Cantidad=" + cantidad + "&idMPS=" + listaidMP + "&UsadoMPS=" + totalUsado; //"/{PTController}/{addreq}";
	}
}
/*
$("#form").submit(function() 
{
	var cantidad = $("#cant").val();
	var necesario = $("#necesario").val();
   alert(cant); 
   

});
*/
/*
$("#Cantidad").on("change", function() 
{
	var cantidad = $("#cant").val();
	var necesario = $("#necesario").val();
   console.log(cant); 
   

});
*/
</script>
