@include('layouts.dash.header')
@include('layouts.dash.menu')
  <div class="row">
    <div class="col-md-12">    
      <form action="{{ url('Finanza/updateCuota') }}" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
      <div class="card">
        <div class="card-header">
          <h2>Editar Gasto</h2>
        </div>
        @include('alerts.validacion')
        <div class="card-header">
          <h4>Datos Del Vendedor: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
            <label>Nombre</label>
			<input type="hidden" class="form-control date"  value="{{ $usuarios[0]->id }}" name="Id" id="Id">				
			<input type="text" readonly id="venci" class="form-control date" name="Nombre" value="{{ $personal[$usuarios[0]->id_personal-1]->nombre }}">						
            </div>
            <div class="col-md-6">
			<label>Cuota </label>
			<input type="text"  id="venci" class="form-control date" name="Valor" value="{{ $usuarios[0]->cuota }}">			
            </div>
          </div>
        </div>
      </div>
    </div>		
	
    <div class="col-md-12">
      <center><input type="submit" class="btn btn-success" value="Actualizar Cuota"></center>
    </div>	
	
  </form>
  </div>
</div>
    @include('layouts.dash.footer')