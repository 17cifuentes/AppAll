<!DOCTYPE html>
<?php

?>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>PDF</title>

    <style>

        .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #E84E0F;
  text-decoration: none;
}

body {
  position: relative;
  width: 18cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #555555;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 14px; 
  font-family: SourceSansPro;
}

header {
  width: 100%;
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

#logo {
  float: left;
  margin-top: 8px;
}

/*#logo img {
  height: 70px;
}*/

#company {
  float: right;
  text-align: right;
}


#details {
  margin-bottom: 50px;
}

#client {
  padding-left: 6px;
  border-left: 6px solid #E84E0F;
  float: left;
}

#client .name{
  text-transform: none;
}


#invoice {
  float: right;
  text-align: right;
}

#client .to {
  color: #777777;
}

h2.name {
  color: #E84E0F;
  font-size: 1.4em;
  font-weight: bold;
  margin: 0;
}

h2.client {
  color: #173e43;
  font-size: 1.0em;
  font-weight: bold;
  margin: 0;
  width:250px;
}



#invoice h3 {
  color: #E84E0F;
  font-size: 1.4em;
  line-height: 1em;
  font-weight: normal;
  margin: 0  0 10px 0;
}

#invoice .date {
  font-size: 1.1em;
  color: #777777;
}



table td h3{
  color: #E84E0F;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}

table .no {
  /*color: #FFFFFF;*/
  font-size: 1.6em;
  background: #DDDDDD;
  text-align: center;
}

table .desc1 {
  margin-top: 100em;
  text-align: left;
  background: #DDDDDD;
}

table .desc {
  text-align: left;
}

table .unit {
  background: #DDDDDD;
  text-align: center;
}

table .qty {
  text-align: center;
}

table .total {
  background: #DDDDDD;
  text-align: center;
  /*color: #FFFFFF;*/
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table tbody tr:last-child td {
  border: none;
}

table tfoot td {
  padding: 10px 20px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1.2em;
  white-space: nowrap; 
  border-top: 1px solid #AAAAAA; 
}

table tfoot td.totalfact {
  text-align: center;
}

table tfoot tr:first-child td {
  border-top: none; 
}

table tfoot tr:last-child td {
  color: #E84E0F;
  font-size: 1.4em;
  border-top: 1px solid #E84E0F; 

}

table tfoot tr td:first-child {
  border: none;
}

#thanks{
  font-size: 1.5em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #E84E0F;  
  border-right: 6px solid #E84E0F;  
  font-size: 1.5em;
}

/*#notices .notice {
  font-size: 0.8em;
}*/

#notices .referido {
  text-transform: uppercase;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}

        
          /*background: url('../../public/images/dimension.png'); */
        
		
    </style>  
</head>
<body>
   

    <header class="clearfix">

    <table border="1" cellspacing="0" style="width: 100%;">
        <tr>
            <td style="width: 25%; color: #444444;">
                <img height="10%" src="{{ $info[0]->pic}}" alt="Logo"><br>
            </td>		
			<td style="width: 25%;text-align:right;font-weight:bold;font-size:20px">
			DESPRENDIBLE DE NÓMINA
			</td>
			<td style="width: 50%; color: #34495e;font-size:12px;text-align:center">
                <span style="color: #34495e;font-size:14px;font-weight:bold">
				{{ ucwords($info[0]->nombre) }}
				</span>
				<br>
				{{ ucwords($info[0]->address) }}
				<br>
				{{ ucwords($info[0]->ciudad.' , '.$info[0]->paiis.' - '.$info[0]->cod_postal) }}
				<br> 
				Teléfono: {{ $info[0]->tel }}<br>
				Correo Electrónico: {{ $info[0]->correo }}
            </td>
        </tr>
    </table>
	
    </header>

    <main><br>

      <div style="margin-top: -40px" id="details">
      <center><h3  class="name">{{ $Nombre }}<br>{{ $Cargo }}<br>{{ $Contrato }}</h3></center>

<br>
    <table border="1" cellspacing="0" style="width: 100%;text-align:center">
        <tr>
            <td style="width: 20%; " bgcolor="#cccccc">
			Fecha Inicio:
            </td>	
			<td style="width: 20%; " bgcolor="#cccccc">
			</td>
			<td style="width: 20%; " bgcolor="#cccccc">
			Fecha Final:
			</td>
			<td style="width: 20%; " bgcolor="#cccccc">
            </td>
			<td style="width: 20%; " bgcolor="#cccccc">
			Total Días:
            </td>			
        </tr>
        <tr>
			</td>
			<td style="width: 20%; ">
			{{ $FechaInicio }}
            </td>		
            <td style="width: 20%; ">
            </td>
			<td style="width: 20%; ">
			{{ $FechaFinal }}
			</td>			
			<td style="width: 20%; ">
			</td>
			<td style="width: 20%; ">
			{{ $días }}
            </td>
        </tr>
		
    </table>	
	
<br>
	  
    <table border="1" cellspacing="0" style="width: 100%; text-align: left; font-size: 10pt;">
        <tr>
            <th style="width: 20%;text-align:center" class='midnight-blue' bgcolor="#cccccc">CONCEPTO</th>
            <th style="width: 35%;text-align:center" class='midnight-blue' bgcolor="#cccccc">DESCRIPCIÓN</th>			
            <th style="width: 15%;text-align: CENTER" class='midnight-blue' bgcolor="#cccccc">CANTIDAD</th>
            <th style="width: 15%;text-align: CENTER" class='midnight-blue' bgcolor="#cccccc">DEVENGO</th>			
            <th style="width: 15%;text-align: CENTER" class='midnight-blue' bgcolor="#cccccc">DEDUCCIONES</th>
            
        </tr>	
<?php
$total= 0;
?>

        <tr>
            <td class=' ' style="width: 20%; text-align: CENTER">010 </td>
            <td class=' ' style="width: 35%; text-align: center">SUELDO BÁSICO</td>
            <td class=' ' style="width: 15%; text-align: right">{{ $días }}</td>
            <td class=' ' style="width: 15%; text-align: right">$ {{ number_format(($Sueldo*$días)/30,0) }} <?php $total+=($Sueldo*$días)/30; ?></td>
            <td class=' ' style="width: 15%; text-align: right">$ 0</td>
        </tr>
        <tr>
            <td class=' ' style="width: 20%; text-align: CENTER">011 </td>
            <td class=' ' style="width: 35%; text-align: center">AUXILIO DE TRANSPORTE</td>
            <td class=' ' style="width: 15%; text-align: right">{{ $días }}</td>
            <td class=' ' style="width: 15%; text-align: right">$ {{ number_format(($Auxilio*$días)/30,0) }} <?php $total+=($Auxilio*$días)/30; ?></td>
            <td class=' ' style="width: 15%; text-align: right">$ 0</td>
        </tr>
        <tr>
            <td class=' ' style="width: 20%; text-align: CENTER">020 </td>
            <td class=' ' style="width: 35%; text-align: center">OTROS PAGOS, EXTRAS, COMPENSATORIOS</td>
            <td class=' ' style="width: 15%; text-align: right">{{ $días }}</td>
            <td class=' ' style="width: 15%; text-align: right">$ {{ number_format($Otros_Pagos,0) }} <?php $total+=$Otros_Pagos; ?></td>
            <td class=' ' style="width: 15%; text-align: right">$ 0</td>
        </tr>
        <tr>
            <td class=' ' style="width: 20%; text-align: CENTER">021 </td>
            <td class=' ' style="width: 35%; text-align: center">COMISIONES</td>
            <td class=' ' style="width: 15%; text-align: right">{{ $días }}</td>
            <td class=' ' style="width: 15%; text-align: right">$ {{ number_format($Comisiones,0) }} <?php $total+=$Comisiones; ?></td>
            <td class=' ' style="width: 15%; text-align: right">$ 0</td>
        </tr>
        <tr>
            <td class=' ' style="width: 20%; text-align: CENTER">030 </td>
            <td class=' ' style="width: 35%; text-align: center">APORTES SALUD</td>
            <td class=' ' style="width: 15%; text-align: right">15</td>
            <td class=' ' style="width: 15%; text-align: right">$ 0</td>
            <td class=' ' style="width: 15%; text-align: right">$ {{ number_format($Salud/2,0) }} <?php $total-=$Salud/2; ?></td>
        </tr>
        <tr>
            <td class=' ' style="width: 20%; text-align: CENTER">030 </td>
            <td class=' ' style="width: 35%; text-align: center">APORTES PENSIÓN</td>
            <td class=' ' style="width: 15%; text-align: right">15</td>
            <td class=' ' style="width: 15%; text-align: right"></td>
            <td class=' ' style="width: 15%; text-align: right">$ {{ number_format($Pensión/2) }} <?php $total-=$Pensión/2; ?></td>
        </tr>
        <tr>
            <td class=' ' style="width: 20%; text-align: CENTER"> </td>
            <td class=' ' style="width: 35%; text-align: center"></td>
            <td class=' ' style="width: 15%; text-align: right"></td>
            <td class=' ' style="width: 15%; text-align: right"></td>
            <td class=' ' style="width: 15%; text-align: right"></td>
        </tr>		
    </table>		
	
<br>	

    <table border="1" cellspacing="0" style="width: 100%; text-align: left; font-size: 20pt;">
		<tr>
            <td colspan="4" style="widtd: 85%; text-align: CENTER;">TOTAL $ </td>
            <td style="widtd: 15%; text-align: right;"> <?php  echo number_format($total,0); ?> </td>
        </tr>
    </table>
	  
<br>	  
		<center>

	  </center>
<br>
		<center>
      <div id="thanks" >"Lo que con mucho trabajo se adquiere, más se ama." ~Aristóteles~</div></center>
      
    </main>

	
</body>
</html> 