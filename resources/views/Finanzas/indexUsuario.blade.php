@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[14] == '1')  
    <div class="col-xs-12">
      <div class="card">
       <div class="card-header" style="overflow-x:auto;">
		<table class="table" width="100%" cellspacing="0" >
		<thead>
			<tr>
            <th ><p style="font-size:50px">Gestión de Gastos</th>
            <th align="center" valign="middle">
@if ($permisos[15] == '1')  			
			<a href="#" data-toggle="modal" data-target="#ingresar" class="btn btn-info" title="Registrar algún gasto"><span class="fa fa-plus"></span> <span class="fa fa-exchange"> Registrar Gasto</a>
						<!-- Modal -->			
						<!-- ModalBODY -->
							<div class="modal fade" id="ingresar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						  <form action="{{ url('Finanza/postregistro') }}" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
							  <div class="col-md-12">
						  <div class="card">
							<div class="card-header">
							  <h2>Registro De Gastos</h2>
							</div>
							<div class="card-header">
							  <h4>Datos Del Gasto: </h4>
							</div>
							<div class="card-body">
							  <div class="row">
								<div class="col-md-6">
								<label>Responsable*</label>
								  <select required="" class="form-control" name="PersonalID">
								@if (Auth::user()->rol == "Administrador")
									@foreach ($vendedores as $usuario)
									@if ($usuario->user_estado === "Activo" && $usuario->rol != "Cliente" && $usuario->nombre != "admin" )
									<option value="{{ $usuario->id }}"> {{ $usuario->nombre }} - {{ $usuario->correo }} - {{ $usuario->cargo }} </option>
									@endif
									@endforeach
								@else
									<option value="{{ Auth::user()->id }}"> {{ Auth::user()->name }} - {{ Auth::user()->email }} - {{ Auth::user()->rol }} </option>
								@endif
								  </select>	
								</div>
								<div class="col-md-6">
								<label>Concepto * </label>
								<input type="text" required id="venci" class="form-control date" name="Concepto">			
								<label>Comprobante</label>	
								<input type="file"  id="venci" class="form-control date" name="Comprobante">			
								<label>Valor </label>
								<input type="number"  id="venci" class="form-control date" name="Valor" value="0">			
								</div>
							  </div>
							</div>
						  </div>
							  </div>

							<div class="col-md-12">
							  <center><input type="submit" class="btn btn-success" value="Registrar Gasto"></center>
							  <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
							</div>
						  </form>
							</div>						  

						<!-- Modal -->
@endif						
			</th>
        </tr>
    </thead>	
	</table>	
        </div>
		@include('alerts.validacion')
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>NOMBRE</th>
              <th>CONCEPTO</th>
              <th>VALOR</th>
              <th>COMPROBANTE</th>				  
			  <th>Editar</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($usuarios as $usuario)
            <tr>
              <td>{{ ucwords($usuario->nombre) }}</td>
              <td>{{ $usuario->concepto }}</td>
              <td>{{ $usuario->valor }}</td>
              <td align="center" valign="middle">
			  <a data-toggle="modal" data-target="#Comprobante{{$usuario->id}}" class="btn-sm btn-primary" title="Ver el registro del certificado de {{ ucwords($usuario->nombre) }}" data-toggle="tooltip" data-placement="left">Ver</a>
				<div id="Comprobante{{$usuario->id}}" class="modal fade" role="dialog">  
				<div class="modal-dialog">
					<div class="modal-content">      
						<div class="modal-header">        
							<button type="button" class="close" data-dismiss="modal">x</button>        
							<h4 class="modal-title">Comprobante</h4>      </div>      
						<div class="modal-body"><img src="{{ asset( $usuario->comprobante)}}" class="img-rounded" alt="Modal Pic" width="304" height="236" />   </div>      
						<div class="modal-footer">        
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>     
						</div>  
					</div>  
				</div>			  
			  </td>	  			  			  
			  <td align="center" valign="middle">
			  <a href="{{ url('Finanza/editar?id='.$usuario->id_gastos) }}" class="btn-sm btn-warning" title="Editar el registro del gasto de {{ ucwords($usuario->nombre) }}" data-toggle="tooltip" data-placement="left">Editar</a>
			  </td>			  
            </tr>
          @endforeach
          </tbody>
        </table>
		</div> 
      </div>
    </div>
	
  </div>
@endif  
@include('layouts.dash.footer')
