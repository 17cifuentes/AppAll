@include('layouts.dash.header')
@include('layouts.dash.menu')
  <div class="row">
    <div class="col-md-12">    
      <form action="{{ url('MateriaPrima/addprovider') }}" method="POST">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
      <div class="card">
        <div class="card-header">
          <h2>AÑADIR PROVEEDOR PARA ({{ $artiiculo[0]->detalle }})</h2>
        </div>
        @include('alerts.validacion')
        <div class="card-header">
          <h4>Datos de la Materia Prima: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
			<label>NOMBRE </label>
			<input type="text" readonly id="venci" class="form-control date" name="Código" value="{{ $artiiculo[0]->cod }}" >
			<input type="hidden" class="form-control date"  value="{{ $artiiculo[0]->id }}" name="Id">					
            <label>Proveedor *</label>
              <select required="" class="form-control" name="Proveedor">
				@foreach($Lista as $usuario)
				
                <option value="{{ $usuario->id }}">{{ $usuario->empresa }}</option>
				
				@endforeach				
              </select>				
            </div>
            <div class="col-md-6">
			<label>Detalle </label>
			<input type="text" readonly id="venci" class="form-control date" name="Detalle" value="{{ $artiiculo[0]->detalle }}">			
			<label>Cantidad</label>	
			<input type="number"  id="venci" class="form-control date" name="Cantidad">
            </div>
          </div>
        </div>
      </div>
    </div>		
	
    <div class="col-md-12">
      <center><input type="submit" class="btn btn-success" value="Actualizar Materia Prima"></center>
    </div>	
	
  </form>
  </div>
</div>
    @include('layouts.dash.footer')
