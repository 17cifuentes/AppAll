@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[6] == '1')
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header" style="overflow-x:auto;">
		<table class="table" width="100%" cellspacing="0" >
		<thead>
			<tr>
				<th ><p style="font-size:50px">Inventario de Insumos</th>
			</tr>
		</thead>	
		</table>	
			</div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>NOMBRE</th>
              <th>DETALLE</th>		  
              <th>DISPONIBLE</th>
@if ($permisos[7] == '1')			  
			  <th>Registrar Bajada</th>
			<th>Agregar Insumos por proveedor</th>			  			  
@endif			
			  <th>Ver Movimientos</th>
            </tr>
          </thead>
          <tbody>
		  @for ($i = 0; $i < count($Lista); $i++)
		  @if ($Lista[$i]->estado == "Activo")
            <tr>
              <td>{{ ucwords($Lista[$i]->cod) }}</td>
              <td>{{ ucwords($Lista[$i]->detalle) }}</td>
              <td>{{ $Inventario[$i]->disponible }}</td>
@if ($permisos[7] == '1')				  
			<td align="center" valign="middle">	
			<!-- Modal -->			
			<a type="button" class="btn-sm btn-danger btn-sm" data-toggle="modal" data-target="#UsarMP{{$Lista[$i]->id}}" title="Registrar consumo externo de materia prima {{ ucwords($Lista[$i]->cod) }}">Usar</a>
			<!-- ModalBODY -->
				<div class="modal fade" id="UsarMP{{$Lista[$i]->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Usar materia prima de {{$Lista[$i]->cod}}</h4>
					  </div>
				  <form action="{{ url('MateriaPrima/removemp') }}" method="POST">		  
				  <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
					<div class="card-body">
					  <div class="row">
						<div class="col-md-6">
						<label>Disponible </label>
						<input readonly type="text" id="venci" class="form-control date" name="Disponible" value="{{ $Inventario[$i]->disponible }}" >
						<input type="hidden" class="form-control date"  value="{{ $Lista[$i]->id }}" name="Id">           
						<label>Anotación </label>
						<input type="text" id="venci" class="form-control date" name="Anotación" value="Desperdicio" >						
						</div>
						<input type="hidden" readonly id="venci" class="form-control date" name="Detalle" value="{{ $Lista[$i]->detalle }}">			
						<div class="col-md-6">
						<label>Cantidad a usar</label>	
						<input type="number" step="any" placeholder="0.00" required="" id="Cantidad" class="form-control date" name="Cantidad" onChange="compararDisponible({{ $Inventario[$i]->disponible }}, this.form.Cantidad);">
						</div>
					  </div>
					</div>			
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary">Usar</button> 
					  </div>
			  </form>		  
					</div>
				  </div>
				</div>			
			<!-- Modal -->			
			</td>	
			            <td align="center" valign="middle">		  
            <!-- <a href="{{ url('MateriaPrima/addproviders?id_materiaprima='.$Lista[$i]->id) }}" class="btn btn-primary">Agregar</a> -->
			<!-- Modal -->			
			<a type="button" class="btn-sm btn-primary btn-sm" data-toggle="modal" data-target="#addProvider{{$Lista[$i]->id}}" title="Agregar cantidad que ingresa de materia prima {{ ucwords($Lista[$i]->cod) }}">Agregar</a>
			<!-- ModalBODY -->
				<div class="modal fade" id="addProvider{{$Lista[$i]->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Añadir ingreso para {{$Lista[$i]->cod}}</h4>
					  </div>
				  <form action="{{ url('MateriaPrima/addprovider') }}" method="POST">		  
				  <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
					<div class="card-body">
					  <div class="row">
						<div class="col-md-6">
						<label>Anotación </label>
						<input type="text" id="venci" class="form-control date" name="Anotación" value="Ingreso por " >
						
							<label>Proveedor *</label>
						  <select required="" class="form-control" name="Proveedor">
							@foreach($Proveedores as $usuario)
							@if ($usuario->estado == "Activo")
							<option value="{{ $usuario->empresa }}">{{ $usuario->empresa }}</option>
							@endif
							@endforeach				
						  </select>	
						  <input type="hidden" class="form-control date"  value="{{ $Lista[$i]->id }}" name="Id">           
						 
						</div>
						<input type="hidden" readonly id="venci" class="form-control date" name="Detalle" value="{{ $Lista[$i]->detalle }}">			
						<div class="col-md-6">
						<label>Cantidad a Agregar</label>	
						<input required="" type="number"  step="any" placeholder="0.00" id="venci" class="form-control date" name="Cantidad">
						<label>El Costo total $</label>	
						<input required="" type="number"  placeholder="0" id="venci" class="form-control date" name="Costo">
						
						</div>
					  </div>
					</div>			
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary">Agregar</button>
					  </div>
			  </form>		  
					</div>
				  </div>
				</div>			
			<!-- Modal -->			
			</td>
@endif			
            <td align="center" valign="middle">		  
            <a href="{{ url('MateriaPrima/ver?id_materiaprima='.$Lista[$i]->id) }}" class="btn-sm btn-info" title="Ver el registro de las entradas y bajadas de las materias primas {{ ucwords($Lista[$i]->cod) }}">Ver</a>						
			</td>
			
            </tr>
		@endif
          @endfor
          </tbody>
        </table>      
      </div>
    </div>
  </div>
  
<!-- Modal -->

<!-- Modal -->
@endif
@include('layouts.dash.footer')

<script>
function compararDisponible(disponible,cantidad) 
{
	if (cantidad.value > disponible)
	{
	alert("La cantidad es mayor a lo disponible");
	cantidad.value = "0";
	}
}


</script>