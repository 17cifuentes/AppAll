@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[5] == '1')
  <div class="row">
    <div class="col-md-12">    
      <form action="{{ url('MateriaPrima/update') }}" method="POST">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
      <div class="card">
        <div class="card-header">
          <h2>EDITAR MATERIA PRIMA ({{ $usuarios[0]->cod }})</h2>
        </div>
        @include('alerts.validacion')
        <div class="card-header">
          <h4>Datos de la Materia Prima: </h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
			<label>Nombre </label>
			<input type="text" required="" id="venci" class="form-control date" name="Código" value="{{ $usuarios[0]->cod }}" maxlength="250">
			<input type="hidden" class="form-control date"  value="{{ $usuarios[0]->id }}" name="Id" id="Id">
			<label>Detalle </label>
			<input type="text"  id="venci" class="form-control date" name="Detalle" value="{{ $usuarios[0]->detalle }}" maxlength="250">
			<label>Unidad de Medida </label>
			<input type="text"  id="venci" class="form-control date" name="Medida" value="{{ $usuarios[0]->medida }}" maxlength="250">					
            </div>
          </div>
        </div>
      </div>
    </div>		
	
    <div class="col-md-12">
      <center><input type="submit" class="btn btn-success" value="Actualizar Materia Prima"></center>
		<center><a href="index" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>		  
    </div>	
	
  </form>
  </div>
</div>
@endif
@include('layouts.dash.footer')
