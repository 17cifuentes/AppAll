@include('layouts.dash.header')
@include('layouts.dash.menu')
<?php $permisos = Session::get('permisos')   ?>
@if ($permisos[6] == '1')
  <div class="row">
    <div class="col-md-12">    
      <form action="{{ url('MateriaPrima/addprovider') }}" method="POST">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">     	 
      <div class="card">
        <div class="card-header">
          <h2>VER MOVIMIENTO PARA ({{ $artiiculo[0]->cod }}) </h2>
        </div>
		
	
		@include('alerts.validacion')		
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
    <thead>
            <tr>
              <th>DETALLE</th>
			  <th>COSTO x UNIDAD $</th>
              <th>SUBIDA</th>
              <th>BAJADA</th>			  
			  <th>FECHA</th>
            </tr>
          </thead>
          <tbody>
		  @for ($i = 0; $i < count($det_prov); $i++)
		  <tr>
		  
			<td>
				{{ $det_prov[$i]->detalle }}
			</td>
			<td align="right">
				{{ $det_prov[$i]->costo }}
			</td>			
		  <td>
		  @if ( $det_prov[$i]->tipo == 1) {{ $det_prov[$i]->cantidad }}
		  @endif
		  </td>
		  <td>
		  @if ( $det_prov[$i]->tipo == 0) {{ $det_prov[$i]->cantidad }}
		  @endif		  
		  </td>		  
		  <td>{{ $det_prov[$i]->updated_at }}</td>	
		  </tr>
          @endfor
          </tbody>
        </table>     
      </div>	
		<label> . Costo promedio de la unidad $ {{ $Inventario[0]->totalcosto }}</label>
		<center><a href="listar" onclick="">
		<i class="btn btn-warning fa fa-reply-all" title="Regresar" data-toggle="tooltip" data-placement="left"></i></a>
		</center>		
	
  </div>
</div>
@endif
    @include('layouts.dash.footer')

